package ar.fiuba.tdd.tp1.spreadsheettest;

import ar.fiuba.tdd.tp1.exceptions.model.ExistingSheetFoundException;
import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;
import ar.fiuba.tdd.tp1.exceptions.model.InvalidSheetNameException;
import ar.fiuba.tdd.tp1.exceptions.model.SheetNotFoundException;
import ar.fiuba.tdd.tp1.format.formatter.FDate;
import ar.fiuba.tdd.tp1.format.formatter.InvalidFormatException;
import ar.fiuba.tdd.tp1.model.cell.Cell;
import ar.fiuba.tdd.tp1.model.cell.CrPair;
import ar.fiuba.tdd.tp1.spreadsheet.ContentAndFormatterSpreadSheet;
import ar.fiuba.tdd.tp1.spreadsheet.ContentAndFormatterSpreadSheetImpl;

import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class ContentAndFormatterSpreadSheetImplTest {

    public final ContentAndFormatterSpreadSheet spreadSheet = new ContentAndFormatterSpreadSheetImpl();

    @Test
    public void createSheet() {
        spreadSheet.createSheet("2");
        assertTrue(spreadSheet.existsSheet("2"));
    }

    @Test
    public void getNameSheet() {
        spreadSheet.createSheet("2");
        List<String> list = spreadSheet.getNamesSheet();
        assertEquals(list.get(0), "2");
    }

    @Test
    public void createAndRenameSheet() {
        spreadSheet.createSheet("2").renameSheet("2", "Hojita");
        assertTrue(spreadSheet.existsSheet("Hojita"));
    }

    @Test
    public void createAndRenameSheet2() {
        spreadSheet.createSheet("2").renameSheet("2", "Hojitas");
        assertFalse(spreadSheet.existsSheet("2"));
    }

    @Test
    public void createAndRemoveSheet() {
        spreadSheet.createSheet("2").removeSheet("2");
        assertFalse(spreadSheet.existsSheet("2"));
    }

    @Test(expected = ExistingSheetFoundException.class)
    public void createAndRenameSheetWithOtherNameSheet() {
        spreadSheet.createSheet("2").createSheet("1");
        spreadSheet.renameSheet("1", "2");
    }

    @Test(expected = ExistingSheetFoundException.class)
    public void createSheetWithOtherNameSheet() {
        spreadSheet.createSheet("2").createSheet("2");
    }

    @Test(expected = SheetNotFoundException.class)
    public void invalidRemoveSheet() {
        spreadSheet.removeSheet("211");
    }

    @Test(expected = SheetNotFoundException.class)
    public void invalidRenameSheet() {
        spreadSheet.renameSheet("2", "Hojita");
    }

    @Test(expected = InvalidSheetNameException.class)
    public void invalidCreateSheet() {
        spreadSheet.createSheet("");
    }

    @Test
    public void getEmptyCell() {
        try {
            spreadSheet.createSheet("1");
            assertEquals(spreadSheet.getCellEvaluatedValue("1", "A", 1), "");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void getEmptyCellFormatter() {
        try {
            spreadSheet.createSheet("1");
            spreadSheet.putCellRawValue("1", "A", 1, "2");
            assertEquals(spreadSheet.getCellFormattedValue("1", "A", 1), "2");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test(expected = SheetNotFoundException.class)
    public void invalidGetCellBecauseNotExistsSheet() throws InvalidCellIdentifierException {
        spreadSheet.getCellEvaluatedValue("1", "A", 1);
    }

    @Test(expected = SheetNotFoundException.class)
    public void invalidGetCellFormatterBecauseNotExistsSheet()
            throws InvalidCellIdentifierException {
        spreadSheet.getCellFormattedValue("1", "A", 1);
    }

    @Test(expected = InvalidCellIdentifierException.class)
    public void invalidGetCellBecauseInvalidColum() throws InvalidCellIdentifierException {
        spreadSheet.createSheet("1");
        spreadSheet.getCellEvaluatedValue("1", "A1", 1);
    }

    @Test(expected = InvalidCellIdentifierException.class)
    public void invalidGetCellFormattedBecauseInvalidColum() throws InvalidCellIdentifierException {
        spreadSheet.createSheet("1");
        spreadSheet.getCellFormattedValue("1", "A1", 1);
    }

    @Test(expected = InvalidCellIdentifierException.class)
    public void invalidGetCellBecauseInvalidRow() throws InvalidCellIdentifierException {
        spreadSheet.createSheet("1");
        spreadSheet.getCellEvaluatedValue("1", "A", -1);
    }

    @Test(expected = InvalidCellIdentifierException.class)
    public void invalidGetCellFormattedBecauseInvalidRow() throws InvalidCellIdentifierException {
        spreadSheet.createSheet("1");
        spreadSheet.getCellFormattedValue("1", "A", -1);
    }

    @Test(expected = SheetNotFoundException.class)
    public void invalidPutCellBecauseNotExistsSheet() {
        spreadSheet.putCellRawValue("1", "A", 1, "Hola");
    }

    @Test(expected = SheetNotFoundException.class)
    public void invalidPutCellFormattedBecauseNotExistsSheet() {
        spreadSheet.putCellFormatString("1", "A", 1);
    }

    @Test(expected = InvalidCellIdentifierException.class)
    public void invalidPutCellBecauseInvalidColum() throws InvalidCellIdentifierException {
        spreadSheet.createSheet("1").putCellRawValue("1", "A1", 1, "Hola");

    }

    @Test(expected = InvalidCellIdentifierException.class)
    public void invalidPutCellFormmatedBecauseInvalidColum() throws InvalidCellIdentifierException {
        spreadSheet.createSheet("1").putCellFormatString("1", "A1", 1);

    }

    @Test(expected = InvalidCellIdentifierException.class)
    public void invalidPutCellBecauseInvalidRow() throws InvalidCellIdentifierException {
        spreadSheet.createSheet("1").putCellRawValue("1", "A", -1, "Hola");
    }

    @Test(expected = InvalidCellIdentifierException.class)
    public void invalidPutCellFormmatedBecauseInvalidRow() throws InvalidCellIdentifierException {
        spreadSheet.createSheet("1").putCellFormatString("1", "A", -1);
    }

    @Test
    public void putString() {
        try {
            spreadSheet.createSheet("1").putCellRawValue("1", "A", 1, "Hola");
            assertEquals(spreadSheet.getCellEvaluatedValue("1", "A", 1), "Hola");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void putInt() {
        try {
            spreadSheet.createSheet("1").putCellRawValue("1", "A", 1, "1");
            assertEquals(spreadSheet.getCellEvaluatedValue("1", "A", 1), "1");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void putDate() {
        try {
            spreadSheet.createSheet("1").putCellRawValue("1", "A", 1, "23-11-1990");
            assertEquals(spreadSheet.getCellEvaluatedValue("1", "A", 1), ""
                    + new FDate("23-11-1990").date());
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void putDouble() {
        try {
            spreadSheet.createSheet("1").putCellRawValue("1", "A", 1, "0.26");
            assertEquals(spreadSheet.getCellEvaluatedValue("1", "A", 1), "0.26");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void putOtherCell() {
        try {
            spreadSheet.createSheet("1").putCellRawValue("1", "A", 2, "1");
            spreadSheet.putCellRawValue("1", "A", 1, "=1!A2");
            assertEquals(spreadSheet.getCellEvaluatedValue("1", "A", 1), "1");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void putOperationAddition() {
        try {
            spreadSheet.createSheet("1").putCellRawValue("1", "A", 1, "=1+2");
            assertEquals(spreadSheet.getCellEvaluatedValue("1", "A", 1), "3.0");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void putOperationAdditionWithOtherCell() {
        try {
            spreadSheet.createSheet("1").putCellRawValue("1", "A", 2, "1")
                    .putCellRawValue("1", "A", 1, "=1!A2+2");
            assertEquals(spreadSheet.getCellEvaluatedValue("1", "A", 1), "3.0");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void putOperationSubstraction() {
        try {
            spreadSheet.createSheet("1").putCellRawValue("1", "A", 1, "=3-1");
            assertEquals(spreadSheet.getCellEvaluatedValue("1", "A", 1), "2.0");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void putOperationSubstractionWithOtherCell() {
        try {
            spreadSheet.createSheet("1").putCellRawValue("1", "A", 2, "3")
                    .putCellRawValue("1", "A", 1, "=1!A2-1");
            assertEquals(spreadSheet.getCellEvaluatedValue("1", "A", 1), "2.0");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void putOperationSubstractionWithTwoCell() {
        try {
            spreadSheet.createSheet("1").putCellRawValue("1", "A", 2, "3")
                    .putCellRawValue("1", "A", 4, "1").putCellRawValue("1", "A", 1, "=1!A2-1!A4");
            assertEquals(spreadSheet.getCellEvaluatedValue("1", "A", 1), "2.0");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void putOperationAdditiontionWithTwoCell() {
        try {
            spreadSheet.createSheet("1").putCellRawValue("1", "A", 2, "1")
                    .putCellRawValue("1", "A", 4, "1.5").putCellRawValue("1", "A", 1, "=1!A2+1!A4");
            assertEquals(spreadSheet.getCellEvaluatedValue("1", "A", 1), "2.5");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void putOperationAdditionAndSubstractionWithOtherCell() {
        try {
            spreadSheet.createSheet("1").putCellRawValue("1", "A", 2, "3")
                    .putCellRawValue("1", "A", 1, "=2+1!A2-1");
            assertEquals(spreadSheet.getCellEvaluatedValue("1", "A", 1), "4.0");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void putOperationInvalid() {
        try {
            spreadSheet.createSheet("1").putCellRawValue("1", "A", 2, "=TirarError()");
            assertEquals(spreadSheet.getCellEvaluatedValue("1", "A", 2), "Error:BAD_FORMULA");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void putOperationInvalidWithFormat() {
        try {
            spreadSheet.createSheet("1").putCellRawValue("1", "A", 2, "=TirarError()");
            assertEquals(spreadSheet.getCellFormattedValue("1", "A", 2), "Error:BAD_FORMULA");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void putOperationWithCellThatDoesNotExist() {
        try {
            spreadSheet.createSheet("1").putCellRawValue("1", "A", 1, "=1!A2-1");
            assertEquals(spreadSheet.getCellEvaluatedValue("1", "A", 1), "Error:BAD_FORMULA");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void putOperationWithCellThatDoesNotExistWithFormat() {
        try {
            spreadSheet.createSheet("1").putCellRawValue("1", "A", 1, "=1!A2-1");
            assertEquals(spreadSheet.getCellFormattedValue("1", "A", 1), "Error:BAD_FORMULA");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void putOperationWithInvalidCellBecauseInvalidRow() {
        try {
            spreadSheet.createSheet("1").putCellRawValue("1", "A", 1, "=1!A-2-1");
            assertEquals(spreadSheet.getCellEvaluatedValue("1", "A", 1), "Error:BAD_FORMULA");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void putOperationWithInvalidCellBecauseInvalidRowWithFormat() {
        try {
            spreadSheet.createSheet("1").putCellRawValue("1", "A", 1, "=1!A-2-1");
            assertEquals(spreadSheet.getCellFormattedValue("1", "A", 1), "Error:BAD_FORMULA");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void putOperationWithInvalidCellBecauseSheetDoesNotExist() {
        try {
            spreadSheet.createSheet("1").putCellRawValue("1", "A", 1, "=Hojita!A2-1");
            assertEquals(spreadSheet.getCellEvaluatedValue("1", "A", 1), "Error:BAD_FORMULA");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void putOperationWithInvalidCellBecauseSheetDoesNotExistWithFormat() {
        try {
            spreadSheet.createSheet("1").putCellRawValue("1", "A", 1, "=Hojita!A2-1");
            assertEquals(spreadSheet.getCellFormattedValue("1", "A", 1), "Error:BAD_FORMULA");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void undoPutNumberAndOperationSubstraction() {
        try {
            spreadSheet.createSheet("1").putCellRawValue("1", "A", 1, "58")
                    .putCellRawValue("1", "A", 1, "=3-1");
            spreadSheet.undo();
            assertEquals(spreadSheet.getCellEvaluatedValue("1", "A", 1), "58");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void undoPutNumberAndFormattedNumberAndMoney() {
        try {
            spreadSheet.createSheet("1").putCellRawValue("1", "A", 1, "58")
                    .putCellFormatNumber("1", "A", 1, 2).putCellFormatMoney("1", "A", 1, "$")
                    .putCellFormatNumber("1", "A", 1, 0);
            spreadSheet.undo();
            assertEquals(spreadSheet.getCellFormattedValue("1", "A", 1), "$ 58.00");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void undoAndRedoPutOperationSubstraction() {
        try {
            spreadSheet.createSheet("1").putCellRawValue("1", "A", 1, "=3-1");
            spreadSheet.undo().redo();
            assertEquals(spreadSheet.getCellEvaluatedValue("1", "A", 1), "2.0");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void undoAndRedoPutOperationSubstractionAndFormatterNumber() {
        try {
            spreadSheet.createSheet("1").putCellFormatNumber("1", "A", 1, 3)
                    .putCellRawValue("1", "A", 1, "=3-1");
            spreadSheet.undo().redo();
            assertEquals(spreadSheet.getCellFormattedValue("1", "A", 1), "2.000");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void undoAndRedoWithFormattersAndValues() {
        try {
            spreadSheet.createSheet("1").putCellRawValue("1", "A", 1, "5")
                    .putCellFormatNumber("1", "A", 1, 3);
            spreadSheet.putCellRawValue("1", "A", 2, "=3-1");
            spreadSheet.undo().undo().putCellRawValue("1", "A", 2, "hola");
            assertEquals(spreadSheet.getCellEvaluatedValue("1", "A", 1), "5");
            assertEquals(spreadSheet.getCellEvaluatedValue("1", "A", 2), "hola");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void testEvaluateCellValueOfFormulaWithReferencedAndUnreferencedCells() {
        String sheet1 = "sheet1";
        spreadSheet.createSheet(sheet1);
        spreadSheet.putCellRawValue(sheet1, "A", 1, "4.5");
        spreadSheet.putCellRawValue(sheet1, "B", 2, "0.5");
        spreadSheet.putCellRawValue(sheet1, "C", 3, "=A1 - B2");
        assertEquals(spreadSheet.getCellEvaluatedValue(sheet1, "C", 3), "4.0");

        String sheet2 = "sheet2";
        spreadSheet.createSheet(sheet2);
        spreadSheet.putCellRawValue(sheet2, "A", 1, "1");
        String complexFormula = "=sheet1!C3 - A1 + 0.25 -1.75";
        spreadSheet.putCellRawValue(sheet2, "B", 2, complexFormula);
        assertEquals(spreadSheet.getCellEvaluatedValue(sheet2, "B", 2), "1.5");

        assertTrue(spreadSheet.getCellRawValue(sheet2, "B", 2).equals(complexFormula));
    }

    @Test
    public void putFormatDateYyyyMmDdWithNumber() {
        FDate date = new FDate(2000, 1, 13);
        long numberDate = date.date();
        spreadSheet.createSheet("1").putCellFormatDate("1", "A", 1, "yyyy-MM-dd")
                .putCellRawValue("1", "A", 1, "" + numberDate);
        assertEquals("2000-01-13", spreadSheet.getCellFormattedValue("1", "A", 1));
    }

    @Test
    public void putFormatDateDdMmYyyyWithNumber() {
        FDate date = new FDate(2000, 1, 13);
        long numberDate = date.date();
        spreadSheet.createSheet("1").putCellFormatDate("1", "A", 1, "dd-MM-yyyy")
                .putCellRawValue("1", "A", 1, "" + numberDate);
        assertEquals("13-01-2000", spreadSheet.getCellFormattedValue("1", "A", 1));
    }

    @Test
    public void putFormatDateMmDdYyyyWithNumber() {
        FDate date = new FDate(2000, 1, 13);
        long numberDate = date.date();
        spreadSheet.createSheet("1").putCellFormatDate("1", "A", 1, "MM-dd-yyyy")
                .putCellRawValue("1", "A", 1, "" + numberDate);
        assertEquals("01-13-2000", spreadSheet.getCellFormattedValue("1", "A", 1));
    }

    @Test
    public void putDateWithFormat() {
        try {
            spreadSheet.createSheet("1").putCellRawValue("1", "A", 1, "23-11-1990");
            assertEquals(spreadSheet.getCellFormattedValue("1", "A", 1), "23-11-1990");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }
    
    @Test
    public void putNotDate() {
        try {
            spreadSheet.createSheet("1").putCellRawValue("1", "A", 1, "23-11");
            assertEquals(spreadSheet.getCellRawValue("1", "A", 1), "23-11");
            assertEquals(spreadSheet.getCellFormattedValue("1", "A", 1), "23-11");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void putFomatNumber() {
        spreadSheet.createSheet("1").putCellFormatNumber("1", "A", 1, 1)
                .putCellRawValue("1", "A", 1, "32.34634");
        assertEquals(spreadSheet.getCellFormattedValue("1", "A", 1), "32.3");
    }

    @Test
    public void putFomatMoney() {
        spreadSheet.createSheet("1").putCellFormatNumber("1", "A", 1, 3)
                .putCellFormatMoney("1", "A", 1, "#").putCellRawValue("1", "A", 1, "32.34634");
        assertEquals(spreadSheet.getCellFormattedValue("1", "A", 1), "# 32.346");
    }

    @Test
    public void putFomatString() {
        spreadSheet.createSheet("1").putCellFormatString("1", "A", 1)
                .putCellRawValue("1", "A", 1, "32.34634");
        assertEquals(spreadSheet.getCellFormattedValue("1", "A", 1), "32.34634");
    }

    @Test
    public void createSpreadSheetAndGetCells() {
        FDate date = new FDate(2000, 1, 13);
        long numberDate = date.date();
        spreadSheet.createSheet("Hoja1").createSheet("Hoja2").createSheet("Hoja3");
        spreadSheet.putCellRawValue("Hoja1", "A", 1, "23").putCellFormatNumber("Hoja1", "A", 1, 2)
                .putCellFormatMoney("Hoja1", "A", 1, "$");
        spreadSheet.putCellRawValue("Hoja1", "A", 2, "26.3458").putCellFormatNumber("Hoja1", "A",
                2, 3);
        spreadSheet.putCellRawValue("Hoja2", "A", 3, "" + numberDate).putCellFormatDate("Hoja2",
                "A", 3, "MM-dd-yyyy");
        spreadSheet.putCellRawValue("Hoja3", "B", 1, "Hola");
        HashMap<String, Map<CrPair, Cell<CrPair, String>>> map = spreadSheet.getCellsInBook();
        Set<String> sheets = map.keySet();
        assertTrue(sheets.contains("Hoja1"));
        assertTrue(sheets.contains("Hoja2"));
        assertTrue(sheets.contains("Hoja3"));
        Map<CrPair, Cell<CrPair, String>> mapCell = map.get("Hoja1");
        assertEquals((mapCell.get(new CrPair(1, "A"))).getRawValue(), "23");
        assertEquals((mapCell.get(new CrPair(2, "A"))).getRawValue(), "26.3458");
        mapCell = map.get("Hoja2");
        assertEquals((mapCell.get(new CrPair(3, "A"))).getRawValue(), "" + numberDate);
        mapCell = map.get("Hoja3");
        assertEquals((mapCell.get(new CrPair(1, "B"))).getRawValue(), "Hola");
    }

    @Test
    public void createSpreadSheetAndGetFormatters() {
        spreadSheet.createSheet("Hoja1");
        spreadSheet.putCellRawValue("Hoja1", "A", 1, "23").putCellFormatNumber("Hoja1", "A", 1, 2)
                .putCellFormatMoney("Hoja1", "A", 1, "$");
        spreadSheet.putCellRawValue("Hoja1", "A", 2, "26.3458").putCellFormatNumber("Hoja1", "A",
                2, 3);
        HashMap<String, Map<CrPair, Map<String, String>>> map = spreadSheet.getFormatterInBook();
        Set<String> sheets = map.keySet();
        assertTrue(sheets.contains("Hoja1"));

        Map<CrPair, Map<String, String>> mapCell = map.get("Hoja1");

        assertEquals((mapCell.get(new CrPair(1, "A"))).get("Type"), "Money");
        assertEquals((mapCell.get(new CrPair(1, "A"))).get("Decimal"), "2");
        assertEquals((mapCell.get(new CrPair(1, "A"))).get("Symbol"), "$");

        assertEquals((mapCell.get(new CrPair(2, "A"))).get("Type"), "Number");
        assertEquals((mapCell.get(new CrPair(2, "A"))).get("Decimal"), "3");
    }

    @Test
    public void createSpreadSheetAndGetFormatters2() {
        FDate date = new FDate(2000, 1, 13);
        long numberDate = date.date();
        spreadSheet.createSheet("Hoja2").createSheet("Hoja3");
        spreadSheet.putCellRawValue("Hoja2", "A", 3, "" + numberDate).putCellFormatDate("Hoja2",
                "A", 3, "MM-dd-yyyy");
        spreadSheet.putCellRawValue("Hoja3", "B", 1, "Hola");
        HashMap<String, Map<CrPair, Map<String, String>>> map = spreadSheet.getFormatterInBook();
        Set<String> sheets = map.keySet();
        assertTrue(sheets.contains("Hoja2"));
        assertTrue(sheets.contains("Hoja3"));

        Map<CrPair, Map<String, String>> mapCell = map.get("Hoja2");

        assertEquals((mapCell.get(new CrPair(3, "A"))).get("Type"), "Date");
        assertEquals((mapCell.get(new CrPair(3, "A"))).get("Format"), "MM-DD-YYYY");

        mapCell = map.get("Hoja3");

        assertEquals((mapCell.get(new CrPair(1, "B"))).get("Type"), "String");
    }

    @Test
    public void createSpreadSheetAndGetCellsFormatted() {
        FDate date = new FDate(2000, 1, 13);
        long numberDate = date.date();
        spreadSheet.createSheet("Hoja1").createSheet("Hoja2").createSheet("Hoja3");
        spreadSheet.putCellRawValue("Hoja1", "A", 1, "23").putCellFormatNumber("Hoja1", "A", 1, 2)
                .putCellFormatMoney("Hoja1", "A", 1, "$");
        spreadSheet.putCellRawValue("Hoja1", "A", 2, "26.3458").putCellFormatNumber("Hoja1", "A",
                2, 3);
        spreadSheet.putCellRawValue("Hoja2", "A", 3, "" + numberDate).putCellFormatDate("Hoja2",
                "A", 3, "MM-dd-yyyy");
        spreadSheet.putCellRawValue("Hoja3", "B", 1, "Hola");
        HashMap<String, Map<CrPair, Cell<CrPair, String>>> map = spreadSheet
                .getCellsFormattedInBook();
        Set<String> sheets = map.keySet();
        assertTrue(sheets.contains("Hoja1"));
        assertTrue(sheets.contains("Hoja2"));
        assertTrue(sheets.contains("Hoja3"));
        Map<CrPair, Cell<CrPair, String>> mapCell = map.get("Hoja1");
        assertEquals((mapCell.get(new CrPair(1, "A"))).getRawValue(), "$ 23.00");
        assertEquals((mapCell.get(new CrPair(2, "A"))).getRawValue(), "26.346");
        mapCell = map.get("Hoja2");
        assertEquals((mapCell.get(new CrPair(3, "A"))).getRawValue(), "01-13-2000");
        mapCell = map.get("Hoja3");
        assertEquals((mapCell.get(new CrPair(1, "B"))).getRawValue(), "Hola");
    }

    @Test
    public void createSpreadSheetAndGetCellsFormatted2() {
        FDate date = new FDate(2000, 1, 13);
        long numberDate = date.date();
        spreadSheet.createSheet("Hoja1").createSheet("Hoja2").createSheet("Hoja3");
        spreadSheet.putCellRawValue("Hoja1", "A", 1, "23").putCellFormatNumber("Hoja1", "A", 1, 2)
                .putCellFormatMoney("Hoja1", "A", 1, "$");
        spreadSheet.putCellRawValue("Hoja1", "A", 2, "26.3458").putCellFormatNumber("Hoja1", "A",
                2, 3);
        spreadSheet.putCellRawValue("Hoja2", "A", 3, "" + numberDate).putCellFormatDate("Hoja2",
                "A", 3, "MM-dd-yyyy");
        spreadSheet.putCellRawValue("Hoja3", "B", 1, "Hola");
        HashMap<String, Map<CrPair, Cell<CrPair, String>>> map = spreadSheet
                .getCellsFormattedInBook();
        map = spreadSheet.getCellsFormattedInBook();
        Map<CrPair, Cell<CrPair, String>> mapCell = map.get("Hoja1");
        assertEquals((mapCell.get(new CrPair(1, "A"))).getRawValue(), "$ 23.00");
        assertEquals((mapCell.get(new CrPair(2, "A"))).getRawValue(), "26.346");
        mapCell = map.get("Hoja2");
        assertEquals((mapCell.get(new CrPair(3, "A"))).getRawValue(), "01-13-2000");
        mapCell = map.get("Hoja3");
        assertEquals((mapCell.get(new CrPair(1, "B"))).getRawValue(), "Hola");
    }

    @Test
    public void getCellWithInvalidFormat() {
        spreadSheet.createSheet("Hoja1");
        spreadSheet.putCellRawValue("Hoja1", "A", 1, "Hola");
        spreadSheet.putCellFormatDate("Hoja1", "A", 1, "DD-MM-YYYY");
        assertEquals(spreadSheet.getCellFormattedValue("Hoja1", "A", 1), "Error:BAD_DATE");
    }

    @Test
    public void getCellWithInvalidFormat2() {
        spreadSheet.createSheet("Hoja1");
        spreadSheet.putCellRawValue("Hoja1", "A", 1, "-26");
        spreadSheet.putCellFormatDate("Hoja1", "A", 1, "DD-MM-YYYY");
        String value = spreadSheet.getCellFormattedValue("Hoja1", "A", 1);
        assertEquals(spreadSheet.getCellFormattedValue("Hoja1", "A", 1), "Error:BAD_DATE");
    }

    @Test(expected = InvalidFormatException.class)
    public void getCellWithInvalidFormat3() {
        spreadSheet.createSheet("Hoja1");
        spreadSheet.putCellFormatDate("Hoja1", "A", 1, "hola");
    }

}
