package ar.fiuba.tdd.tp1.spreadsheettest;

import ar.fiuba.tdd.tp1.exceptions.model.ExistingSheetFoundException;
import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;
import ar.fiuba.tdd.tp1.exceptions.model.InvalidSheetNameException;
import ar.fiuba.tdd.tp1.exceptions.model.SheetNotFoundException;
import ar.fiuba.tdd.tp1.spreadsheet.ContentSpreadSheet;
import ar.fiuba.tdd.tp1.spreadsheet.ContentSpreadSheetImpl;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class ContentSpreadSheetImplTest {

    public final ContentSpreadSheet fiubaSpreadSheet = new ContentSpreadSheetImpl();

    @Test
    public void createSheet() {
        fiubaSpreadSheet.createSheet("2");
        assertTrue(fiubaSpreadSheet.existsSheet("2"));
    }

    @Test
    public void getNameSheet() {
        fiubaSpreadSheet.createSheet("2");
        List<String> list = fiubaSpreadSheet.getNamesSheet();
        assertEquals(list.get(0), "2");
    }

    @Test
    public void createAndRenameSheet() {
        fiubaSpreadSheet.createSheet("2").renameSheet("2", "Hojita");
        assertTrue(fiubaSpreadSheet.existsSheet("Hojita"));
    }

    @Test
    public void createAndRenameSheet2() {
        fiubaSpreadSheet.createSheet("2").renameSheet("2", "Hojita");
        assertFalse(fiubaSpreadSheet.existsSheet("2"));
    }

    @Test
    public void createAndRemoveSheet() {
        fiubaSpreadSheet.createSheet("2").removeSheet("2");
        assertFalse(fiubaSpreadSheet.existsSheet("2"));
    }

    @Test(expected = ExistingSheetFoundException.class)
    public void createAndRenameSheetWithOtherNameSheet() {
        fiubaSpreadSheet.createSheet("2").createSheet("1");
        fiubaSpreadSheet.renameSheet("1", "2");
    }

    @Test(expected = ExistingSheetFoundException.class)
    public void createSheetWithOtherNameSheet() {
        fiubaSpreadSheet.createSheet("2").createSheet("2");
    }

    @Test(expected = SheetNotFoundException.class)
    public void invalidRemoveSheet() {
        fiubaSpreadSheet.removeSheet("2");
    }

    @Test(expected = SheetNotFoundException.class)
    public void invalidRenameSheet() {
        fiubaSpreadSheet.renameSheet("2", "Hojita");
    }

    @Test(expected = InvalidSheetNameException.class)
    public void invalidCreateSheet() {
        fiubaSpreadSheet.createSheet("");
    }

    @Test
    public void getEmptyCell() {
        try {
            fiubaSpreadSheet.createSheet("1");
            assertEquals(fiubaSpreadSheet.getCellEvaluatedValue("1", "A", 1), "");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test(expected = SheetNotFoundException.class)
    public void invalidGetCellBecauseNotExistsSheet() throws InvalidCellIdentifierException {
        fiubaSpreadSheet.getCellEvaluatedValue("1", "A", 1);
    }

    @Test(expected = InvalidCellIdentifierException.class)
    public void invalidGetCellBecauseInvalidColum() throws InvalidCellIdentifierException {
        fiubaSpreadSheet.createSheet("1");
        fiubaSpreadSheet.getCellEvaluatedValue("1", "A1", 1);
    }

    @Test(expected = InvalidCellIdentifierException.class)
    public void invalidGetCellBecauseInvalidRow() throws InvalidCellIdentifierException {
        fiubaSpreadSheet.createSheet("1");
        fiubaSpreadSheet.getCellEvaluatedValue("1", "A", -1);
    }

    @Test(expected = SheetNotFoundException.class)
    public void invalidPutCellBecauseNotExistsSheet() {
        fiubaSpreadSheet.putCell("1", "A", 1, "Hola");
    }

    @Test(expected = InvalidCellIdentifierException.class)
    public void invalidPutCellBecauseInvalidColum() throws InvalidCellIdentifierException {
        fiubaSpreadSheet.createSheet("1").putCell("1", "A1", 1, "Hola");

    }

    @Test(expected = InvalidCellIdentifierException.class)
    public void invalidPutCellBecauseInvalidRow() throws InvalidCellIdentifierException {
        fiubaSpreadSheet.createSheet("1").putCell("1", "A", -1, "Hola");
    }

    @Test
    public void putString() {
        try {
            fiubaSpreadSheet.createSheet("1").putCell("1", "A", 1, "Hola");
            assertEquals(fiubaSpreadSheet.getCellEvaluatedValue("1", "A", 1), "Hola");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void putInt() {
        try {
            fiubaSpreadSheet.createSheet("1").putCell("1", "A", 1, "1");
            assertEquals(fiubaSpreadSheet.getCellEvaluatedValue("1", "A", 1), "1");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void putDouble() {
        try {
            fiubaSpreadSheet.createSheet("1").putCell("1", "A", 1, "0.26");
            assertEquals(fiubaSpreadSheet.getCellEvaluatedValue("1", "A", 1), "0.26");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void putOtherCell() {
        try {
            fiubaSpreadSheet.createSheet("1").putCell("1", "A", 2, "1");
            fiubaSpreadSheet.putCell("1", "A", 1, "=1!A2");
            assertEquals("1",fiubaSpreadSheet.getCellEvaluatedValue("1", "A", 1));
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void putOperationAddition() {
        try {
            fiubaSpreadSheet.createSheet("1").putCell("1", "A", 1, "=1+2");
            assertEquals(fiubaSpreadSheet.getCellEvaluatedValue("1", "A", 1), "3.0");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void putOperationAdditionWithOtherCell() {
        try {
            fiubaSpreadSheet.createSheet("1").putCell("1", "A", 2, "1")
                    .putCell("1", "A", 1, "=1!A2+2");
            assertEquals(fiubaSpreadSheet.getCellEvaluatedValue("1", "A", 1), "3.0");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void putOperationSubstraction() {
        try {
            fiubaSpreadSheet.createSheet("1").putCell("1", "A", 1, "=3-1");
            assertEquals(fiubaSpreadSheet.getCellEvaluatedValue("1", "A", 1), "2.0");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void putOperationSubstractionWithOtherCell() {
        try {
            fiubaSpreadSheet.createSheet("1").putCell("1", "A", 2, "3")
                    .putCell("1", "A", 1, "=1!A2-1");
            assertEquals(fiubaSpreadSheet.getCellEvaluatedValue("1", "A", 1), "2.0");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void putOperationSubstractionWithTwoCell() {
        try {
            fiubaSpreadSheet.createSheet("1").putCell("1", "A", 2, "3").putCell("1", "A", 4, "1")
                    .putCell("1", "A", 1, "=1!A2-1!A4");
            assertEquals(fiubaSpreadSheet.getCellEvaluatedValue("1", "A", 1), "2.0");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void putOperationAdditiontionWithTwoCell() {
        try {
            fiubaSpreadSheet.createSheet("1").putCell("1", "A", 2, "1").putCell("1", "A", 4, "1.5")
                    .putCell("1", "A", 1, "=1!A2+1!A4");
            assertEquals(fiubaSpreadSheet.getCellEvaluatedValue("1", "A", 1), "2.5");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void putOperationAdditionAndSubstractionWithOtherCell() {
        try {
            fiubaSpreadSheet.createSheet("1").putCell("1", "A", 2, "3")
                    .putCell("1", "A", 1, "=2+1!A2-1");
            assertEquals(fiubaSpreadSheet.getCellEvaluatedValue("1", "A", 1), "4.0");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void putOperationInvalid() {
        try {
            fiubaSpreadSheet.createSheet("1").putCell("1", "A", 2, "=TirarError()");
            assertEquals(fiubaSpreadSheet.getCellEvaluatedValue("1", "A", 2), "Formula Invalida");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void putOperationWithCellThatDoesNotExist() {
        try {
            fiubaSpreadSheet.createSheet("1").putCell("1", "A", 1, "=1!A2-1");
            assertEquals(fiubaSpreadSheet.getCellEvaluatedValue("1", "A", 1), "Formula Invalida");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void putOperationWithInvalidCellBecauseInvalidRow() {
        try {
            fiubaSpreadSheet.createSheet("1").putCell("1", "A", 1, "=1!A-2-1");
            assertEquals(fiubaSpreadSheet.getCellEvaluatedValue("1", "A", 1), "Formula Invalida");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void putOperationWithInvalidCellBecauseSheetDoesNotExist() {
        try {
            fiubaSpreadSheet.createSheet("1").putCell("1", "A", 1, "=Hojita!A2-1");
            assertEquals(fiubaSpreadSheet.getCellEvaluatedValue("1", "A", 1), "Formula Invalida");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    // @Test
    // public void putIntAndGetCellWithIdCell() {
    // try {
    // fiubaSpreadSheet.createSheet("1").putCell("1", "A", 1, "23");
    // assertEquals(fiubaSpreadSheet.getCellRawValue("1!A1"), "23");
    // } catch (InvalidCellIdentifierException e) {
    // fail();
    // }
    // }

    @Test
    public void undoPutNumberAndOperationSubstraction() {
        try {
            fiubaSpreadSheet.createSheet("1").putCell("1", "A", 1, "58")
                    .putCell("1", "A", 1, "=3-1");
            fiubaSpreadSheet.undo();
            assertEquals(fiubaSpreadSheet.getCellEvaluatedValue("1", "A", 1), "58");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void undoAndRedoPutOperationSubstraction() {
        try {
            fiubaSpreadSheet.createSheet("1").putCell("1", "A", 1, "=3-1");
            fiubaSpreadSheet.undo().redo();
            assertEquals(fiubaSpreadSheet.getCellEvaluatedValue("1", "A", 1), "2.0");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void testEvaluateCellValueOfFormulaWithReferencedAndUnreferencedCells() {
        String sheet1 = "sheet1";
        fiubaSpreadSheet.createSheet(sheet1);
        fiubaSpreadSheet.putCell(sheet1, "A", 1, "4.5");
        fiubaSpreadSheet.putCell(sheet1, "B", 2, "0.5");
        fiubaSpreadSheet.putCell(sheet1, "C", 3, "=A1 - B2");
        assertEquals(fiubaSpreadSheet.getCellEvaluatedValue(sheet1, "C", 3), "4.0");

        String sheet2 = "sheet2";
        fiubaSpreadSheet.createSheet(sheet2);
        fiubaSpreadSheet.putCell(sheet2, "A", 1, "1");
        String complexFormula = "=sheet1!C3 - A1 + 0.25 -1.75";
        fiubaSpreadSheet.putCell(sheet2, "B", 2, complexFormula);
        assertEquals(fiubaSpreadSheet.getCellEvaluatedValue(sheet2, "B", 2), "1.5");

        assertTrue(fiubaSpreadSheet.getCellRawValue(sheet2, "B", 2).equals(complexFormula));
    }
}
