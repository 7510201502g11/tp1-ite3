package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.command.Command;
import ar.fiuba.tdd.tp1.command.SheetDeleteCommand;
import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;
import ar.fiuba.tdd.tp1.exceptions.model.SheetNotFoundException;
import ar.fiuba.tdd.tp1.model.book.Book;
import ar.fiuba.tdd.tp1.model.book.CrpBook;
import ar.fiuba.tdd.tp1.model.cell.CrPair;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SheetDeleteCommandTest {
    private final Book<CrPair, String> book = (new CrpBook<String>()).createSheet("Hoja");

    @Test
    public void excecuteCommand() {
        Command command = new SheetDeleteCommand<CrPair, String>("Hoja", book);
        command.execute();
        assertFalse(book.existsSheet("Hoja"));
    }

    @Test
    public void excecuteAndUndoCommand() {
        Command command = new SheetDeleteCommand<CrPair, String>("Hoja", book);
        command.execute();
        command.undo();
        assertTrue(book.existsSheet("Hoja"));
    }

    @Test
    public void undoCommandWithoutExecute() {
        Command command = new SheetDeleteCommand<CrPair, String>("Hoja", book);
        command.undo();
        assertTrue(book.existsSheet("Hoja"));
    }

    @Test(expected = NullPointerException.class)
    public void commandWithNullParam() {
        new SheetDeleteCommand<CrPair, String>(null, book);
    }

    @Test(expected = SheetNotFoundException.class)
    public void excecuteCommandAndExistingSheetFound() throws InvalidCellIdentifierException {
        Command command = new SheetDeleteCommand<CrPair, String>("Hojita", book);
        command.execute();
    }

}
