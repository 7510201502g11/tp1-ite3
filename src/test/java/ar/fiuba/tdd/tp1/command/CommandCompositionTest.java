package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.exceptions.model.ExistingSheetFoundException;
import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;
import ar.fiuba.tdd.tp1.exceptions.model.InvalidSheetNameException;
import ar.fiuba.tdd.tp1.exceptions.model.SheetNotFoundException;
import ar.fiuba.tdd.tp1.model.book.Book;
import ar.fiuba.tdd.tp1.model.book.CrpBook;
import ar.fiuba.tdd.tp1.model.cell.CrPair;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CommandCompositionTest {

    private final Book<CrPair, String> book = new CrpBook<String>();

    @Test
    public void excecuteCommand() {
        Command command = new SheetCreateCommand<CrPair, String>("Hoja", book);
        Command command2 = new SheetCreateCommand<CrPair, String>("Hoja2", book);
        CommandComposition commandComposition = new CommandComposition();
        commandComposition.add(command).add(command2);
        commandComposition.execute();
        assertTrue(book.existsSheet("Hoja"));
        assertTrue(book.existsSheet("Hoja2"));
    }

    @Test
    public void excecuteAndUndoCommand() {
        Command command = new SheetCreateCommand<CrPair, String>("Hoja", book);
        Command command2 = new SheetCreateCommand<CrPair, String>("Hoja2", book);
        CommandComposition commandComposition = new CommandComposition();
        commandComposition.add(command).add(command2);
        commandComposition.execute();
        commandComposition.undo();
        assertFalse(book.existsSheet("Hoja"));
        assertFalse(book.existsSheet("Hoja2"));
    }

    @Test(expected = SheetNotFoundException.class)
    public void undoCommandWithoutExecute() {
        Command command = new SheetCreateCommand<CrPair, String>("Hoja", book);
        Command command2 = new SheetCreateCommand<CrPair, String>("Hoja2", book);
        CommandComposition commandComposition = new CommandComposition();
        commandComposition.add(command).add(command2);
        commandComposition.undo();
        assertFalse(book.existsSheet("Hoja"));
    }

    @Test(expected = InvalidSheetNameException.class)
    public void excecuteCommandWithInvalidName() throws InvalidCellIdentifierException {
        Command command = new SheetCreateCommand<CrPair, String>("", book);
        Command command2 = new SheetCreateCommand<CrPair, String>("Hoja2", book);
        CommandComposition commandComposition = new CommandComposition();
        commandComposition.add(command).add(command2);
        commandComposition.execute();
    }
    
    @Test(expected = ExistingSheetFoundException.class)
    public void excecuteCommandAndExistingSheetFound() throws InvalidCellIdentifierException {
        book.createSheet("Hoja");
        Command command = new SheetCreateCommand<CrPair, String>("Hoja", book);
        Command command2 = new SheetCreateCommand<CrPair, String>("Hoja2", book);
        CommandComposition commandComposition = new CommandComposition();
        commandComposition.add(command).add(command2);
        commandComposition.execute();
    }
    
}
