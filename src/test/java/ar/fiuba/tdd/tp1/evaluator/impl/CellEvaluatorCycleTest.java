package ar.fiuba.tdd.tp1.evaluator.impl;

import ar.fiuba.tdd.tp1.evaluator.CellEvaluator;
import ar.fiuba.tdd.tp1.evaluator.CellEvaluatorException;
import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;
import ar.fiuba.tdd.tp1.parser.Parser;
import ar.fiuba.tdd.tp1.parser.ParserBuilder;
import ar.fiuba.tdd.tp1.parser.impl.RangePairParserBuilder;
import ar.fiuba.tdd.tp1.parser.impl.StandardExcelParserBuilder;
import ar.fiuba.tdd.tp1.spreadsheet.CellVarIdAccessor;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class CellEvaluatorCycleTest {
    private static final Double DELTA = 0.0000000001;

    private CellVarIdAccessor<String> cellVarIdAccessor = new CellVarIdAccessor<String>() {
        @Override
        public String getCellRawValue(String cellId) {
            Map<String, String> vars = new HashMap<String, String>();
            vars.put("hoja1!A23", "1.5");
            vars.put("hoja2!B24", "23.687");
            vars.put("hoja3!C17", "hoja4!D3 + 9.98"); // -7.7844
            vars.put("hoja4!D3", "-17.7644");
            vars.put("hoja5!E6", "hoja3!C17 + hoja2!B24 - hoja1!A23"); // 14.4026

            vars.put("default!A1", "2.25");
            vars.put("default!A2", "= A1 + 0.75"); // 3
            vars.put("default!CB34", "= A2 - 0.5"); // 2.5
            vars.put("default!C6", "4.66");
            vars.put("default!B5", "= hoja1!A23 - A2 + 1.5 + C6"); //4.66 

            vars.put("default!G1", "1.0");
            vars.put("default!G2", "2.55");
            vars.put("default!G3", "5.0");

            vars.put("default!G4", "G1 - G3 + G5");
            vars.put("default!G5", "G2 - G4");
            
            vars.put("default!H1", "4.0 + H2");
            vars.put("default!H2", "5.14 - H1");

            return vars.get(cellId);
        }
    };

    static final RangePairParserBuilder parserBuilder = new RangePairParserBuilder();

    @Test(expected = CycleDetectedException.class)
    public void testDetectCycle() throws CellEvaluatorException {
        String defaultPointer = "default";
        CellEvaluatorImpl cellEvaluator = new CellEvaluatorImpl()
                .defSheetName(defaultPointer);

        String toParse = "=G5";
        cellEvaluator.getCellValueAsDouble(toParse, cellVarIdAccessor, parserBuilder);
    }
    
    @Test(expected = CycleDetectedException.class)
    public void testDetectOwnCycle() throws CellEvaluatorException {
        String defaultPointer = "default";
        CellEvaluatorImpl cellEvaluator = new CellEvaluatorImpl()
                .defSheetName(defaultPointer);

        String toParse = "=17.0 + H2";
        cellEvaluator.getCellValueAsDouble(toParse, cellVarIdAccessor, parserBuilder);
    }
}
