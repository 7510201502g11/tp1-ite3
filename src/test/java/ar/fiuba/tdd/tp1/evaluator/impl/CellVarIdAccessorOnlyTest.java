package ar.fiuba.tdd.tp1.evaluator.impl;

import ar.fiuba.tdd.tp1.spreadsheet.CellVarIdAccessor;

import java.util.HashMap;

/**
 * Esta clase solo sirve para pruebas de CellInterpreter. Guarda los siguientes valores: "hoja1!A1"
 * -> "2" "hoja1!A2" -> "=hoja1!A1" "hoja1!A3" -> "=hoja1!A1+5" "2!A3" -> "=hoja1!A1+5" "2!A4" ->
 * "=2!A3+1"
 */
public class CellVarIdAccessorOnlyTest implements CellVarIdAccessor<String> {

    private HashMap<String, String> book;

    public CellVarIdAccessorOnlyTest() {
        book = new HashMap<String, String>();
        book.put("hoja1!A1", "2");
        book.put("hoja1!A2", "=hoja1!A1");
        book.put("hoja1!A3", "=hoja1!A1+5");
        book.put("2!A3", "=hoja1!A1+5");
        book.put("2!A4", "=2!A3+1");
    }

    @Override
    public String getCellRawValue(String cellId) {
        // TODO ver que pasa cuando pedimos una celda que no existe en el book
        // Optional.ofNullable(book.get(cellId)).orElse();
        return book.get(cellId);
    }

}
