package ar.fiuba.tdd.tp1.evaluator.impl;

import ar.fiuba.tdd.tp1.evaluator.CellInterpreter;
import ar.fiuba.tdd.tp1.evaluator.CellInterpreterImpl;
import ar.fiuba.tdd.tp1.spreadsheet.CellVarIdAccessor;

import org.junit.Test;

import static org.junit.Assert.*;

public class CellInterpreterTest {

    public final CellInterpreter cellInterpreter = new CellInterpreterImpl();
    public final CellVarIdAccessor<String> cellAccessor = new CellVarIdAccessorOnlyTest();

    @Test
    public void evaluateText() {
        assertEquals(cellInterpreter.getCellValue("Buenos dias", cellAccessor), "Buenos dias");
    }

    @Test
    public void evaluateInt() {
        assertEquals(cellInterpreter.getCellValue("23", cellAccessor), "23");
    }

    @Test
    public void evaluateDouble() {
        assertEquals(cellInterpreter.getCellValue("4.59", cellAccessor), "4.59");
    }

    @Test
    public void evaluateOperationAdditionTwoNumber() {
        assertEquals(cellInterpreter.getCellValue("=2+1", cellAccessor), "3.0");
    }

    @Test
    public void evaluateOperationAdditionWithOtherCell() {
        assertEquals(cellInterpreter.getCellValue("=hoja1!A1+1", cellAccessor), "3.0");
    }

    @Test
    public void evaluateOperationAdditionWithOtherCellWithNumberNameSheet() {
        assertEquals(cellInterpreter.getCellValue("=2!A4+1", cellAccessor), "9.0");
    }

    @Test
    public void evaluateOperationSubtractionTwoNumber() {
        String result = cellInterpreter.getCellValue("=2-1", cellAccessor);
        assertEquals(result, "1.0");
    }

    @Test
    public void evaluateOperationSubtractionWithOtherCell() {
        assertEquals(cellInterpreter.getCellValue("=hoja1!A1-1", cellAccessor), "1.0");
    }

    @Test
    public void evaluateOperationAdditionTwoCell() {
        assertEquals(cellInterpreter.getCellValue("=hoja1!A1+hoja1!A3", cellAccessor), "9.0");
    }

    @Test
    public void evaluateOperationInvalid() {
        assertEquals(cellInterpreter.getCellValue("=Formula()", cellAccessor),
                "Formula Invalida");
    }

    @Test
    public void evaluateOperationWithCellThatDoesNotExist() {
        assertEquals(cellInterpreter.getCellValue("=hoja1!Z59+1", cellAccessor),
                "Formula Invalida");
    }

    @Test
    public void evaluateOperationWithCellThatDoesNotExistSheet() {
        assertEquals(cellInterpreter.getCellValue("=hoja456!A1+1", cellAccessor),
                "Formula Invalida");
    }

    @Test
    public void evaluateOperationWithInvalidCell() {
        assertEquals(cellInterpreter.getCellValue("=hoja1!A-1+1", cellAccessor),
                "Formula Invalida");
    }

}
