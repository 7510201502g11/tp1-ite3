package ar.fiuba.tdd.tp1.evaluator.impl;

import ar.fiuba.tdd.tp1.evaluator.CellEvaluator;
import ar.fiuba.tdd.tp1.evaluator.CellEvaluatorException;
import ar.fiuba.tdd.tp1.parser.impl.RangePairParserBuilder;
import ar.fiuba.tdd.tp1.spreadsheet.CellVarIdAccessor;

import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class CellEvaluatorRangePairTest {
    private static final Double DELTA = 0.0000000001;

    private CellVarIdAccessor<String> cellVarIdAccessor = new CellVarIdAccessor<String>() {
        @Override
        public String getCellRawValue(String cellId) {
            Map<String, String> vars = new HashMap<String, String>();
            vars.put("hoja1!A1", "1.5");

            vars.put("hoja1!G1", "19.25");
            vars.put("hoja1!G2", "97.4567");
            vars.put("hoja1!G3", "78.87");
            vars.put("hoja1!G4", "25.73");

            vars.put("default!G1", "1.0");
            vars.put("default!G2", "2.55");
            vars.put("default!G3", "5.0");
            vars.put("default!H1", "12.45");
            vars.put("default!H2", "9.123");
            vars.put("default!H3", "8");
            vars.put("default!I1", "-4.2");
            vars.put("default!I2", "-6.45");
            vars.put("default!I3", "15.67");

            vars.put("default!J1", "G1 + G3 - G2 + 125.23");
            vars.put("default!J2", "0.0");
            vars.put("default!J3", "1.1");

            return vars.get(cellId);
        }
    };

    @Test
    public void testMixOperationsAndFormulas() throws CellEvaluatorException {
        // vars.put("default!G1", "1.0");
        // vars.put("default!G2", "2.55");
        // vars.put("default!G3", "5.0");

        String defaultPointer = "default";
        RangePairParserBuilder parserBuilder = new RangePairParserBuilder();
        String toParse = "= J1 + 17.564";
        CellEvaluator cellEvaluator = new CellEvaluatorImpl(defaultPointer);

        Double expected = 1.0 + 5.0 - 2.55 + 125.23 + 17.564;
        assertEquals(expected,
                cellEvaluator.getCellValueAsDouble(toParse, cellVarIdAccessor, parserBuilder),
                DELTA);

        toParse = "=MAX(H1:J2) - hoja1!G1";
        expected = expected - 17.564 - 19.25;
        assertEquals(expected,
                cellEvaluator.getCellValueAsDouble(toParse, cellVarIdAccessor, parserBuilder),
                DELTA);
    }

    @Test
    public void testParseAndGetValueOfMax() throws CellEvaluatorException {
        String defaultPointer = "default";
        RangePairParserBuilder parserBuilder = new RangePairParserBuilder();
        String toParse = "= MAX(G1:G3)";

        CellEvaluator cellEvaluator = new CellEvaluatorImpl(defaultPointer);
        double expected = 5.0;
        Double actual = cellEvaluator.getCellValueAsDouble(toParse, cellVarIdAccessor,
                parserBuilder);
        assertEquals(expected, actual, DELTA);
    }

    @Test
    public void testGetValueOfMaxSingleColumn() throws CellEvaluatorException {
        String defaultPointer = "default";
        RangePairParserBuilder parserBuilder = new RangePairParserBuilder();
        String toParse = "= MAX(G1:I1)";

        CellEvaluator cellEvaluator = new CellEvaluatorImpl(defaultPointer);
        double expected = 12.45;
        Double actual = cellEvaluator.getCellValueAsDouble(toParse, cellVarIdAccessor,
                parserBuilder);
        assertEquals(expected, actual, DELTA);
    }

    @Test
    public void testGetMaxOfBlockRange() throws CellEvaluatorException {
        String defaultPointer = "";
        RangePairParserBuilder parserBuilder = new RangePairParserBuilder();
        String toParse = "= MAX(default!G1:default!I3)";

        CellEvaluator cellEvaluator = new CellEvaluatorImpl(defaultPointer);
        double expected = 15.67;
        Double actual = cellEvaluator.getCellValueAsDouble(toParse, cellVarIdAccessor,
                parserBuilder);
        assertEquals(expected, actual, DELTA);
    }

    @Test
    public void testParseAndGetValueOfMin() throws CellEvaluatorException {
        String defaultPointer = "default";
        RangePairParserBuilder parserBuilder = new RangePairParserBuilder();
        String toParse = "= MIN(G1:G3)";

        CellEvaluator cellEvaluator = new CellEvaluatorImpl(defaultPointer);
        double expected = 1.0;
        Double actual = cellEvaluator.getCellValueAsDouble(toParse, cellVarIdAccessor,
                parserBuilder);
        assertEquals(expected, actual, DELTA);
    }

    @Test
    public void testGetMinOfBlockRange() throws CellEvaluatorException {
        String defaultPointer = "";
        RangePairParserBuilder parserBuilder = new RangePairParserBuilder();
        String toParse = "= MIN(default!G1:default!I3)";

        CellEvaluator cellEvaluator = new CellEvaluatorImpl(defaultPointer);
        double expected = -6.45;
        Double actual = cellEvaluator.getCellValueAsDouble(toParse, cellVarIdAccessor,
                parserBuilder);
        assertEquals(expected, actual, DELTA);
    }

    @Test
    public void testGetValueOfMinSingleColumn() throws CellEvaluatorException {
        String defaultPointer = "default";
        RangePairParserBuilder parserBuilder = new RangePairParserBuilder();
        String toParse = "= MIN(G1:I1)";

        CellEvaluator cellEvaluator = new CellEvaluatorImpl(defaultPointer);
        double expected = -4.2;
        Double actual = cellEvaluator.getCellValueAsDouble(toParse, cellVarIdAccessor,
                parserBuilder);
        assertEquals(expected, actual, DELTA);
    }

    @Test
    public void testGetValueOfMaxAndMixOtherOperations() throws CellEvaluatorException {
        String defaultPointer = "default";
        RangePairParserBuilder parserBuilder = new RangePairParserBuilder();
        String toParse = "= MAX(G1:G3) - hoja1!A1 + 0.5";

        CellEvaluator cellEvaluator = new CellEvaluatorImpl(defaultPointer);
        double expected = 4.0;
        Double actual = cellEvaluator.getCellValueAsDouble(toParse, cellVarIdAccessor,
                parserBuilder);
        assertEquals(expected, actual, DELTA);
    }

    @Test
    public void testGetAverage() throws CellEvaluatorException {
        /*
         * vars.put("hoja1!G1", "19.25"); vars.put("hoja1!G2", "97.4567"); vars.put("hoja1!G3",
         * "78.87"); vars.put("hoja1!G4", "25.73");
         */
        Double[] arr = { 19.25, 97.4567, 78.87, 25.73 };
        List<Double> doubles = Arrays.asList(arr);
        double average = doubles.stream().mapToDouble(d -> d).average().getAsDouble();

        String defaultPointer = "hoja1";
        RangePairParserBuilder parserBuilder = new RangePairParserBuilder();
        String toParse = "= AVERAGE(G1:G4)";

        CellEvaluator cellEvaluator = new CellEvaluatorImpl(defaultPointer);
        double expected = average;
        Double actual = cellEvaluator.getCellValueAsDouble(toParse, cellVarIdAccessor,
                parserBuilder);
        assertEquals(expected, actual, DELTA);

    }

    @Test(expected = CellEvaluatorException.class)
    public void testGetAverageAndFail() throws CellEvaluatorException {
        String defaultPointer = "hoja1";
        RangePairParserBuilder parserBuilder = new RangePairParserBuilder();
        String toParse = "= AVERAGE(G1:G5)";

        CellEvaluator cellEvaluator = new CellEvaluatorImpl(defaultPointer);
        cellEvaluator.getCellValueAsDouble(toParse, cellVarIdAccessor, parserBuilder);
    }
}
