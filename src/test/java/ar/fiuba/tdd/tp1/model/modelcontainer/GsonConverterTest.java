package ar.fiuba.tdd.tp1.model.modelcontainer;

import com.google.gson.Gson;

import ar.fiuba.tdd.tp1.format.formatter.FDate;
import ar.fiuba.tdd.tp1.model.cell.Cell;
import ar.fiuba.tdd.tp1.model.cell.CrPair;
import ar.fiuba.tdd.tp1.model.cell.CrpCell;
import ar.fiuba.tdd.tp1.spreadsheet.ContentAndFormatterSpreadSheet;
import ar.fiuba.tdd.tp1.spreadsheet.ContentAndFormatterSpreadSheetImpl;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class GsonConverterTest {

    public final ContentAndFormatterSpreadSheet spreadSheet = new ContentAndFormatterSpreadSheetImpl();

    @Test
    public void convertCellToJsonTest() {
        CrpCell cellTest = new CrpCell(new CrPair(1, "A"), "testValue");
        CellModel cellModel = new CellModel(cellTest);
        cellModel.setSheetName("aSheet");

        final Gson gson = new Gson();
        final String JsonRepresentation = gson.toJson(cellModel);
        assertEquals(
                "{\"sheet\":\"aSheet\",\"id\":\"A1\",\"value\":\"testValue\",\"type\":\"\",\"formatter\":{}}",
                JsonRepresentation);
    }

    @Test
    public void convertSpreadSheetToJsonTest() {
        // creo una spreadsheet
        createSpreadSheet();

        SpreadSheetModel ssModel = new SpreadSheetModel();

        // mapa de valores de celdas
        HashMap<String, Map<CrPair, Cell<CrPair, String>>> mapCellValues = spreadSheet
                .getCellsFormattedInBook();

        ssModel.setName("test.jss");
        ssModel.setVersion("1.1");

        List<CellModel> cellList = new ArrayList<CellModel>();

        mapCellValues.forEach((sheet, contenedor) -> {
                contenedor.forEach((id, cell) -> {
                        CellModel cellModel = new CellModel();
                        cellModel.setCellId(id.toString());
                        cellModel.setSheetName(sheet);
                        cellModel.setValue(cell.getRawValue());
                        cellModel.setType("");
                        cellList.add(cellModel);
                    }
                );
            }
        );

        ssModel.setCells(cellList);

        Gson gson = new Gson();
        String jsonRepresentation = gson.toJson(ssModel);

        assertEquals("{\"name\":\"test.jss\",\"version\":\"1.1\",\"cells\":[{\"sheet\":\"Hoja1\","
                + "\"id\":\"A2\",\"value\":\"26.346\",\"type\":\"\",\"formatter\":{}},"
                + "{\"sheet\":\"Hoja1\",\"id\":\"A1\",\"value\":\"$ 23.00\",\"type\":\"\","
                + "\"formatter\":{}},{\"sheet\":\"Hoja2\",\"id\":\"A3\",\"value\":\"01-13-2000\","
                + "\"type\":\"\",\"formatter\":{}},{\"sheet\":\"Hoja3\",\"id\":\"B1\","
                + "\"value\":\"Hola\",\"type\":\"\",\"formatter\":{}}]}", jsonRepresentation);

        // System.out.println(JsonRepresentation);

    }

    private void createSpreadSheet() {
        FDate date = new FDate(2000, 1, 13);
        long numberDate = date.date();
        spreadSheet.createSheet("Hoja1").createSheet("Hoja2").createSheet("Hoja3");
        spreadSheet.putCellRawValue("Hoja1", "A", 1, "23").putCellFormatNumber("Hoja1", "A", 1, 2)
                .putCellFormatMoney("Hoja1", "A", 1, "$");
        spreadSheet.putCellRawValue("Hoja1", "A", 2, "26.3458").putCellFormatNumber("Hoja1", "A",
                2, 3);
        spreadSheet.putCellRawValue("Hoja2", "A", 3, "" + numberDate).putCellFormatDate("Hoja2",
                "A", 3, "MM-dd-yyyy");
        spreadSheet.putCellRawValue("Hoja3", "B", 1, "Hola");
    }

}
