package ar.fiuba.tdd.tp1.model.cell;

import org.junit.Test;

import static org.junit.Assert.*;

public class CrPairTest {

    CrPair anId = null;

    @Test
    public void testCreateCrPair() {
        anId = new CrPair(1, "A");

        assertEquals(anId.getColumn(), "A");
        assertEquals(anId.getRow(), 1);
    }

    @Test
    public void testGetValidToStringFormat() {
        anId = new CrPair(1, "A");

        assertEquals(anId.toString(), "A1");
    }

    @Test
    public void testEqualPairs() {
        anId = new CrPair(1, "A");
        CrPair anotherId = new CrPair(1, "A");

        assertEquals(anId.equals(anotherId), true);
    }

    @Test
    public void testPairsNotEquals() {
        anId = new CrPair(1, "A");
        CrPair anotherId = new CrPair(2, "A");

        assertEquals(anId.equals(anotherId), false);

        assertNotEquals(anId, "something else");
    }

    @Test
    public void testCreateFromString() {
        CrPair crpair = CrPair.create("A23");
        assertEquals(23, crpair.getRow());
        assertEquals("A", crpair.getColumn());
    }
}
