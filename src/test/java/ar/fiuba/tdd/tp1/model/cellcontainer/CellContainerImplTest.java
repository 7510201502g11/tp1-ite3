package ar.fiuba.tdd.tp1.model.cellcontainer;

import ar.fiuba.tdd.tp1.model.cell.Cell;
import ar.fiuba.tdd.tp1.model.cell.CellImpl;
import ar.fiuba.tdd.tp1.model.cell.CrPair;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class CellContainerImplTest {

    CellContainerImpl<CrPair, String> cellContainerImpl = null;

    @Test
    public void testPutCellCellOfCellIdType() {
        cellContainerImpl = new CellContainerImpl<CrPair, String>();

        Cell<CrPair, String> someCell = new CellImpl<CrPair, String>(new CrPair(2, "A"));
        cellContainerImpl.putCell(someCell);

        assertEquals(cellContainerImpl.getCell(new CrPair(2, "A")).equals(someCell), true);
    }

    // @Test
    // public void testPutCellStringCellIdType() {
    // cellContainerImpl = new CellContainerImpl<CrPair,String>();
    //
    // cellContainerImpl.putCell("testValue", new CrPair(4, "B"));
    //
    // assertEquals(cellContainerImpl.getCell(new CrPair(4, "B")).getRawValue(), "testValue");
    //
    // }

    @Test
    public void testRemoveCellCellIdType() {
        cellContainerImpl = new CellContainerImpl<CrPair, String>();

        Cell<CrPair, String> cell1 = cellContainerImpl.putCell(new CellImpl<CrPair, String>(
                new CrPair(4, "B"), "testValue"));

        Cell<CrPair, String> cell3 = new CellImpl<CrPair, String>(new CrPair(4, "B"), "testValue");
        assertEquals(cell3, cell1);

        assertEquals(cellContainerImpl.removeCell(new CrPair(4, "B")),
                new CellImpl<CrPair, String>(new CrPair(4, "B"), "testValue"));
    }

    @Test
    public void testRemoveCellCellOfCellIdType() {
        cellContainerImpl = new CellContainerImpl<CrPair, String>();

        Cell<CrPair, String> cell = new CellImpl<CrPair, String>(new CrPair(4, "B"), "testValue");

        Cell<CrPair, String> cell1 = cellContainerImpl.putCell(cell);

        assertEquals(cellContainerImpl.removeCell(cell1.getId()), cell1);

    }

    @Test
    public void testPutDifferentRawValuesInSameCell() {
        cellContainerImpl = new CellContainerImpl<CrPair, String>();

        CrPair cellId = new CrPair(4, "B");

        cellContainerImpl.putCell(new CellImpl<CrPair, String>(cellId, "testValue"));

        cellContainerImpl.putCell(new CellImpl<CrPair, String>(cellId, "anotherTestValue"));

        assertEquals(cellContainerImpl.getCell(cellId).getRawValue(), "anotherTestValue");
    }
    
    @Test
    public void testGetCells() {
        cellContainerImpl = new CellContainerImpl<CrPair, String>();

        CrPair cellId = new CrPair(4, "B");

        cellContainerImpl.putCell(new CellImpl<CrPair, String>(cellId, "testValue"));

        cellContainerImpl.putCell(new CellImpl<CrPair, String>(new CrPair(2, "B"), "anotherTestValue"));

        Map<CrPair, Cell<CrPair, String>> cells = cellContainerImpl.getCells();
        
        assertEquals(cells.get(cellId).getRawValue(), "testValue");
        assertEquals(cells.get(new CrPair(2, "B")).getRawValue(), "anotherTestValue");
    }
    
    @Test
    public void testAllMapKeysFromContainer() {
        cellContainerImpl = new CellContainerImpl<CrPair, String>();

        CrPair cellId = new CrPair(4, "B");
        
        List<CrPair> idList = new ArrayList<CrPair>();

        cellContainerImpl.putCell(new CellImpl<CrPair, String>(cellId, "testValue"));

        cellContainerImpl.putCell(new CellImpl<CrPair, String>(new CrPair(2, "B"), "anotherTestValue"));

        Map<CrPair, Cell<CrPair, String>> cells = cellContainerImpl.getCells();
        
        for (Cell<CrPair, String> cell : cells.values()) {
            idList.add(cell.getId());
        }
        
        assertEquals(idList.get(1), cellId);
        assertEquals(idList.get(0), new CrPair(2, "B"));
    }

}
