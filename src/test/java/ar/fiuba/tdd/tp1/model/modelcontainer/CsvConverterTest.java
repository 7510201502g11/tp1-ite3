package ar.fiuba.tdd.tp1.model.modelcontainer;

import ar.fiuba.tdd.tp1.model.cell.CrPair;
import ar.fiuba.tdd.tp1.model.cell.CrpCell;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class CsvConverterTest {

    @Test
    public void convertCellToCsvTest() {
        CrpCell cellTest = new CrpCell(new CrPair(1, "A"), "testValue");
        CrpCell cellTest1 = new CrpCell(new CrPair(2, "A"), "testValue1");
        List<CrpCell> cellList = new ArrayList<CrpCell>();
        cellList.add(cellTest);
        cellList.add(cellTest1);
        
        String csvRepresentation = "";
        
        for (CrpCell cell : cellList) {
            csvRepresentation += cell.getRawValue().trim() + ";";
        }
        
        assertEquals("testValue;testValue1;", csvRepresentation);
    }

}
