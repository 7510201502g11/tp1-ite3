package ar.fiuba.tdd.tp1.model.cell;

import org.junit.Test;

import static org.junit.Assert.*;

public class CrpCellTest {

    CrpCell someCrpCell = null;

    @Test
    public void createCrpCellwithIdTest() {
        someCrpCell = new CrpCell(new CrPair(1, "A"));

        assertEquals(someCrpCell.getId().getColumn(), "A");
        assertEquals(someCrpCell.getId().getRow(), 1);

        assertEquals(someCrpCell.getRawValue(), "");
    }

    @Test
    public void createCrpCellwithIdAndRawValueTest() {
        someCrpCell = new CrpCell(new CrPair(1, "A"), "test");

        assertEquals(someCrpCell.getId().getColumn(), "A");
        assertEquals(someCrpCell.getId().getRow(), 1);

        assertEquals(someCrpCell.getRawValue(), "test");
    }

    @Test
    public void setRawValueTest() {
        someCrpCell = new CrpCell(new CrPair(1, "A"));

        someCrpCell.setRawValue("someValue");

        assertEquals(someCrpCell.getRawValue(), "someValue");
    }

}
