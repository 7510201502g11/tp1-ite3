package ar.fiuba.tdd.tp1.model.sheet;

import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;
import ar.fiuba.tdd.tp1.model.cellcontainer.CrpCellContainer;

import org.junit.Test;

import static org.junit.Assert.*;

public class CrpSheetTest {

    CrpSheet<String> testSheet = null;

    @Test
    public void testCrpSheet() {
        testSheet = new CrpSheet<String>("hojaTest", new CrpCellContainer<String>());        
        assertEquals(testSheet.getSheetName(), "hojaTest");
    }

    @Test
    public void testPutCell() {
        testSheet = new CrpSheet<String>("hojaTest", new CrpCellContainer<String>());

        testSheet.putCell("C", 1, "valueTest");

        assertEquals(testSheet.getCell("C", 1).getRawValue(), "valueTest");
        assertNotEquals(testSheet.getCell("C", 1), null);
    }

    @Test
    public void testRenameSheet() {
        testSheet = new CrpSheet<String>("hojaTest", new CrpCellContainer<String>());

        testSheet.renameSheet("nuevoNombreTest");

        assertEquals(testSheet.getSheetName(), "nuevoNombreTest");
    }

    @Test
    public void testClearCell() {
        testSheet = new CrpSheet<String>("hojaTest", new CrpCellContainer<String>());

        testSheet.putCell("C", 2, "valueTest");
        testSheet.clearCell("C", 2);

        assertEquals(testSheet.getCellRawValue("C", 2), null);
    }

    @Test
    public void testGetCellRawValue() {
        testSheet = new CrpSheet<String>("hojaTest", new CrpCellContainer<String>());

        testSheet.putCell("C", 2, "valueTest");

        assertEquals(testSheet.getCellRawValue("C", 2), "valueTest");
    }

    @Test
    public void testSetCellContainer() {
        CrpCellContainer<String> oneCellContainer = new CrpCellContainer<String>();
        CrpCellContainer<String> anotherCellContainer = new CrpCellContainer<String>();

        testSheet = new CrpSheet<String>("hojaTest", oneCellContainer);
        testSheet.putCell("C", 2, "valueTest");

        testSheet.setCellContainer(anotherCellContainer);

        assertEquals(testSheet.getCellRawValue("C", 2), null);
    }

    @Test(expected = InvalidCellIdentifierException.class)
    public void getInvalidCellRawValue() throws InvalidCellIdentifierException {
        testSheet = new CrpSheet<String>("hojaTest", new CrpCellContainer<String>());

        testSheet.getCellRawValue("53B8", -5);
    }

    @Test(expected = InvalidCellIdentifierException.class)
    public void putInvalidCellInSheet() throws InvalidCellIdentifierException {
        testSheet = new CrpSheet<String>("hojaTest", new CrpCellContainer<String>());

        testSheet.putCell("53B8", -5, "fail!");
    }

}
