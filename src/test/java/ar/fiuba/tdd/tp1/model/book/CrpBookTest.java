package ar.fiuba.tdd.tp1.model.book;

import ar.fiuba.tdd.tp1.exceptions.model.ExistingSheetFoundException;
import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;
import ar.fiuba.tdd.tp1.exceptions.model.InvalidSheetNameException;
import ar.fiuba.tdd.tp1.exceptions.model.SheetNotFoundException;
import ar.fiuba.tdd.tp1.model.cell.Cell;
import ar.fiuba.tdd.tp1.model.cell.CrPair;

import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class CrpBookTest {

    CrpBook<String> testBook = null;

    @Test
    public void testCreateSheet() {
        testBook = new CrpBook<String>();

        testBook.createSheet("sheetNameTest");

        assertNotEquals(testBook.getSheet("sheetNameTest"), null);
    }

    @Test(expected = InvalidSheetNameException.class)
    public void createEmptySheetTest() {
        testBook = new CrpBook<String>();

        testBook.createSheet("");
    }

    @Test(expected = ExistingSheetFoundException.class)
    public void createSheetWithSameNameTest() {
        testBook = new CrpBook<String>();

        testBook.createSheet("testName");
        testBook.createSheet("testName");
    }

    @Test
    public void testRemoveSheet() {
        testBook = new CrpBook<String>();

        testBook.createSheet("testName");
        testBook.removeSheet("testName");

        assertEquals(testBook.getSheet("testName"), null);
    }

    @Test(expected = SheetNotFoundException.class)
    public void testRemoveNotExistingSheet() {
        testBook = new CrpBook<String>();

        testBook.removeSheet("testName");
    }

    @Test(expected = ExistingSheetFoundException.class)
    public void testRenameWithNewInvalidName() {
        testBook = new CrpBook<String>();

        testBook.createSheet("testName");
        testBook.createSheet("anotherTestName");

        testBook.renameSheet("testName", "anotherTestName");
    }

    @Test
    public void testGetSheet() {
        testBook = new CrpBook<String>();

        testBook.createSheet("testName");

        assertNotEquals(testBook.getSheet("testName"), null);
    }

    @Test
    public void testGetSheetNotFound() {
        testBook = new CrpBook<String>();

        assertEquals(testBook.getSheet("testName"), null);
    }

    @Test
    public void testPutCell() {
        testBook = new CrpBook<String>();

        testBook.createSheet("testName");

        assertEquals(
                testBook.putCell("testName", "B", 1, "testValue").getCellRawValue("testName", "B",
                        1), "testValue");
    }

    @Test(expected = SheetNotFoundException.class)
    public void testPutCellInUnexistingSheet() {
        testBook = new CrpBook<String>();

        testBook.putCell("testName", "B", 1, "testValue");
    }

    @Test
    public void testClearCell() {
        testBook = new CrpBook<String>();

        testBook.createSheet("testName");

        testBook.putCell("testName", "A", 8, "testValue");

        assertEquals(testBook.getCellRawValue("testName", "A", 8), "testValue");

        testBook.clearCell("testName", "A", 8);

        assertEquals(testBook.getCellRawValue("testName", "A", 8), null);
    }

    @Test(expected = SheetNotFoundException.class)
    public void testClearCellWithUnexistingSheet() {
        testBook = new CrpBook<String>();

        testBook.clearCell("testName", "A", 8);
    }

    @Test
    public void testGetCellRawValue() {
        testBook = new CrpBook<String>();

        testBook.createSheet("testName");

        testBook.putCell("testName", "A", 8, "testValue");

        assertEquals(testBook.getCellRawValue("testName", "A", 8), "testValue");
    }

    @Test(expected = SheetNotFoundException.class)
    public void testGetCellRawValueFromUnexistingSheet() {
        testBook = new CrpBook<String>();

        testBook.getCellRawValue("testName", "A", 8);
    }

    @Test(expected = InvalidCellIdentifierException.class)
    public void testGetCellRawValueInvalidId() {
        testBook = new CrpBook<String>();

        testBook.createSheet("testName");

        testBook.getCellRawValue("testName", "A35", 8);
    }

    @Test
    public void testGetCellRawValueString() {
        testBook = new CrpBook<String>();

        testBook.createSheet("testName");

        testBook.putCell("testName", "A", 8, "testValue");

        assertEquals(testBook.getCellRawValue("testName!A8"), "testValue");
    }

    @Test(expected = InvalidCellIdentifierException.class)
    public void testGetCellRawValueStringInvalidId() {
        testBook = new CrpBook<String>();

        testBook.createSheet("testName");

        testBook.getCellRawValue("testNameA8");
    }

    @Test
    public void testRenameSheet() {
        testBook = new CrpBook<String>();

        testBook.createSheet("testName");

        assertNotEquals(testBook.getSheet("testName"), null);

        testBook.renameSheet("testName", "newNameTest");

        assertEquals(testBook.getSheet("testName"), null);
        assertNotEquals(testBook.getSheet("newNameTest"), null);
    }

    @Test(expected = SheetNotFoundException.class)
    public void testRenameUnexistingSheet() {
        testBook = new CrpBook<String>();

        testBook.renameSheet("testName", "newNameTest");
    }

    @Test
    public void testGetNamesSheets() {
        testBook = new CrpBook<String>();
        testBook.createSheet("testName");
        testBook.createSheet("testName2");
        List<String> list = testBook.getNamesSheet();
        assertEquals(list.get(0), "testName");
        assertEquals(list.get(1), "testName2");
    }
    
    @Test
    public void testGetCellsInBook() {
        testBook = new CrpBook<String>();
        testBook.createSheet("testName1");
        testBook.createSheet("testName2");
        testBook.putCell("testName1", "A", 1, "valor1");
        testBook.putCell("testName1", "A", 2, "valor2");
        testBook.putCell("testName2", "B", 1, "valor3");
        testBook.putCell("testName2", "B", 2, "valor4");
        
        HashMap<String, Map<CrPair, Cell<CrPair, String>>> bookMap = testBook.getCellsInBook();
        
        assertEquals(bookMap.get("testName1").get(new CrPair(1, "A")).getRawValue(), "valor1");
        assertEquals(bookMap.get("testName1").get(new CrPair(2, "A")).getRawValue(), "valor2");
        assertEquals(bookMap.get("testName2").get(new CrPair(1, "B")).getRawValue(), "valor3");
        assertEquals(bookMap.get("testName2").get(new CrPair(2, "B")).getRawValue(), "valor4");
    }

}
