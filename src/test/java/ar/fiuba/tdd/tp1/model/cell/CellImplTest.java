package ar.fiuba.tdd.tp1.model.cell;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class CellImplTest {

    @Test
    public void testEqualsObject() {
        CellImpl<Integer, String> cell1 = new CellImpl<Integer, String>(1, new String("value1"));
        CellImpl<Integer, String> cell2 = new CellImpl<Integer, String>(1, new String("value1"));

        assertEquals(cell1, cell2);

        assertNotEquals(cell1, new String("asd"));

        assertNotEquals(cell1, new CellImpl<Integer, String>(1, new String("value2")));

        assertNotEquals(cell1, new CellImpl<Integer, String>(2, new String("value1")));
    }

    @Test
    public void testHashCode() {
        CellImpl<Integer, String> cell1 = new CellImpl<Integer, String>(1, new String("value1"));
        Map<CellImpl<Integer, String>, Object> map = new HashMap<CellImpl<Integer, String>, Object>();
        map.put(cell1, cell1);
        assertEquals(cell1, map.get(cell1));
    }
}
