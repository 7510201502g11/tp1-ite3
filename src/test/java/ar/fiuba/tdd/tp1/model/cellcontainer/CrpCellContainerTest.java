package ar.fiuba.tdd.tp1.model.cellcontainer;

import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;
import ar.fiuba.tdd.tp1.model.cell.Cell;
import ar.fiuba.tdd.tp1.model.cell.CrPair;
import ar.fiuba.tdd.tp1.model.cell.CrpCell;

import org.junit.Test;

import static org.junit.Assert.*;

public class CrpCellContainerTest {

    CrpRawValueCellContainerImpl cellContainer = null;

    @Test
    public void putValidCellInContainerTest() {
        CrpCell someCell = new CrpCell(new CrPair(1, "A"), "test");
        cellContainer = new CrpRawValueCellContainerImpl();

        cellContainer.putCell(someCell);

        try {
            CrPair cellIdVal = new CrPair(1, "A");
            Cell<CrPair, String> cell = cellContainer.getCell(cellIdVal);
            assertEquals(cell.equals(someCell), true);
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test(expected = InvalidCellIdentifierException.class)
    public void putInvalidCellInContainer() throws InvalidCellIdentifierException {
        cellContainer = new CrpRawValueCellContainerImpl();

        cellContainer.putCell("test", new CrPair(-5, "53B8"));
    }

    @Test(expected = InvalidCellIdentifierException.class)
    public void getInvalidCellInContainer() throws InvalidCellIdentifierException {
        cellContainer = new CrpRawValueCellContainerImpl();

        cellContainer.getCell(new CrPair(-5, "53B8"));
    }

    @Test
    public void getNotExistingCell() {
        cellContainer = new CrpRawValueCellContainerImpl();
        Cell<CrPair, String> cell = null;

        try {
            cell = cellContainer.getCell(new CrPair(2, "B"));
        } catch (InvalidCellIdentifierException e) {
            fail();
        }

        assertEquals(cell.getId().getRow(), 2);
        assertEquals(cell.getId().getColumn(), "B");
        assertEquals(cell.getRawValue(), null);
    }

    @Test
    public void removeCellWithId() {
        cellContainer = new CrpRawValueCellContainerImpl();
        CrPair anId = new CrPair(1, "A");

        try {
            cellContainer.putCell("testValue", anId);
        } catch (InvalidCellIdentifierException e) {
            fail();
        }

        Cell<CrPair, String> deletedCell = cellContainer.removeCell(anId);

        assertEquals(deletedCell.getRawValue(), "testValue");
        assertEquals(deletedCell.getId().getRow(), 1);
        assertEquals(deletedCell.getId().getColumn(), "A");

        try {
            assertEquals(cellContainer.getCell(anId).getRawValue(), null);
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void removeCellWithGivenCell() {
        cellContainer = new CrpRawValueCellContainerImpl();
        CrPair anId = new CrPair(1, "A");
        Cell<CrPair, String> acell = new CrpCell(anId);

        try {
            cellContainer.putCell("testValue", anId);
        } catch (InvalidCellIdentifierException e) {
            fail();
        }

        Cell<CrPair, String> deletedCell = cellContainer.removeCell(acell);

        assertEquals(deletedCell.getRawValue(), "testValue");
        assertEquals(deletedCell.getId().getRow(), 1);
        assertEquals(deletedCell.getId().getColumn(), "A");

        try {
            assertEquals(cellContainer.getCell(anId).getRawValue(), null);
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

}
