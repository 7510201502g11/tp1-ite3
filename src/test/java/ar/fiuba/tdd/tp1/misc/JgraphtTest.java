package ar.fiuba.tdd.tp1.misc;

import org.jgrapht.*;
import org.jgrapht.alg.CycleDetector;
import org.jgrapht.graph.*;
import org.junit.Test;

import static org.junit.Assert.*;

public class JgraphtTest {

    @Test
    public void testDetectCycle() {
        DirectedGraph<String, DefaultEdge> graph = new SimpleDirectedGraph<>(DefaultEdge.class);

        String vertix1 = "v1";
        String vertix2 = "v2";
        String vertix3 = "v3";
        String vertix4 = "v4";

        // add the vertices
        graph.addVertex(vertix1);
        graph.addVertex(vertix2);
        graph.addVertex(vertix3);
        graph.addVertex(vertix4);

        // add edges to create a circuit
        graph.addEdge(vertix1, vertix2);
        graph.addEdge(vertix2, vertix3);
        graph.addEdge(vertix3, vertix4);
        graph.addEdge(vertix4, vertix1);

        //try duplicate vertisex 
        assertFalse(graph.addVertex(vertix1));
        graph.addVertex(vertix2);
        graph.addVertex(vertix3);
        graph.addVertex(vertix4);

        CycleDetector<String, DefaultEdge> cycleDetector = new CycleDetector<String, DefaultEdge>(
                graph);
        assertTrue(cycleDetector.detectCycles());
    }
}
