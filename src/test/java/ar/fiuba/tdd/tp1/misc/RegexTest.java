package ar.fiuba.tdd.tp1.misc;

import org.junit.Test;

import static org.junit.Assert.*;

public class RegexTest {

    @Test
    public void testMatchAnything() {
        String regex = "(\\S+\\s*)+";
        assertTrue("hola como estas todo bien".matches(regex));
        assertTrue("hola".matches(regex));
        assertTrue("Esta es una, larga sentencia: con puntuacion rara...".matches(regex));
    }
    
    @Test
    public void testMatchWords() {
        String regex = "(\\w+\\s*)+";
        assertTrue("hola como estas todo bien".matches(regex));
        assertTrue("hola".matches(regex));
        assertFalse("Esta es una, larga sentencia: con puntuacion rara...".matches(regex));
        assertFalse("Esta es una oracion, con comas".matches(regex));
        assertTrue("hola47 este 78 como 987jhg".matches(regex));
    }

}
