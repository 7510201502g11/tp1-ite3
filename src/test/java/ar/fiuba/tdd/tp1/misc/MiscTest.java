package ar.fiuba.tdd.tp1.misc;

import com.google.gson.Gson;

import ar.fiuba.tdd.tp1.parser.expression.ArithmeticOperation;
import ar.fiuba.tdd.tp1.parser.expression.Expression;
import ar.fiuba.tdd.tp1.parser.expression.impl.ConstantExpression;
import ar.fiuba.tdd.tp1.parser.token.TokenConsumer;
import ar.fiuba.tdd.tp1.parser.token.TokenType;
import ar.fiuba.tdd.tp1.parser.token.Tokenizer;
import ar.fiuba.tdd.tp1.parser.token.impl.ExcelTokenizer;

import org.junit.Test;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.text.DateFormatter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class MiscTest {

    private static final int UNREF_VAR = 99;
    private static final double DELTA = 0.0000000001;

    @Test
    public void test() {
        ArithmeticOperation sin = Math::sin;
        assertEquals(0, sin.calculate(Math.PI), 0.0001);
    }

    @Test
    public void testCheckInstance() {
        Expression exp = ConstantExpression.create(29.0);
        assertTrue(exp instanceof Expression);
        assertTrue(exp instanceof ConstantExpression);
    }

    @Test
    public void testSplit() {
        String parsedString = "A23";
        String[] columnRow = parsedString.split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");
        System.out.println(Arrays.asList(columnRow));
    }

    @Test
    public void testIterateWithRegex() {
        assertTrue("hoja!A22".matches("[a-z]+![A-Z0-9]+"));
        assertTrue("hoja@A22".matches("[a-z]+[^!][A-Z0-9]+"));
        assertTrue("hojA_1!BC14".matches("\\w+![A-Z]+\\d+"));
        // assertTrue( "hojA_1!BC14".matches("\\w+!\\D+\\d+") );
    }

    @Test
    public void filterStringUsingTokenizer() {
        Tokenizer tokenizer = new ExcelTokenizer();
        final TokenType TT = TokenType.get();

        tokenizer.add("[+-]", TT.plusminus());
        tokenizer.add("[*/]", TT.multdiv());
        tokenizer.add("\\w+![A-Z]+\\d+", TT.variable());
        tokenizer.add("[A-Z]+\\d+", UNREF_VAR);
        tokenizer.add("\\d*\\.?\\d+", TT.number());

        String str = "default!A23 + BC2";
        TokenConsumer tokenConsumer = tokenizer.tokenize(str).getTokenConsumer();
        System.out.println(tokenConsumer);
    }

    @Test
    public void testPatterns() {
        String regex = "\\D+\\d+";
        Pattern pattern = Pattern.compile(regex);

        assertTrue(pattern.matcher("A23").matches());
        assertFalse(pattern.matcher("ABC").matches());
    }

    @Test
    public void testTransformReference() {
        String ref = "!other.A2";
        String regex = "!\\w+\\.[A-Z]+\\d+";
        Pattern pattern = Pattern.compile(regex);

        Matcher matcher = pattern.matcher("= 1 + !other.A2 + 0.5 - A3");
        while (matcher.find()) {
            String sequence = matcher.group();
            System.out.println(sequence);
            String[] split = sequence.split("\\.");
            String sheet = split[0].replaceAll("!", "");
            System.out.println(sheet);
            String cell = split[1];
            System.out.println(cell);
            String trueReference = sheet + "!" + cell;
            System.out.println(ref.replaceAll(sequence, trueReference));
        }
    }

    @Test
    public void testMatchRangeRegex() {
        assertTrue("A1:D2".matches("[A-Z]+\\d+:[A-Z]+\\d+"));
        assertTrue("hoja1!AB12:default!DC23".matches("\\w+![A-Z]+\\d+:\\w+![A-Z]+\\d+"));
    }

    private class Person {
        private String name;
        private List<String> jobs = new ArrayList<>();

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<String> getJobs() {
            return jobs;
        }

        public void setJobs(List<String> jobs) {
            this.jobs = jobs;
        }

    }

    @Test
    public void testMapJson() {
        Gson gson = new Gson();
        String strJson = "{ name:\"martin\" , jobs:[ \"programmer\" , \"salesman\" ] }";
        Person person = gson.fromJson(strJson, Person.class);

        assertEquals(person.getName(), "martin");
        assertEquals("programmer", person.getJobs().get(0));
        assertEquals("salesman", person.getJobs().get(1));
    }

    @Test
    public void decimalFormat() {
        String strNum = "1.23456";
        Double doubleVal = Double.valueOf(strNum);
        DecimalFormat df = new DecimalFormat("#.000");

        df.setRoundingMode(RoundingMode.HALF_UP);
        String format = df.format(doubleVal.doubleValue());
        System.out.println(format);
    }

    @Test
    public void testMapDefValue() {
        Map<String, String> defMap = new HashMap<String, String>();
        defMap.put(null, "asda");
        System.out.println(defMap.getOrDefault("asdad", "HOLA"));
    }

    @Test(expected = RuntimeException.class)
    public void testFilterOrElseThrow() {
        Object value = "hola";
        Optional.of(value).filter(v -> v.equals("asd")).orElseThrow(RuntimeException::new);
    }

    @Test
    public void testSet() {
        Set<String> set = new HashSet<>();
        String str = "hola";
        set.add(str);
        set.add(new String(str));
        set.add(new String(str));
        assertEquals(1, set.size());

        set.clear();
        assertEquals(0, set.size());
        for (String string : set) {
            fail();
        }
    }

    @Test
    public void testAddAllOnSet() {
        String[] arr = { "A1", "A2", new String("A3"), "B1", "B2", new String("B3"), "C1", "C2",
                new String("C3") };
        List<String> list = Arrays.asList(arr);
        Set<String> set = new HashSet<String>(list);
        assertEquals(set.size(), list.size());
        set.add(new String("A2"));
        set.add(new String("B3"));
        assertEquals(set.size(), list.size());
        set.add(new String("F4"));
        assertTrue(set.contains("F4") && set.size() == list.size() + 1);
    }

    @Test
    public void testGetMaxOfList() {
        List<Double> values = new ArrayList<>();
        values.add(2.3);
        values.add(4.75);
        double expectedMax = 8.7;
        values.add(expectedMax);
        values.add(1.13);
        values.add(5.123132);
        Double max = values.stream().max(Double::compare).get();
        assertEquals(expectedMax, max, DELTA);

        values.stream().mapToDouble(a -> a).average();
        OptionalDouble otherMax = values.stream().mapToDouble(a -> a).max();
        System.out.println("other max=" + otherMax);
    }

    @Test
    public void testRegexWithCommaAndParenthesis() {
        assertTrue("(hola)".matches("\\(hola\\)"));
        assertTrue("(hola,como)".matches("\\(hola,como\\)"));
    }

    private class CustomException extends Exception {

    }

    @Test(expected = CustomException.class)
    public void testTriggerCustomExceptionOfNullable() throws CustomException {
        String value = null;
        Optional.ofNullable(value).filter(v -> v.isEmpty() == false)
                .orElseThrow(CustomException::new);
    }

    @Test
    public void testCollectAges() {
        System.out.println("testCollectAges---------------------------------------------------");
        class DoubleContainer {
            Double val;

            public DoubleContainer(Double val) {
                super();
                this.val = val;
            }
        }

        List<DoubleContainer> containers = new ArrayList<>();
        containers.add(new DoubleContainer(12.25));
        containers.add(new DoubleContainer(9.99));
        containers.add(new DoubleContainer(4.33));

        double[] doubles = containers.stream().mapToDouble(con -> con.val).toArray();
    }

    @Test
    public void testParseDate() throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date parse = simpleDateFormat.parse("23-11-1990");
        System.out.println(parse);
    }

    @Test
    public void testDate() {
        String formatter = "DD-MM-YYYY";
        formatter = formatter.toLowerCase(Locale.ROOT).replaceAll("m", "M");
        assertEquals(formatter, "dd-MM-yyyy");
    }

    @Test
    public void testGsonMapToJson() {
        Gson gson = new Gson();
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("Number.Decimal", "2");
        String strJson = gson.toJson(map);

        System.out.println(strJson);
    }

    @Test
    public void testJsonToMap() {
        Gson gson = new Gson();
        HashMap map = new HashMap();
        String jsonString = "{\"Number.Decimal\":\"2\"}";
        map = gson.fromJson(jsonString, HashMap.class);

        assertEquals(map.get("Number.Decimal"), "2");

        jsonString = "{\"Number.decimal\" : \"2\" ; \"Number.symbol\" : \"asdasd\"}";
        HashMap otherMap = gson.fromJson(jsonString, HashMap.class);
        System.out.println(otherMap.toString());
    }

    @Test
    public void testMatchEqualsSign() {
        assertTrue( "  = ".matches("\\s*=\\s*") );
    }

}
