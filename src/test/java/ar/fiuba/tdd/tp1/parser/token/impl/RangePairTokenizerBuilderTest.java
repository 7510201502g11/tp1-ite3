package ar.fiuba.tdd.tp1.parser.token.impl;

import ar.fiuba.tdd.tp1.parser.token.Token;
import ar.fiuba.tdd.tp1.parser.token.TokenType;
import ar.fiuba.tdd.tp1.parser.token.Tokenizer;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;

import static org.junit.Assert.*;

public class RangePairTokenizerBuilderTest {
    static final Integer rangeFunction = TokenType.get().rangeFunction();
    static final Integer pairFunction = TokenType.get().pairFunction();
    static final int variable = TokenType.get().variable();
    static final int comma = TokenType.get().comma();
    static final int openBracket = TokenType.get().openBracket();
    static final int closeBracket = TokenType.get().closeBracket();

    static final String maxTypeAndSequence = rangeFunction + "->" + "MAX";
    static final String minTypeAndSequence = rangeFunction + "->" + "MIN";
    static final String averageTypeAndSequence = rangeFunction + "->" + "AVERAGE";
    static final String concatTypeAndSequence = pairFunction + "->" + "CONCAT";
    static final String obTypeAndSequence = openBracket + "->" + "(";
    static final String cbTypeAndSequence = closeBracket + "->" + ")";
    static final String commaTypeAndSequence = comma + "->" + ",";

    public List<String> getTypesAndSequences(List<Token> tokens) {
        List<String> res = new ArrayList<>();
        tokens.forEach(tok -> res.add(tok.typeAndSequence()));
        return res;
    }

    RangePairTokenizerBuilder rangePairTokenizerBuilder = new RangePairTokenizerBuilder();

    @Test
    public void testParseRangeOperations() {
        {
            Tokenizer tokenizer = rangePairTokenizerBuilder.build();
            String str = "MAX(A2:C23)";
            List<String> typesAndSequences = getTypesAndSequences(tokenizer.tokenize(str)
                    .getTokens());
            String rangeTypeAndSequence = TokenType.get().rangeTerm() + "->" + "A2:C23";
            assertThat(
                    typesAndSequences,
                    contains(maxTypeAndSequence, obTypeAndSequence, rangeTypeAndSequence,
                            cbTypeAndSequence));
        }

        {
            Tokenizer tokenizer = rangePairTokenizerBuilder.build();
            String str = "= MIN( A2:C23 )";
            List<String> typesAndSequences = getTypesAndSequences(tokenizer.tokenize(str)
                    .getTokens());
            String rangeTypeAndSequence = TokenType.get().rangeTerm() + "->" + "A2:C23";
            assertThat(
                    typesAndSequences,
                    hasItems(minTypeAndSequence, obTypeAndSequence, rangeTypeAndSequence,
                            cbTypeAndSequence));
        }

        {
            Tokenizer tokenizer = rangePairTokenizerBuilder.build();
            String str = "= AVERAGE( A2:C23   )";
            List<String> typesAndSequences = getTypesAndSequences(tokenizer.tokenize(str)
                    .getTokens());
            String rangeTypeAndSequence = TokenType.get().rangeTerm() + "->" + "A2:C23";
            assertThat(
                    typesAndSequences,
                    hasItems(averageTypeAndSequence, obTypeAndSequence, rangeTypeAndSequence,
                            cbTypeAndSequence));
        }
    }

    @Test
    public void testParsePairOperations() {
        {
            Tokenizer tokenizer = rangePairTokenizerBuilder.build();
            String str = "CONCAT( A2, C23 )";
            List<String> typesAndSequences = getTypesAndSequences(tokenizer.tokenize(str)
                    .getTokens());
            String refTypeAndSequence1 = variable + "->" + "A2";
            String refTypeAndSequence2 = variable + "->" + "C23";
            assertThat(
                    typesAndSequences,
                    contains(concatTypeAndSequence, obTypeAndSequence, refTypeAndSequence1,
                            commaTypeAndSequence, refTypeAndSequence2, cbTypeAndSequence));
        }

    }

    @Test
    public void testParseRangeOperationsUsingPointers() {
        {
            Tokenizer tokenizer = rangePairTokenizerBuilder.build();
            String str = "MAX(default!A2:default!C23)";
            List<String> typesAndSequences = getTypesAndSequences(tokenizer.tokenize(str)
                    .getTokens());
            String rangeTypeAndSequence = TokenType.get().rangeTerm() + "->"
                    + "default!A2:default!C23";
            assertThat(
                    typesAndSequences,
                    contains(maxTypeAndSequence, obTypeAndSequence, rangeTypeAndSequence,
                            cbTypeAndSequence));
        }

        {
            Tokenizer tokenizer = rangePairTokenizerBuilder.build();
            String str = "MIN(hoja1!A2:hoja1!C23)";
            List<String> typesAndSequences = getTypesAndSequences(tokenizer.tokenize(str)
                    .getTokens());
            String rangeTypeAndSequence = TokenType.get().rangeTerm() + "->"
                    + "hoja1!A2:hoja1!C23";
            assertThat(
                    typesAndSequences,
                    contains(minTypeAndSequence, obTypeAndSequence, rangeTypeAndSequence,
                            cbTypeAndSequence));
        }

        {
            Tokenizer tokenizer = rangePairTokenizerBuilder.build();
            String str = "AVERAGE(  hoja1!A2:hoja1!C23 )";
            List<String> typesAndSequences = getTypesAndSequences(tokenizer.tokenize(str)
                    .getTokens());
            String rangeTypeAndSequence = TokenType.get().rangeTerm() + "->"
                    + "hoja1!A2:hoja1!C23";
            assertThat(
                    typesAndSequences,
                    contains(averageTypeAndSequence, obTypeAndSequence, rangeTypeAndSequence,
                            cbTypeAndSequence));
        }
    }
}
