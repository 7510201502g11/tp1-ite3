package ar.fiuba.tdd.tp1.parser.expression.impl;

import ar.fiuba.tdd.tp1.parser.expression.Expression;

import org.junit.Test;

import static org.junit.Assert.*;

public class SequenceOperationExpressionTest {

    private static final Double DELTA = 0.0000000001;

    private Expression createConstExpression(Double val) {
        return new ConstantExpression(val);
    }

    @Test
    public void testGetValueMultipleArgs() {
        Double val1 = 2.5;
        Double val2 = 6.25;
        Double val3 = 10.33;
        Double val4 = 3.75;

        SequenceOperationExpression sequenceOperationExpression = new SequenceOperationExpression(
                createConstExpression(val1));
        sequenceOperationExpression.addOperand(createConstExpression(val2), "*");
        sequenceOperationExpression.addOperand(createConstExpression(val3), "-");
        sequenceOperationExpression.addOperand(createConstExpression(val4), "/");

        Double expected = (((val1 * val2) - val3) / val4);
        double actual = sequenceOperationExpression.getValueAsDouble(null);
        assertEquals(expected, actual, DELTA);
    }

    @Test
    public void testGetValueSingleArg() {
        Double val1 = 2.5;

        SequenceOperationExpression sequenceOperationExpression = new SequenceOperationExpression(
                createConstExpression(val1));

        Double expected = val1;
        double actual = sequenceOperationExpression.getValueAsDouble(null);
        assertEquals(expected, actual, DELTA);
    }

    @Test
    public void testGetValueMultipleArgsNegativeResult() {
        Double val1 = 2.5;
        Double val2 = 6.25;
        Double val3 = 10.33;
        Double val4 = 3.75;

        SequenceOperationExpression sequenceOperationExpression = new SequenceOperationExpression(
                createConstExpression(val1));
        sequenceOperationExpression.addOperand(createConstExpression(val2), "*");
        sequenceOperationExpression.addOperand(createConstExpression(val3), "-");
        sequenceOperationExpression.addOperand(createConstExpression(val4), "/");

        sequenceOperationExpression.setPositive(false);

        Double expected = (((val1 * val2) - val3) / val4) * (-1);
        double actual = sequenceOperationExpression.getValueAsDouble(null);
        assertEquals(expected, actual, DELTA);
    }

    @Test
    public void testGetValueOfEmptyExpression() {
        SequenceOperationExpression sequenceOperationExpression = new SequenceOperationExpression();

        sequenceOperationExpression.setPositive(false);

        Double expected = 0.0;
        double actual = sequenceOperationExpression.getValueAsDouble(null);
        assertEquals(expected, actual, DELTA);
    }
}
