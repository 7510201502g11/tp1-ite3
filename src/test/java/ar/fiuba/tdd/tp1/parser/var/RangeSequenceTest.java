package ar.fiuba.tdd.tp1.parser.var;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RangeSequenceTest {
    @Test
    public void testNextUsingTopLeftAndBottomRight() {
        String topLeft = "A1";
        String bottomRight = "B3";
        RangeSequence rangeSequence = new RangeSequence(topLeft, bottomRight)
                .setDefaultPointer("hoja1");
        List<String> sequenceList = rangeSequence.asList();
        String[] arr = { "hoja1!A1", "hoja1!A2", "hoja1!A3", "hoja1!B1", "hoja1!B2", "hoja1!B3" };
        List<String> expectedList = Arrays.asList(arr);
        assertTrue(sequenceList.containsAll(expectedList));
    }

    @Test
    public void testNextUsingTopLeftAndBottomRightNoDefaultPointer() {
        String topLeft = "A1";
        String bottomRight = "B3";
        RangeSequence rangeSequence = new RangeSequence(topLeft, bottomRight);
        List<String> sequenceList = rangeSequence.asList();
        String[] arr = { "A1", "A2", "A3", "B1", "B2", "B3" };
        List<String> expectedList = Arrays.asList(arr);
        assertTrue(sequenceList.containsAll(expectedList));
    }

    @Test
    public void testNext() {
        String range = "C2:E4";
        RangeSequence rangeSequence = new RangeSequence(range).setDefaultPointer("");
        assertEquals(rangeSequence.getCurrentAsString(), "C2");
        assertEquals(rangeSequence.next().getCurrentAsString(), "D2");
        assertEquals(rangeSequence.next().getCurrentAsString(), "E2");
        assertEquals(rangeSequence.next().getCurrentAsString(), "C3");
        assertEquals(rangeSequence.next().getCurrentAsString(), "D3");
        assertEquals(rangeSequence.next().getCurrentAsString(), "E3");
        assertEquals(rangeSequence.next().getCurrentAsString(), "C4");
        assertEquals(rangeSequence.next().getCurrentAsString(), "D4");
        assertEquals(rangeSequence.next().getCurrentAsString(), "E4");
        assertTrue(rangeSequence.ended());
        assertEquals(rangeSequence.next().getCurrentAsString(), "E4");
        assertTrue(rangeSequence.ended());

        assertTrue(rangeSequence.getCurrentCopy().getPointer().isEmpty());
    }

    @Test
    public void testNextWithPointers() {
        String range = "default!C2:default!E4";
        RangeSequence rangeSequence = new RangeSequence(range);

        assertEquals(rangeSequence.getCurrentAsString(), "default!C2");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!D2");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!E2");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!C3");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!D3");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!E3");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!C4");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!D4");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!E4");
        assertTrue(rangeSequence.ended());
        assertEquals("default!E4", rangeSequence.next().getCurrentAsString());
        assertTrue(rangeSequence.ended());
    }

    @Test
    public void testNextTillEndAndReset() {
        String range = "default!C2:default!E4";
        RangeSequence rangeSequence = new RangeSequence(range);

        while (rangeSequence.ended() == false) {
            rangeSequence.next();
        }

        RangeSequence copy = rangeSequence.copy();
        assertEquals(copy.getCurrentAsString(), "default!C2");
        assertEquals(copy.next().getCurrentAsString(), "default!D2");
        assertEquals(copy.next().getCurrentAsString(), "default!E2");
        assertEquals(copy.next().getCurrentAsString(), "default!C3");
        assertEquals(copy.next().getCurrentAsString(), "default!D3");
        assertEquals(copy.next().getCurrentAsString(), "default!E3");
        assertEquals(copy.next().getCurrentAsString(), "default!C4");
        assertEquals(copy.next().getCurrentAsString(), "default!D4");
        assertEquals(copy.next().getCurrentAsString(), "default!E4");
        assertTrue(copy.ended());
        assertEquals("default!E4", copy.next().getCurrentAsString());
        assertTrue(copy.ended());

        assertTrue(copy.ended() && rangeSequence.ended());
    }

    @Test
    public void testNextWithPointersWithDefaultPointer() {
        String range = "C2:E4";
        RangeSequence rangeSequence = new RangeSequence(range).setDefaultPointer("default");

        assertEquals(rangeSequence.getCurrentAsString(), "default!C2");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!D2");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!E2");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!C3");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!D3");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!E3");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!C4");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!D4");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!E4");
        assertTrue(rangeSequence.ended());
        assertEquals("default!E4", rangeSequence.next().getCurrentAsString());
        assertTrue(rangeSequence.ended());
    }

    @Test
    public void testNextWithDefaultPointerAndOnlyRows() {
        String range = "C2:C6";
        RangeSequence rangeSequence = new RangeSequence(range).setDefaultPointer("default");

        assertEquals(rangeSequence.getCurrentAsString(), "default!C2");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!C3");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!C4");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!C5");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!C6");
        assertTrue(rangeSequence.ended());
        assertEquals("default!C6", rangeSequence.next().getCurrentAsString());
        assertTrue(rangeSequence.ended());
    }

    @Test
    public void testNextWithPointersWithDefaultPointerAndHigherIndexes() {
        String range = "C28:E31";
        RangeSequence rangeSequence = new RangeSequence(range).setDefaultPointer("default");

        assertEquals(rangeSequence.getCurrentAsString(), "default!C28");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!D28");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!E28");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!C29");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!D29");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!E29");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!C30");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!D30");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!E30");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!C31");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!D31");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!E31");
        assertTrue(rangeSequence.ended());
        assertEquals("default!E31", rangeSequence.next().getCurrentAsString());
        assertTrue(rangeSequence.ended());
    }

    @Test
    public void testNextWithPointersAndDefaultPointerOverlap() {
        String range = "default!C2:default!E4";
        RangeSequence rangeSequence = new RangeSequence(range).setDefaultPointer("hoja");

        assertEquals(rangeSequence.getCurrentAsString(), "default!C2");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!D2");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!E2");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!C3");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!D3");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!E3");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!C4");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!D4");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!E4");
        assertTrue(rangeSequence.ended());
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!E4");
        assertTrue(rangeSequence.ended());
    }

    @Test
    public void testNextWithPointersAndColumnsOnly() {
        String range = "default!C2:default!F2";
        RangeSequence rangeSequence = new RangeSequence(range).setDefaultPointer("hoja");

        assertEquals(rangeSequence.getCurrentAsString(), "default!C2");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!D2");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!E2");
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!F2");
        assertTrue(rangeSequence.ended());
        assertEquals(rangeSequence.next().getCurrentAsString(), "default!F2");
        assertTrue(rangeSequence.ended());
    }

    @Test
    public void oneElementSequence() {
        String range = "C3:C3";
        RangeSequence rangeSequence = new RangeSequence(range);

        assertEquals("C3", rangeSequence.getCurrentAsString());
        assertTrue(rangeSequence.ended());
    }

    @Test
    public void testAsList() {
        String range = "A1:C3";
        String defaultPointer = "hoja1";
        String prefix = defaultPointer + "!";
        RangeSequence rangeSequence = new RangeSequence(range).setDefaultPointer(defaultPointer);

        while (rangeSequence.ended() == false) {
            rangeSequence.next();
        }

        String[] arr = { prefix + "A1", prefix + "A2", new String(prefix + "A3"), prefix + "B1",
                prefix + "B2", new String(prefix + "B3"), prefix + "C1", prefix + "C2",
                new String(prefix + "C3") };
        List<String> list = Arrays.asList(arr);

        assertTrue(rangeSequence.asList().containsAll(list));
    }
}
