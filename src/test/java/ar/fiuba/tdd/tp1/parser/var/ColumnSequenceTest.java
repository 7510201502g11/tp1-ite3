package ar.fiuba.tdd.tp1.parser.var;

import org.junit.Test;

import static org.junit.Assert.*;

public class ColumnSequenceTest {

    @Test
    public void testNext() {
        ColumnSequence columnSequence = new SimpleColumnSequence("x");
        assertEquals("X", columnSequence.getCurrent());
        assertEquals("Y", columnSequence.next().getCurrent());

        assertTrue(columnSequence.next().next().didReset());
        assertFalse(columnSequence.didReset());
        assertEquals("A", columnSequence.getCurrent());
    }
    
    @Test
    public void testNextUsingFirstAndLast() {
        ColumnSequence columnSequence = new SimpleColumnSequence("A","C");
        assertEquals("A", columnSequence.getCurrent());
        assertEquals("B", columnSequence.next().getCurrent());

        assertTrue(columnSequence.next().next().didReset());
        assertFalse(columnSequence.didReset());
        assertEquals("A", columnSequence.getCurrent());
    }

    @Test
    public void testNextUsingLimitedSequence() {
        ColumnSequence columnSequence = new SimpleColumnSequence("G","f","i");
        assertEquals("G", columnSequence.getCurrent());
        assertEquals("H", columnSequence.next().getCurrent());

        assertTrue(columnSequence.next().next().didReset());
        assertFalse(columnSequence.didReset());
        assertEquals("F", columnSequence.getCurrent());
    }
}
