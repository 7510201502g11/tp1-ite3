package ar.fiuba.tdd.tp1.parser.expression.impl;

import ar.fiuba.tdd.tp1.parser.expression.Context;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public class ContextImplTest {
    class KeyValue {
        public final String key;
        public final Object val;

        public KeyValue(String key, Object val) {
            super();
            this.key = key;
            this.val = val;
        }
    }

    @Test
    public void testKeysAndValues() {
        Context context = new ContextImpl();
        List<KeyValue> keyValues = new ArrayList<ContextImplTest.KeyValue>();
        int size = 10;
        for (int i = 0; i < size; i++) {
            KeyValue keyValue = new KeyValue("key_" + i, "value_" + i);
            keyValues.add(keyValue);
            context.put(keyValue.key, keyValue.val);
        }

        Set<String> keys = context.keys();
        Collection<Object> values = context.values();

        for (int i = 0; i < size; i++) {
            String key = keyValues.get(i).key;
            Object val = keyValues.get(i).val;
            keys.contains(key);
            values.contains(val);
        }
    }

    @Test
    public void testValues() {
    }

}
