package ar.fiuba.tdd.tp1.parser.expression;

import ar.fiuba.tdd.tp1.parser.expression.impl.ConstantExpression;
import ar.fiuba.tdd.tp1.parser.expression.impl.ExponentiationExpression;
import ar.fiuba.tdd.tp1.parser.expression.impl.FunctionExpression;
import ar.fiuba.tdd.tp1.parser.expression.impl.SequenceOperationExpression;
import ar.fiuba.tdd.tp1.parser.expression.impl.StandardArithmeticFunctionExpressionBuilder;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExpressionNodeTest {

    private static final double DELTA = 0.0000000000000000001;
    private static final StandardArithmeticFunctionExpressionBuilder arithmeticFunctionExpressionBuilder = 
            StandardArithmeticFunctionExpressionBuilder.newInstance();

    @Test
    public void testGetValue() {
        // testeando 3*2^4 - sqrt(1+3)

        SequenceOperationExpression add1 = new SequenceOperationExpression(
                new ConstantExpression(1));
        add1.addOperand(new ConstantExpression(3), "+");
        assertEquals(4.0, add1.getValueAsDouble(null), DELTA);

        ExponentiationExpression exp = new ExponentiationExpression(add1, new ConstantExpression(
                0.5));
        assertEquals(2.0, exp.getValueAsDouble(null), DELTA);

        // ExpressionNode sqrt = new FunctionExpressionNode(FunctionExpressionNode.SQRT, add1);
        // ExpressionNode sqrt =
        // FunctionExpressionNode.createStandardArithmeticFunctionExpressionNode().setOperationId("sqrt").setArgument(add1);
        Expression sqrt = arithmeticFunctionExpressionBuilder.build().setOperationId("sqrt")
                .setArgument(add1);
        assertEquals(2.0, sqrt.getValueAsDouble(null), DELTA);

        exp = new ExponentiationExpression(new ConstantExpression(2.0), new ConstantExpression(4.0));
        assertEquals(16.0, exp.getValueAsDouble(null), DELTA);

        SequenceOperationExpression mult = new SequenceOperationExpression();
        mult.addOperand(new SequenceOperationExpression(new ConstantExpression(3.0), true), "*");
        // mult.add(new ConstantExpressionNode(3.0), true);
        mult.addOperand(exp, "*");
        assertEquals(48.0, mult.getValueAsDouble(null), DELTA);

        SequenceOperationExpression add2 = new SequenceOperationExpression();
        add2.addOperand(mult);
        add2.addOperand(sqrt, "/");
        assertEquals(24.0, add2.getValueAsDouble(null), DELTA);
    }

    @Test
    public void testLogs() {
        ConstantExpression add2 = new ConstantExpression(24.0);

        FunctionExpression log = arithmeticFunctionExpressionBuilder.build().setOperationId("log")
                .setArgument(add2);
        assertEquals(Math.log(24.0) * 0.43429448190325182765, log.getValueAsDouble(null), DELTA);

        FunctionExpression log2 = arithmeticFunctionExpressionBuilder.build()
                .setOperationId("log2").setArgument(add2);
        assertEquals(Math.log(24.0) * 1.442695040888963407360, log2.getValueAsDouble(null), DELTA);
    }

}
