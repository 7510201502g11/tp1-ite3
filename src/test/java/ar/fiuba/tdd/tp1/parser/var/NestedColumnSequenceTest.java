package ar.fiuba.tdd.tp1.parser.var;

import org.junit.Test;

import static org.junit.Assert.*;

public class NestedColumnSequenceTest {

    @Test
    public void testNext() {
        ColumnSequence columnSequence = new NestedColumnSequence("A", "C");
        assertEquals("B", columnSequence.next().getCurrent());
        assertEquals("C", columnSequence.next().getCurrent());
        assertEquals("AA", columnSequence.next().getCurrent());
        assertEquals("AB", columnSequence.next().getCurrent());
        assertEquals("AC", columnSequence.next().getCurrent());
        assertEquals("BA", columnSequence.next().getCurrent());
        assertEquals("BB", columnSequence.next().getCurrent());
        assertEquals("BC", columnSequence.next().getCurrent());
        assertEquals("CA", columnSequence.next().getCurrent());
        assertEquals("CB", columnSequence.next().getCurrent());
        assertEquals("CC", columnSequence.next().getCurrent());
        assertEquals("AAA", columnSequence.next().getCurrent());
        assertEquals("AAB", columnSequence.next().getCurrent());
        assertEquals("AAC", columnSequence.next().getCurrent());
    }

    @Test
    public void testNextStartingFrom() {
        {
            ColumnSequence columnSequence = NestedColumnSequence.create("BA", "A", "C");
            assertEquals("BB", columnSequence.next().getCurrent());
            assertEquals("BC", columnSequence.next().getCurrent());
            assertEquals("CA", columnSequence.next().getCurrent());
            assertEquals("CB", columnSequence.next().getCurrent());
            assertEquals("CC", columnSequence.next().getCurrent());
            assertEquals("AAA", columnSequence.next().getCurrent());
            assertEquals("AAB", columnSequence.next().getCurrent());
            assertEquals("AAC", columnSequence.next().getCurrent());
        }

        {
            ColumnSequence columnSequence = NestedColumnSequence.create("CB", "A", "C");
            assertEquals("CC", columnSequence.next().getCurrent());
            assertEquals("AAA", columnSequence.next().getCurrent());
            assertEquals("AAB", columnSequence.next().getCurrent());
            assertEquals("AAC", columnSequence.next().getCurrent());
        }
        
        {
            ColumnSequence columnSequence = NestedColumnSequence.create("Y");
            assertEquals("Z", columnSequence.next().getCurrent());
            assertEquals("AA", columnSequence.next().getCurrent());
            assertEquals("AB", columnSequence.next().getCurrent());
            assertEquals("AC", columnSequence.next().getCurrent());
        }
    }
}
