package ar.fiuba.tdd.tp1.parser.var;

import ar.fiuba.tdd.tp1.util.VarRef;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class VariableReferenceTest {
    private static final VarRef variableReference = VarRef.get();

    @Test
    public void testSplitVariable() {
        String var = "default!A2";
        PointerVariablePair pointerVar = PointerVariablePair.create(var);
        assertEquals("default", pointerVar.pointer);
        assertEquals("A2", pointerVar.relativeVariable);
        assertEquals(var, pointerVar.toString());
    }

    @Test
    public void testMatchesVariables() {
        assertTrue(variableReference.relativeMatcher().matches("A32"));
        assertTrue(variableReference.absoluteMatcher().matches("default!A32"));
        assertTrue(variableReference.absoluteMatcher().matches("default!A32"));
    }

    @Test
    public void testBuildAbsoluteVariable() {
        String pointer = "default";
        String relVar = "AB32";
        assertEquals("default!AB32", VarRef.get().absVarReferenceBuilder().build(pointer, relVar));
        assertEquals("hoja!AB32", variableReference.absVarReferenceBuilder().build(pointer, "hoja!AB32"));
        assertEquals("AB32", variableReference.absVarReferenceBuilder().build("", relVar));
    }

    @Test
    public void testSplitRange() {
        {
            String range = "default!A2:C34";
            RangePair splitRange = RangePair.create(range);
            assertTrue(splitRange.left.pointer.equals("default"));
            assertTrue(splitRange.left.hasPointer());
            assertFalse(splitRange.right.hasPointer());

            assertEquals(range, splitRange.toString());
        }

        {
            String range = "A2:C34";
            RangePair splitRange = RangePair.create(range);
            assertFalse(splitRange.left.hasPointer());
            assertFalse(splitRange.right.hasPointer());

            assertEquals(range, splitRange.toString());
        }

        {
            String range = "hoja!A2:bla!C34";
            RangePair splitRange = RangePair.create(range);
            assertEquals(splitRange.left.pointer, "hoja");
            assertEquals(splitRange.right.pointer, "bla");
            assertTrue(splitRange.left.hasPointer());
            assertTrue(splitRange.right.hasPointer());

            assertEquals(range, splitRange.toString());
        }
    }

}
