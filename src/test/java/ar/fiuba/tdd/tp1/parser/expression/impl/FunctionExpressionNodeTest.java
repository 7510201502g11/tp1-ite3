package ar.fiuba.tdd.tp1.parser.expression.impl;

import ar.fiuba.tdd.tp1.parser.expression.EvaluationException;
import ar.fiuba.tdd.tp1.parser.expression.impl.ConstantExpression;
import ar.fiuba.tdd.tp1.parser.expression.impl.FunctionExpression;

import org.junit.Test;

import static org.junit.Assert.*;

public class FunctionExpressionNodeTest {

    private static final double DELTA = 0.000001;

    @Test(expected = EvaluationException.class)
    public void testGetValueGetEvalException() {
        FunctionExpression functionExpressionNode = new FunctionExpression("cos",
                new ConstantExpression(Math.PI));
        functionExpressionNode.getValueAsDouble(null);
    }

    @Test
    public void testGetValueGetValueCos() {
        FunctionExpression functionExpressionNode = new FunctionExpression("cos",
                new ConstantExpression(Math.PI));

        functionExpressionNode.addOperation("cos", Math::cos);

        double value = functionExpressionNode.getValueAsDouble(null);
        assertEquals(-1.0, value, DELTA);
    }

}
