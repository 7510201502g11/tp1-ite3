package ar.fiuba.tdd.tp1.parser.token;

import org.junit.Test;

import static org.junit.Assert.*;

public class TokenTest {

    @Test
    public void testToString() {
        Token token = new Token(TokenType.get().plusminus(), "+-");
        System.out.println(token.toString());
        assertEquals("{sequence=+-, pos=-1, tokenType=1}", token.toString());
    }

}
