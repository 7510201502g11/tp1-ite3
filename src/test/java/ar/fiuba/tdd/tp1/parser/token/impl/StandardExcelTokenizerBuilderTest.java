package ar.fiuba.tdd.tp1.parser.token.impl;

import ar.fiuba.tdd.tp1.parser.token.Token;
import ar.fiuba.tdd.tp1.parser.token.TokenType;
import ar.fiuba.tdd.tp1.parser.token.Tokenizer;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class StandardExcelTokenizerBuilderTest {

    @Test
    public void testBuild() {
        try {
            Tokenizer tokenizer = new ExcelTokenizerBuilder().build();
            tokenizer.tokenize("hoja1!C24 + 9.8");
            tokenizer.tokenize("=hoja1!C24 + 9.8");
            List<Token> tokens = tokenizer.getTokens();
            assertFalse(tokens.isEmpty());
            assertEquals(4, tokens.size());
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void testTokenizeEmptyString() {
        try {
            Tokenizer tokenizer = new ExcelTokenizerBuilder().build();
            tokenizer.tokenize("=");
            List<Token> tokens = tokenizer.getTokens();
            assertEquals( tokens.size() , 1);
            assertTrue(tokens.get(0).typeEquals( TokenType.get().equals() ));
        } catch (Exception e) {
            fail();
        }
    }
}
