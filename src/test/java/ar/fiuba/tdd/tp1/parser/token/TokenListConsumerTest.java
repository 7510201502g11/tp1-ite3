package ar.fiuba.tdd.tp1.parser.token;

import ar.fiuba.tdd.tp1.parser.token.impl.TokenListConsumer;

import org.junit.Test;

import java.util.LinkedList;

import static org.junit.Assert.*;

public class TokenListConsumerTest {

    private static final Double DELTA = 0.0000000001;

    @Test
    public void testGetIfEmpty() {
        LinkedList<Token> tokenList = new LinkedList<Token>();
        Token tk1 = new Token(TokenType.get().function(), "sin");
        tokenList.add(tk1);
        TokenConsumer tokenContainer = new TokenListConsumer(tokenList);

        assertEquals(tk1, tokenContainer.getCurrent());
        assertEquals(tokenContainer, tokenContainer.next());
        Token epsilon = new Token(TokenType.get().epsilon(), "asdasd");
        tokenContainer.setDefault(epsilon);
        assertTrue(tokenContainer.empty());
        assertEquals(epsilon, tokenContainer.getCurrent());
    }

    @Test
    public void testGetNextOfEmpty() {
        LinkedList<Token> tokenList = new LinkedList<Token>();
        Token tk1 = new Token(TokenType.get().function(), "sin");
        tokenList.add(tk1);
        TokenConsumer tokenConsumer = new TokenListConsumer(tokenList);

        tokenConsumer.next();
        assertTrue(tokenConsumer.empty());
        tokenConsumer.next();
        assertTrue(tokenConsumer.empty());
        tokenConsumer.next();
        assertTrue(tokenConsumer.empty());

        tokenConsumer.add(new Token(0, "sequence"));
        assertFalse(tokenConsumer.empty());
        tokenConsumer.next();
        assertTrue(tokenConsumer.empty());
    }

    @Test
    public void testGetCurrentTokenStuff() {
        LinkedList<Token> tokenList = new LinkedList<Token>();
        int pos = 4;
        String sequence = "sin";
        int tokenType = TokenType.get().function();
        Token tk1 = new Token(tokenType, sequence, pos);
        tokenList.add(tk1);
        TokenConsumer tokenConsumer = new TokenListConsumer(tokenList);

        assertEquals(pos, tokenConsumer.getCurrentTokenPos(), DELTA);
        assertEquals(sequence, tokenConsumer.getCurrentTokenSequence());
        assertEquals(tokenType, tokenConsumer.getCurrentTokenType(), DELTA);
    }

    @Test
    public void testContainsType() {
        LinkedList<Token> tokenList = new LinkedList<Token>();
        final Integer equals = TokenType.get().equals();
        tokenList.add(new Token(equals, "  = "));
        TokenConsumer tokenConsumer = new TokenListConsumer(tokenList);
        assertTrue(tokenConsumer.containsType(equals));
    }
}
