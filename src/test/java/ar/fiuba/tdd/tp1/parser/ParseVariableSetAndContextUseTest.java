package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.parser.expression.Context;
import ar.fiuba.tdd.tp1.parser.expression.Expression;
import ar.fiuba.tdd.tp1.parser.expression.impl.ContextImpl;
import ar.fiuba.tdd.tp1.parser.impl.ParserImpl;
import ar.fiuba.tdd.tp1.parser.token.Tokenizer;
import ar.fiuba.tdd.tp1.parser.token.impl.StandardArithmeticTokenizerBuilder;

import org.junit.Test;

import static org.junit.Assert.*;

public class ParseVariableSetAndContextUseTest {
    private static final double DELTA = 0.0000000000001;

    private Parser buildParser() {
        Tokenizer tokenizer1 = new StandardArithmeticTokenizerBuilder().build();
        Parser parser1 = new ParserImpl(tokenizer1);
        return parser1;
    }

    @Test
    public void testSolveManyCells() {
        Context context = new ContextImpl();
        Parser parser = buildParser();
        Expression mainExpression = parser.parse("y + z - w"); // x=18.2

        context.put("x", mainExpression);
        context.put("y", buildParser().parse("z + w")); // y=17.6
        context.put("z", buildParser().parse("9.1"));
        context.put("w", buildParser().parse("8.5"));

        assertEquals(18.2, mainExpression.getValueAsDouble(context), DELTA);
    }
}
