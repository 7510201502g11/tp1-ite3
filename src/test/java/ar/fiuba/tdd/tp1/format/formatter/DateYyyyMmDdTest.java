package ar.fiuba.tdd.tp1.format.formatter;

import ar.fiuba.tdd.tp1.format.formatter.FDate;
import ar.fiuba.tdd.tp1.format.formatter.Formatter;
import ar.fiuba.tdd.tp1.format.formatter.FormatterDate;
import ar.fiuba.tdd.tp1.format.formatter.InvalidFormatException;

import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;

public class DateYyyyMmDdTest {

    @Test
    public void createDateWithNumber() {
        FDate date = new FDate(2000, 1, 13);
        long numberDate = date.date();
        Formatter formatter = new FormatterDate("yyyy-MM-dd");
        String dateString = formatter.format("" + numberDate);
        assertEquals("2000-01-13", dateString);
    }

    @Test //(expected = InvalidFormatException.class)
    public void createDateWithInvalidDate() {
        Formatter formatter = new FormatterDate("yyyy-MM-dd");
        assertEquals(formatter.format("hola"), "Error:BAD_DATE");
    }

    @Test(expected = NullPointerException.class)
    public void createDateWithNullParametre() {
        new FormatterDate(null);
    }

    @Test(expected = InvalidFormatException.class)
    public void createDateWithInvalidFormatter() {
        new FormatterDate("hola");
    }

    @Test
    public void getInformation() {
        Formatter formatter = new FormatterDate("yyyy-MM-dd");
        Map<String, String> information = formatter.getInformation();
        assertEquals(information.get("Type"), "Date");
        assertEquals(information.get("Format"), "YYYY-MM-DD");
    }
    
    @Test
    public void formatEmptyString() {
        Formatter formatter = new FormatterDate("yyyy-MM-dd");
        assertEquals(formatter.format(""), "");
    }

    @Test
    public void format() {
        DateYyyyMmDd format = new DateYyyyMmDd();
        assertEquals(format.format(), "yyyy-MM-dd");
    }

    @Test
    public void getInformationDateYyyyMmDd() {
        DateYyyyMmDd format = new DateYyyyMmDd();
        assertEquals(format.getInformation(), "YYYY-MM-DD");
    }

}
