package ar.fiuba.tdd.tp1.format.formatter;

import ar.fiuba.tdd.tp1.format.formatter.FNumber;
import ar.fiuba.tdd.tp1.format.formatter.Formatter;
import ar.fiuba.tdd.tp1.format.formatter.InvalidFormatException;

import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

public class FNumberTest {

    @Test
    public void formatterWithTwoDecimal() {
        Formatter formatter = new FNumber(2, new FString());
        assertEquals("2.02", formatter.format("02.020002"));
    }

    @Test
    public void formatterWithoutDecimal() {
        Formatter formatter = new FNumber(0, new FString());
        String number = formatter.format("02.020002");
        assertEquals("2", number);
    }

    @Test
    public void formatterWithFiveDecimal() {
        Formatter formatter = new FNumber(5, new FString());
        assertEquals("2.02001", formatter.format("02.020007"));
    }

    @Test //(expected = InvalidFormatException.class)
    public void invalidFormatter() {
        Formatter formatter = new FNumber(5, new FString());
        assertEquals(formatter.format("Hola"), "Error:BAD_NUMBER");
    }

    @Test(expected = InvalidFormatException.class)
    public void createFormatterWithInvalidParameter() {
        new FNumber(-2, new FString());
    }

    @Test
    public void getInformation() {
        Formatter formatter = new FNumber(3, new FString());
        Map<String, String> information = formatter.getInformation();
        assertEquals(information.get("Type"), "Number");
        assertEquals(information.get("Decimal"), "3");
    }
    
    @Test
    public void formatEmptyString() {
        Formatter formatter = new FNumber(3, new FString());
        assertEquals(formatter.format(""), "");
    }

}
