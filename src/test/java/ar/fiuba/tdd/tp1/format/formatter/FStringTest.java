package ar.fiuba.tdd.tp1.format.formatter;

import ar.fiuba.tdd.tp1.format.formatter.FString;
import ar.fiuba.tdd.tp1.format.formatter.Formatter;

import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

public class FStringTest {

    @Test
    public void formatString() {
        String value = "Hola";
        Formatter formatter = new FString();
        assertEquals(formatter.format(value), value);
    }

    @Test
    public void getInformation() {
        Formatter formatter = new FString();
        Map<String, String> information = formatter.getInformation();
        assertEquals(information.get("Type"), "String");
    }

}
