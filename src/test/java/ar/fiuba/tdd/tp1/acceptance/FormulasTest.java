package ar.fiuba.tdd.tp1.acceptance;

import ar.fiuba.tdd.tp1.acceptance.driver.BadFormulaException;
import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetDriverImpl;
import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetTestDriver;
import ar.fiuba.tdd.tp1.acceptance.driver.UndeclaredWorkSheetException;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class FormulasTest {

    private static final double DELTA = 0.0001;
    private SpreadSheetDriverImpl testDriver;

    @Before
    public void setUp() {
        testDriver = new SpreadSheetDriverImpl();
    }

    @Test
    public void sumLiterals() {
        // parser no contempla el uso de formulas + -1
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + 0 + 3.5 + -1");
        // testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + 0 + 3.5 - 1");

        assertEquals(3.5, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);
    }

    @Test
    public void sumLiteralsInDifferentRows() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "1");
        testDriver.setCellValue("tecnicas", "default", "B2", "2");
        testDriver.setCellValue("tecnicas", "default", "C3", "= A1 + B2");

        assertEquals(1 + 2, testDriver.getCellValueAsDouble("tecnicas", "default", "C3"), DELTA);
    }

    @Test
    public void subtractLiterals() {
        testDriver.createNewWorkBookNamed("tecnicas");
        // testDriver.setCellValue("tecnicas", "default", "A1", "= 1 - 0 - 3.5 +1");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 - 0 - 3.5 - -1");

        assertEquals(1 - 3.5 - -1, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"),
                DELTA);
    }

    @Test
    public void formulaWithReferences() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + A2 + 0.5 - A3");
        testDriver.setCellValue("tecnicas", "default", "A2", "5");
        testDriver.setCellValue("tecnicas", "default", "A3", "2");

        assertEquals(1 + 5 + 0.5 - 2, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"),
                DELTA);
    }

    @Test
    public void formulaWithReferenceToReference() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + A2"); // 6
        testDriver.setCellValue("tecnicas", "default", "A2", "= 2 + A3"); // 5
        testDriver.setCellValue("tecnicas", "default", "A3", "3");

        // assertEquals(1 + 2 + 3, testDriver.getCellValueAsDouble("tecnicas", "default", "A3"),
        // DELTA);
        assertEquals(1 + 2 + 3, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);
    }

    @Test
    public void formulaWithReferencesFromOtherSpreadSheet() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.createNewWorkSheetNamed("tecnicas", "other");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + !other.A2 + 0.5 - A3");
        testDriver.setCellValue("tecnicas", "other", "A2", "-1");
        testDriver.setCellValue("tecnicas", "default", "A3", "2");

        assertEquals(1 + -1 + 0.5 - 2,
                testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);
    }

    @Test
    public void formulaWithReferencesFromOtherSpreadSheetNiceTyping() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.createNewWorkSheetNamed("tecnicas", "other");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + other!A2 + 0.5 - A3");
        testDriver.setCellValue("tecnicas", "other", "A2", "-1");
        testDriver.setCellValue("tecnicas", "default", "A3", "2");

        assertEquals(1 + -1 + 0.5 - 2,
                testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);
    }

    @Test(expected = BadFormulaException.class)
    public void badFormulaStringAndNumber() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + hello");

        testDriver.getCellValueAsDouble("tecnicas", "default", "A1");
    }

    @Test
    public void badFormulaStringAndNumberRetrieveAsString() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + hello");

        assertEquals("Error:BAD_FORMULA",
                testDriver.getCellValueAsString("tecnicas", "default", "A1"));
    }

    @Test(expected = BadFormulaException.class)
    public void badFormulaWithReference() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + A2");
        testDriver.setCellValue("tecnicas", "default", "A2", "Hello");

        testDriver.getCellValueAsDouble("tecnicas", "default", "A1");
    }

    @Test(expected = BadFormulaException.class)
    public void badFormulaWithoutEqualSymbol() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "1 + 2");

        testDriver.getCellValueAsDouble("tecnicas", "default", "A1");
    }

    @Test(expected = UndeclaredWorkSheetException.class)
    public void undeclaredWorkSheetInvocation() {
        testDriver.createNewWorkBookNamed("tecnicas");

        testDriver.getCellValueAsString("tecnicas", "undeclaredWorkSheet", "A1");
    }

    @Test
    public void setDateAndAddDays() {
        testDriver.createNewWorkBookNamed("tecnicas");

        testDriver.setCellValue("tecnicas", "default", "A1", "23-11-1990");
        testDriver.setCellValue("tecnicas", "default", "A2", "=A1+4");
        testDriver.setCellFormatDate("tecnicas", "default", "A2", "dd-mm-yyyy");
        assertTrue(testDriver.getCellValueAsString("tecnicas", "default", "A2").equals(
                "27-11-1990"));
    }

}
