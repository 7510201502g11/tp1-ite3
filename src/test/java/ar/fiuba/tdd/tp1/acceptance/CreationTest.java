package ar.fiuba.tdd.tp1.acceptance;

import ar.fiuba.tdd.tp1.acceptance.driver.BookNotFoundException;
import ar.fiuba.tdd.tp1.acceptance.driver.ExistingBookFoundException;
import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetDriverImpl;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasItems;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class CreationTest {
    private SpreadSheetDriverImpl testDriver;

    @Before
    public void setUp() {
        testDriver = new SpreadSheetDriverImpl();
    }

    @Test
    public void startWithNoWorkBooks() {
        assertTrue(testDriver.workBooksNames().isEmpty());
    }

    @Test
    public void createOneWorkBook() {
        testDriver.createNewWorkBookNamed("tecnicas");

        assertThat(testDriver.workBooksNames(), contains("tecnicas"));
    }
    
    @Test (expected = ExistingBookFoundException.class)
    public void createOneWorkBook2() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.createNewWorkBookNamed("tecnicas");
    }

    @Test
    public void createMultipleWorkBooks() {
        testDriver.createNewWorkBookNamed("tecnicas 1");
        testDriver.createNewWorkBookNamed("tecnicas 2");

        assertThat(testDriver.workBooksNames(), contains("tecnicas 1", "tecnicas 2"));
        assertThat(testDriver.workBooksNames(), not(contains("tecnicas")));
    }

    @Test
    public void workBookStartsWithDefaultWorkSheet() {
        testDriver.createNewWorkBookNamed("tecnicas");

        assertThat(testDriver.workSheetNamesFor("tecnicas"), contains("default"));
    }

//    @Ignore("Criterios incompatibles respecto a FormulasTest::formulaWithReferencesFromOtherSpreadSheet")
    @Test
    public void createOneWorkSheet() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.createNewWorkSheetNamed("tecnicas", "other");

        assertThat(testDriver.workSheetNamesFor("tecnicas"), hasItems("other"));
    }

    @Test
    public void createAdditionalWorkSheets() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.createNewWorkSheetNamed("tecnicas", "firstAdditionalWorksheet");
        testDriver.createNewWorkSheetNamed("tecnicas", "secondAdditionalWorksheet");

        assertTrue(testDriver.workSheetNamesFor("tecnicas").contains("firstAdditionalWorksheet"));

        //        FIXME : estos asserts deberian andar, pero no funcan, preguntar  a diego
        //        assertThat(testDriver.workSheetNamesFor("tecnicas"), contains("firstAdditionalWorksheet"));
        //        assertThat(testDriver.workSheetNamesFor("tecnicas"), contains("secondAdditionalWorksheet"));
    }
    
    @Test (expected = BookNotFoundException.class)
    public void createOneWorkSheet2() {
        testDriver.createNewWorkSheetNamed("tecnicas", "other");
    }

}
