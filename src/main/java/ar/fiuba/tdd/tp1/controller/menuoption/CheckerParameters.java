package ar.fiuba.tdd.tp1.controller.menuoption;

/**
 * Esta clase se encarga de verificar si el vector de string guarda una cierta cantidad de strings.
 * 
 * @author Galli
 *
 */
public class CheckerParameters {

    /**
     * Verifica si el vector que se paso por parametro tiene numParametros en su interior.
     * 
     * @param numParameter
     *            : cantidad de parametros que debe tener el parameter.
     * @param parameter
     *            : vector con los parametros que se quieren controlar.
     * @return true si tiene la cantidad de parametros que se esperaba y en caso contrario imprime
     *         un mensaje de error por consola y devuelve false.
     */
    public boolean hasTheNecessaryParameters(int numParameter, String[] parameter) {
        if ( parameter.length == numParameter) {
            return true;
        } else {
            if (parameter.length < numParameter ) {
                System.out
                        .println("Le faltan parametros a su comando, consulte la ayuda (Comando 'help').");
            } else {
                System.out.println("Hay parametros de mas en su comando, consulte la ayuda (Comando 'help').");
            }
        }
        return false;
    }

}
