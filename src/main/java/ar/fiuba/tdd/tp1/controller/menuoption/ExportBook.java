package ar.fiuba.tdd.tp1.controller.menuoption;

import ar.fiuba.tdd.tp1.controller.ControllerWindow;
import ar.fiuba.tdd.tp1.controller.MenuOption;

/**
 * Esta clase se encarga de exportar un libro que se esta modificando.
 * 
 * @author Galli
 *
 */
public class ExportBook extends MenuOption {

    public ExportBook() {
        numParameter = 3;
    }

    @Override
    protected void executeMethod(String[] parameter, ControllerWindow controller) {
        controller.exportBook(parameter[1], parameter[2]);
    }
}
