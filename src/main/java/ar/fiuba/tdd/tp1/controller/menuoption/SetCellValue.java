package ar.fiuba.tdd.tp1.controller.menuoption;

import ar.fiuba.tdd.tp1.controller.ControllerWindow;
import ar.fiuba.tdd.tp1.controller.MenuOption;

/**
 * Esta clase se encarga de insertar un valor a una celda en un libro del controlador.
 * 
 * @author Galli
 *
 */
public class SetCellValue extends MenuOption {

    @Override
    protected void executeMethod(String[] parameter, ControllerWindow controller) {
        controller.setCellValue(parameter[1], parameter[2], parameter[3], parameter[4]);
    }

}
