package ar.fiuba.tdd.tp1.controller.menuoption;

import ar.fiuba.tdd.tp1.controller.ControllerWindow;
import ar.fiuba.tdd.tp1.controller.MenuOption;

/**
 * Esta clase se encarga de ponerle formato de meneda a una celda de un libro del controller.
 * 
 * @author Galli
 *
 */
public class SetCellFormatMoney extends MenuOption {

    @Override
    protected void executeMethod(String[] parameter, ControllerWindow controller) {
        controller.setCellFormatMoney(parameter[1], parameter[2], parameter[3], parameter[4]);
    }

}
