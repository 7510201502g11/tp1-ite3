package ar.fiuba.tdd.tp1.controller.menuoption;

import ar.fiuba.tdd.tp1.controller.ControllerWindow;
import ar.fiuba.tdd.tp1.controller.MenuOption;

/**
 * Esta clase ejecuta el deshacer en el controlador.
 * 
 * @author Galli
 *
 */
public class Undo extends MenuOption {

    public Undo() {
        this.numParameter = 1;
    }

    @Override
    protected void executeMethod(String[] parameter, ControllerWindow controller) {
        controller.undo();
    }

}
