package ar.fiuba.tdd.tp1.controller.menuoption;

import ar.fiuba.tdd.tp1.controller.ControllerWindow;
import ar.fiuba.tdd.tp1.controller.MenuOption;

/**
 * Esta clase se encarga de eliminar una hoja de un libro del controller.
 * 
 * @author Galli
 *
 */
public class RemoveWorkSheet extends MenuOption {

    public RemoveWorkSheet() {
        numParameter = 3;
    }

    @Override
    protected void executeMethod(String[] parameter, ControllerWindow controller) {
        controller.removeWorkSheetNamed(parameter[2], parameter[1]);
    }

}
