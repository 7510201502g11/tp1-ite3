package ar.fiuba.tdd.tp1.controller;

/**
 * Esta interfaz la debe implementar aquella clase que mantiene la hoja y book que se esta
 * modificando.
 * 
 * @author Galli
 *
 */
public interface SetterCurrentSheetAndBook {

    /**
     * Actualiza el nombre de la hoja y el libro.
     * 
     * @param newSheet
     *            : nombre de la hoja.
     * @param newBook
     *            : nombre del libro.
     */
    public void setCurrentSheetAndBook(String newSheet, String newBook);

}
