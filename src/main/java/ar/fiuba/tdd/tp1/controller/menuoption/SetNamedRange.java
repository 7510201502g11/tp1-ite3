package ar.fiuba.tdd.tp1.controller.menuoption;

import ar.fiuba.tdd.tp1.controller.ControllerWindow;
import ar.fiuba.tdd.tp1.controller.MenuOption;

public class SetNamedRange extends MenuOption {

    public SetNamedRange() {
        this.numParameter = 2;
    }

    @Override
    protected void executeMethod(String[] parameter, ControllerWindow controller) {

        String[] rangeDefinition = parameter[1].split("=");
        controller.setNamedRange(rangeDefinition[0], rangeDefinition[1]);
    }
}
