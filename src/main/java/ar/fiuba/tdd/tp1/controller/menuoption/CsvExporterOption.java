package ar.fiuba.tdd.tp1.controller.menuoption;

import ar.fiuba.tdd.tp1.controller.ControllerWindow;
import ar.fiuba.tdd.tp1.controller.MenuOption;

/**
 * Esta clase se encarga de exportar un CSV de la hoja que se esta modificando.
 * 
 * @author Galli
 *
 */
public class CsvExporterOption extends MenuOption {

    public CsvExporterOption() {
        numParameter = 2;
    }

    @Override
    protected void executeMethod(String[] parameter, ControllerWindow controller) {
        controller.persistCurrentSheet(parameter[1]);
    }

}
