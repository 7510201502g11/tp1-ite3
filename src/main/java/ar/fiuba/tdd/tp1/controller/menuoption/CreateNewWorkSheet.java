package ar.fiuba.tdd.tp1.controller.menuoption;

import ar.fiuba.tdd.tp1.controller.ControllerWindow;
import ar.fiuba.tdd.tp1.controller.MenuOption;

/**
 * Esta clase se encarga de crear una hoja en un libro del controller.
 * 
 * @author Galli
 *
 */
public class CreateNewWorkSheet extends MenuOption {

    public CreateNewWorkSheet() {
        numParameter = 3;
    }

    @Override
    protected void executeMethod(String[] parameter, ControllerWindow controller) {
        controller.createNewWorkSheetNamed(parameter[2], parameter[1]);
    }

}
