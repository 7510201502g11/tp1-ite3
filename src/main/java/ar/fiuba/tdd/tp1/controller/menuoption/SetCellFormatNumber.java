package ar.fiuba.tdd.tp1.controller.menuoption;

import ar.fiuba.tdd.tp1.controller.ControllerWindow;
import ar.fiuba.tdd.tp1.controller.MenuOption;

/**
 * Esta clase se encarga de ponerle formato de numero a una celda de un libro del controller.
 * 
 * @author Galli
 *
 */
public class SetCellFormatNumber extends MenuOption {

    @Override
    protected void executeMethod(String[] parameter, ControllerWindow controller) {
        controller.setCellFormatNumber(parameter[1], parameter[2], parameter[3],
                Integer.parseInt(parameter[4]));
    }

}
