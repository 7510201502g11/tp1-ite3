package ar.fiuba.tdd.tp1.controller;

import ar.fiuba.tdd.tp1.controller.menuoption.CheckerParameters;

/**
 * Esta interfaz la deben implementar las opciones del menu.
 * 
 * @author Galli
 *
 */
public abstract class MenuOption {

    protected int numParameter = 5;

    /**
     * Ejecuta el comando correspondiente.
     * 
     * @param parameter
     *            : son los parametros que se necesitan para ejecutar el comando.
     * @param controller
     *            : es el controlador al que le van a ejecutar el comando.
     */
    protected abstract void executeMethod(String[] parameter, ControllerWindow controller);

    /**
     * Ejecuta la opcion con los parametros pasados.
     * 
     * @param parameter
     *            son los parametros que necesita la operacion.
     */
    public void execute(String[] parameter, ControllerWindow controller) {
        CheckerParameters checkerParameters = new CheckerParameters();
        if (checkerParameters.hasTheNecessaryParameters(numParameter, parameter)) {
            this.executeMethod(parameter, controller);
        }
    }

}
