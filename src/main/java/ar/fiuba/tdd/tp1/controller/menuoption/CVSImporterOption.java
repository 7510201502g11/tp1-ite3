package ar.fiuba.tdd.tp1.controller.menuoption;

import ar.fiuba.tdd.tp1.controller.ControllerWindow;
import ar.fiuba.tdd.tp1.controller.MenuOption;

public class CVSImporterOption extends MenuOption {
    
    public CVSImporterOption() {
        this.numParameter = 4;
    }

    @Override
    protected void executeMethod(String[] parameter, ControllerWindow controller) {
        controller.importCVS(parameter[1], parameter[2], parameter[3]);
    }
}
