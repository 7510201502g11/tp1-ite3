package ar.fiuba.tdd.tp1.controller.menuoption;

import ar.fiuba.tdd.tp1.controller.ControllerWindow;
import ar.fiuba.tdd.tp1.controller.MenuOption;

/**
 * Esta clase se encarga de importar un book.
 * 
 * @author Galli
 *
 */
public class ImportBook extends MenuOption {

    public ImportBook() {
        numParameter = 3;
    }

    @Override
    protected void executeMethod(String[] parameter, ControllerWindow controller) {
        controller.importBook(parameter[1], parameter[2]);
    }
}
