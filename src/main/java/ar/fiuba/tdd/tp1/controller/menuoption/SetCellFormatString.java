package ar.fiuba.tdd.tp1.controller.menuoption;

import ar.fiuba.tdd.tp1.controller.ControllerWindow;
import ar.fiuba.tdd.tp1.controller.MenuOption;

/**
 * Esta clase se encarga de ponerle formato de string a una celda de un libro del controller.
 * 
 * @author Galli
 *
 */
public class SetCellFormatString extends MenuOption {

    public SetCellFormatString() {
        numParameter = 4;
    }

    @Override
    protected void executeMethod(String[] parameter, ControllerWindow controller) {
        controller.setCellFormatString(parameter[1], parameter[2], parameter[3]);
    }

}
