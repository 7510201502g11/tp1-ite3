package ar.fiuba.tdd.tp1.acceptance.driver;

public class BadFormulaException extends RuntimeException {

    public BadFormulaException(Throwable cause) {
        super(cause);
    }

    public BadFormulaException(String string) {
        super(string);
    }
    
}
