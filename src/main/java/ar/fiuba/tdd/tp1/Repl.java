package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.controller.ControllerConsoleWindow;

/**
 * Es la aplicacion que usara el cliente para poder ingresar los comandos y visualizar los cambios
 * por medio de una tabla.
 * 
 * @author Galli
 *
 */
public class Repl {

    public static void main(String[] args) {
        ControllerConsoleWindow controller = new ControllerConsoleWindow();
        controller.run();
    }

}
