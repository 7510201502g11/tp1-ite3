package ar.fiuba.tdd.tp1.util;

/**
 * Utilidades de comparacion.
 * 
 * @author martin
 *
 */
public class CompareUtils {
    @FunctionalInterface
    static interface CompareMode {
        boolean compare(Object left, boolean all, Object right);
    }

    /**
     * Retorna true si left es igual a alguno de los rights. simplifica la accion a.equals(b) ||
     * a.equals(c) || a.equals(d)...
     * 
     * @param left
     *            - Objeto izquierdo a comparar.
     * @param rights
     *            - Objetos "derechos" a comparar.
     * @return true si left es igual a alguno de los rights.
     */
    private static boolean equalsGroup(Object left, CompareMode compareMode, Object... rights) {
        boolean allEquals = false;
        for (int i = 0; i < rights.length; i++) {
            Object right = rights[i];
            if (right == null) {
                continue;
            }
            allEquals = compareMode.compare(left, allEquals, right);
            if (allEquals) {
                break;
            }
        }

        return allEquals;
    }

    public static boolean equalsOr(Object left, Object... rights) {
        return equalsGroup(left, CompareUtils::orCompare, rights);
    }

    public static boolean equalsAnd(Object left, Object... rights) {
        return equalsGroup(left, CompareUtils::andCompare, rights);
    }

    private static boolean orCompare(Object left, boolean allEquals, Object right) {
        return allEquals || left.equals(right);
    }

    private static boolean andCompare(Object left, boolean allEquals, Object right) {
        return allEquals && left.equals(right);
    }
}
