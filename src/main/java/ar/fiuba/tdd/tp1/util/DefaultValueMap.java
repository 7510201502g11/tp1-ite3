package ar.fiuba.tdd.tp1.util;

import java.util.HashMap;

/**
 * Representa un mapa que puede almacenar un valor por defecto asignado, de forma tal que si se
 * accede a una llave que no tiene valor asignado, el mapa retorna el valor por defecto.
 * 
 * @author martin
 *
 * @param <K>
 *            Tipo de clave.
 * @param <V>
 *            Tipo de Valor.
 */
public class DefaultValueMap<K, V> extends HashMap<K, V> {
    private static final long serialVersionUID = 1L;

    private V defaultValue;

    public DefaultValueMap<K, V> setDefaultValue(V defaultValue) {
        this.defaultValue = defaultValue;
        return this;
    }

    @Override
    public V get(Object key) {
        return super.getOrDefault(key, defaultValue);
    }
}
