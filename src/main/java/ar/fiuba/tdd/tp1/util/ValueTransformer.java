package ar.fiuba.tdd.tp1.util;

/**
 * Transformador de valores Object hacia diferentes tipos.
 * 
 * @author martin
 *
 */
public class ValueTransformer {
    private static ValueTransformer valueTransformer = new ValueTransformer();

    protected ValueTransformer() {
    }

    public static ValueTransformer get() {
        return valueTransformer;
    }

    /**
     * Transforma un Object en un Double. Ej: asDouble("12") => 12.0.
     * 
     * @param obj
     *            - Valor a transformar.
     * @return Valor como Double.
     */
    public Double asDouble(Object obj) {
        return Double.parseDouble(obj.toString());
    }

    /**
     * Intenta transformar un valor en un String con formato Double (ej: "1" -> "1.0").
     * 
     * @param obj
     *            - Valor a transformar.
     * @return Valor como String con formato Double u obj.toString() en caso de no ser posible.
     */
    public String asDoubleString(Object obj) {
        String str = obj.toString().trim();
        if (VarRef.get().doubleMatcher().matches(str)) {
            return Double.valueOf(str).toString();
        }

        return obj.toString();
    }
}
