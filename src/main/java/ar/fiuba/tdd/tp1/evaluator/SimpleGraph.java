package ar.fiuba.tdd.tp1.evaluator;

public interface SimpleGraph<VertexT> {

    SimpleGraph<VertexT> addVertex(VertexT vertexName);

    SimpleGraph<VertexT> addEdge(VertexT orgVertexName, VertexT dstVertexName);

    boolean cycleDetected();

    boolean noCyclesFound();
}