package ar.fiuba.tdd.tp1.evaluator;

import ar.fiuba.tdd.tp1.evaluator.impl.CellEvaluatorImpl;
import ar.fiuba.tdd.tp1.evaluator.impl.CycleDetectedException;
import ar.fiuba.tdd.tp1.parser.ParserBuilder;
import ar.fiuba.tdd.tp1.parser.impl.RangePairParserBuilder;
import ar.fiuba.tdd.tp1.spreadsheet.CellVarIdAccessor;
import ar.fiuba.tdd.tp1.util.VarRef;

import java.util.Optional;

/**
 * Esta clase se encarga de interpretar el contenido de una celda. Si guardaba una operacion
 * entonces la resuelve, si tenia un texto o un numero entonces lo devuelve sin hacer cambios.
 *
 */
public class CellInterpreterImpl implements CellInterpreter {
    //    private ParserBuilder parserBuilder = new StandardExcelParserBuilder();
    private ParserBuilder parserBuilder = new RangePairParserBuilder();
    private String result;
    private String sheetName = "";
    private String col = "";
    private Integer row = 0;

    public String getCol() {
        return col;
    }

    public CellInterpreterImpl setCol(String col) {
        this.col = Optional.ofNullable(col).orElse("");
        return this;
    }

    public Integer getRow() {
        return row;
    }

    public CellInterpreterImpl setRow(Integer row) {
        this.row = Optional.ofNullable(row).orElse(0);
        return this;
    }

    public CellInterpreterImpl() {
    }

    public CellInterpreterImpl(String sheetName) {
        this.sheetName = sheetName;
    }

    /**
     * Calcula la operacion que guarda el cellRawValue.
     * 
     * @param cellRawValue
     *            : es la operacion que se quiere calcular.
     * @param cellAccessor
     *            : es un objeto que me permite obtener el contenido de otras celdas.
     * @return devuelve Formula Invalida si la operacion no se puede resolver, en caso contrario
     *         devuelve el valor de la operacion.
     */
    private String getCellDoubleValue(String cellRawValue, CellVarIdAccessor<String> cellAccessor) {
        String relVar = VarRef.get().relVarReferenceBuilder().build(col, row.toString());
        String startVar = VarRef.get().absVarReferenceBuilder().build(sheetName, relVar);
        CellEvaluatorImpl cellEvaluatorImpl = new CellEvaluatorImpl(sheetName).currVar(startVar);
        try {
            return "" + cellEvaluatorImpl.getCellValue(cellRawValue, cellAccessor, parserBuilder);
        } catch (CycleDetectedException cycleDetectedException) {
            throw cycleDetectedException;
        } catch (Exception e) {
            return "Formula Invalida";
        }
    }

    @Override
    public String getCellValue(String cellRawValue, CellVarIdAccessor<String> cellAccessor) {
        result = cellRawValue;
        Optional.of(cellRawValue).filter(rawValue -> rawValue.startsWith("="))
                .ifPresent(rawValue -> {
                        result = getCellDoubleValue(rawValue, cellAccessor);
                    });
        return result;
    }

}
