package ar.fiuba.tdd.tp1.evaluator;

public class CellEvaluatorException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public CellEvaluatorException(Throwable cause) {
        super(cause);
        
    }
    
    public CellEvaluatorException(String msg) {
        super(msg);
        
    }

}
