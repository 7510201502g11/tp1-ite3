package ar.fiuba.tdd.tp1.evaluator.impl;

import ar.fiuba.tdd.tp1.evaluator.CellEvaluator;
import ar.fiuba.tdd.tp1.evaluator.CellEvaluatorException;
import ar.fiuba.tdd.tp1.evaluator.SimpleGraph;
import ar.fiuba.tdd.tp1.parser.Parser;
import ar.fiuba.tdd.tp1.parser.ParserBuilder;
import ar.fiuba.tdd.tp1.parser.expression.Context;
import ar.fiuba.tdd.tp1.parser.expression.Expression;
import ar.fiuba.tdd.tp1.parser.expression.impl.ContextImpl;
import ar.fiuba.tdd.tp1.parser.var.PointerVariablePair;
import ar.fiuba.tdd.tp1.spreadsheet.CellVarIdAccessor;
import ar.fiuba.tdd.tp1.util.StringUtils;
import ar.fiuba.tdd.tp1.util.VarRef;

import java.util.Optional;
import java.util.Set;

/**
 * Evaluador por defecto de formulas de celdas. No detecta ciclos de referencias.
 * 
 * @author martin
 *
 */
public class CellEvaluatorImpl implements CellEvaluator {
    private Context context = new ContextImpl();
    private String defaultSheetName = "";
    private SimpleGraph<String> simpleGraph = new StringVertexSimpleGraph();
    private String currVar = "ALPHA";

    public CellEvaluatorImpl() {
        super();
    }

    public CellEvaluatorImpl(String defaultSheetName) {
        super();
        this.defaultSheetName = defaultSheetName;
    }

    public CellEvaluatorImpl currVar(String currVar) {
        this.currVar = currVar;
        return this;
    }

    protected CellEvaluatorImpl simpleGraph(SimpleGraph<String> simpleGraph) {
        this.simpleGraph = simpleGraph;
        return this;
    }

    protected CellEvaluatorImpl context(Context ctx) {
        this.context = ctx;
        return this;
    }

    public String getDefaultSheetName() {
        return defaultSheetName;
    }

    public CellEvaluatorImpl defSheetName(String defaultSheetName) {
        this.defaultSheetName = defaultSheetName;
        return this;
    }

    @Override
    public Object getCellValue(String toParse, CellVarIdAccessor<String> cellAccessor,
            ParserBuilder parserBuilder) {
        Parser parser = parserBuilder.build().setDefaultPointer(defaultSheetName);

        if (VarRef.get().miscMatcher().matches(toParse)) {
            return toParse;
        }

        Expression mainExpression = parser.parse(toParse);

        Set<String> referencedVariables = parser.getReferencedVariables();

        simpleGraph.addVertex(currVar);

        for (String variableReference : referencedVariables) {
            String absoluteVariable = buildAbsoluteVariable(variableReference);
            String nextSheet = checkSheet(absoluteVariable);

            simpleGraph.addVertex(absoluteVariable).addEdge(currVar, absoluteVariable);
            Optional.of(simpleGraph)
                    .filter(SimpleGraph::noCyclesFound)
                    .orElseThrow(
                            () -> new CycleDetectedException("Se detecto ciclo " + simpleGraph));

            String referencedRawValue = Optional
                    .ofNullable(cellAccessor.getCellRawValue(absoluteVariable))
                    .filter(StringUtils::notEmpty)
                    .orElseThrow(
                            () -> new EmptyCellException("Celda " + absoluteVariable
                                    + " esta vacia!"));

            CellEvaluatorImpl recursiveEvaluator = new CellEvaluatorImpl().context(context)
                    .defSheetName(nextSheet).simpleGraph(simpleGraph).currVar(absoluteVariable);

            Object recVal = recursiveEvaluator.getCellValue(referencedRawValue, cellAccessor,
                    parserBuilder).toString();

            context.put(absoluteVariable, recVal);
        }

        return mainExpression.getValueAsString(context);
    }

    protected String checkSheet(String referencedVarId) {
        return PointerVariablePair.create(referencedVarId).pointer.trim();
    }

    protected String buildAbsoluteVariable(String referencedVarId) {
        return VarRef.get().absVarReferenceBuilder().build(defaultSheetName, referencedVarId);
    }
}
