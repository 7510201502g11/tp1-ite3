package ar.fiuba.tdd.tp1.evaluator;

import ar.fiuba.tdd.tp1.spreadsheet.CellVarIdAccessor;

/**
 * Esta clase se encarga de interpretar el contenido de una celda.
 *
 */
public interface CellInterpreter {

    /**
     * Diferencia si el cellRawValue es un texto, valor u operacion y devuelve el valor que
     * corresponde.
     * 
     * @param cellRawValue
     *            : es la operacion que se quiere calcular.
     * @param cellAccessor
     *            : es un objeto que me permite obtener el contenido de otras celdas.
     * @return si es un texto o valor entonces devuelve el cellRawValue, si es una operacion
     *         entonces la calcula, si da error devuelve Formula Invalida.
     */
    public String getCellValue(String cellRawValue, CellVarIdAccessor<String> cellAccessor);

}
