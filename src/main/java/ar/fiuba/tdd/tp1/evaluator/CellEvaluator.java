package ar.fiuba.tdd.tp1.evaluator;

import ar.fiuba.tdd.tp1.parser.ParserBuilder;
import ar.fiuba.tdd.tp1.spreadsheet.CellVarIdAccessor;
import ar.fiuba.tdd.tp1.util.ValueTransformer;

/**
 * Representa un evaluador de celdas.
 * 
 * @author martin.zaragoza
 *
 */
public interface CellEvaluator {
    /**
     * Establece el nombre de hoja por defecto.
     * 
     * @param defaultSheetName
     *            - nombre de hoja por defecto.
     * @return this.
     */
    public CellEvaluator defSheetName(String defaultSheetName);

    /**
     * Evalua una celda retornando su valor numerico.
     * 
     * @param toParse
     *            - formula a evaluar.
     * @param cellAccessor
     *            - permite acceso a las celdas.
     * @param parserBuilder
     *            - Constructor de parser a utilizar.
     * @return Valor numerico de celda.
     */
    default Double getCellValueAsDouble(String toParse, CellVarIdAccessor<String> cellAccessor,
            ParserBuilder parserBuilder) throws CellEvaluatorException {
        Object cellValue = this.getCellValue(toParse, cellAccessor, parserBuilder);
        return ValueTransformer.get().asDouble(cellValue);
    }

    /**
     * Evalua una celda retornando su valor evaluado.
     * 
     * @param toParse
     *            - formula a evaluar.
     * @param cellAccessor
     *            - permite acceso a las celdas.
     * @param parserBuilder
     *            - Constructor de parser a utilizar.
     * @return Valor de celda.
     */
    Object getCellValue(String toParse, CellVarIdAccessor<String> cellAccessor,
            ParserBuilder parserBuilder) throws CellEvaluatorException;
}
