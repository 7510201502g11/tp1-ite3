package ar.fiuba.tdd.tp1.model.cell;

public class CrpCell implements Cell<CrPair,String> {
    private CellImpl<CrPair,String> stdCell;

    /**
     * Construye una celda con un id asignado, dejandola en un estado valido.
     * 
     * @param id : un id representando el par (fila, columna).
     */
    public CrpCell(CrPair id) {
        stdCell = new CellImpl<CrPair,String>(id);
        this.stdCell.setRawValue("");
    }

    /**
     * Contruye una celda con un id asignado y un valor, dejandola en un estado valido
     * 
     * @param id : un id representando (fila,columna)
     * @param rawValue : un valor crudo para la celda.
     */
    public CrpCell(CrPair id, String rawValue) {
        stdCell = new CellImpl<CrPair,String>(id, rawValue);
    }

    @Override
    public Cell<CrPair,String> setRawValue(String value) {
        return stdCell.setRawValue(value);
    }

    @Override
    public String getRawValue() {
        return stdCell.getRawValue();
    }

    @Override
    public CrPair getId() {
        return stdCell.getId();
    }
}
