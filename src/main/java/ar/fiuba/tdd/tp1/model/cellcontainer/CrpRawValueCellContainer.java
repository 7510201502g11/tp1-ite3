package ar.fiuba.tdd.tp1.model.cellcontainer;

import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;
import ar.fiuba.tdd.tp1.model.cell.Cell;
import ar.fiuba.tdd.tp1.model.cell.CrPair;

public interface CrpRawValueCellContainer extends CellContainer<CrPair, String> {

    /**
     * Inserta un valor en la posicion dada por un identificador, de haber una celda, inserta el
     * valor en ella, caso contrario, crea la celda.
     * 
     * @param rawValue
     *            : un vlaor a asignar.
     * @param cellIdVal
     *            : un identificador de celda.
     * @return : devuelve la celda del contenedor con su valor asignado.
     * @throws InvalidCellIdentifierException
     *             : devuelve esta excepcion en caso de que el identificador de la celda sea
     *             invalido.
     */
    public Cell<CrPair, String> putCell(String rawValue, CrPair cellIdVal)
            throws InvalidCellIdentifierException;

    /**
     * Quita una celda del contenedor.
     * 
     * @param cell
     *            : una celda valida a remover.
     * @return : devuelve la celda removida del contenedor.
     */
    public Cell<CrPair, String> removeCell(Cell<CrPair, String> cell);

}
