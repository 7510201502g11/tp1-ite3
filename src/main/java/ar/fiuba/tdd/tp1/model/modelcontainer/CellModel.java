package ar.fiuba.tdd.tp1.model.modelcontainer;

import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetDriverImpl;
import ar.fiuba.tdd.tp1.formatconverter.FormatConverterFactoryImpl;
import ar.fiuba.tdd.tp1.model.cell.CrpCell;

import java.util.HashMap;

public class CellModel {
    private String sheet;
    private String id;
    private String value;
    private String type;
    private HashMap<String, String> formatter;

    public CellModel() {
        this.id = "";
        this.value = "";
        this.type = "";
        this.formatter = new HashMap<String, String>();
    }

    public CellModel(CrpCell cell) {
        this.id = cell.getId().toString();
        this.value = cell.getRawValue();
        this.type = "";
        this.formatter = new HashMap<String, String>();
    }

    public String getSheetName() {
        return sheet;
    }

    public void setSheetName(String sheetName) {
        this.sheet = sheetName;
    }

    public String getCellId() {
        return id;
    }

    public void setCellId(String cellId) {
        this.id = cellId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void addCellToSpreadSheet(SpreadSheetDriverImpl driver, String bookName) {
        driver.setCellValue(bookName, this.sheet, this.id, this.value);
        FormatConverterFactoryImpl formatFactory = new FormatConverterFactoryImpl();
        FormatterModel formatterModel = new FormatterModel();
        formatterModel.setFormatter(this.formatter);
        formatFactory.create(this.type).convert(formatterModel, this.id, this.sheet, driver,
                bookName);
    }
    
    public HashMap<String, String> getFormatter() {
        return formatter;
    }

    public void setFormatter(HashMap<String, String> formatter) {
        this.formatter = formatter;
    }

    public String getValueOfFormatter(String formatterName) {
        return this.formatter.get(formatterName);
    }

}