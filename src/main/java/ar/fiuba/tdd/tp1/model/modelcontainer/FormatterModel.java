package ar.fiuba.tdd.tp1.model.modelcontainer;

import java.util.HashMap;

public class FormatterModel {

    private HashMap<String, String> formatter;

    @SuppressWarnings("CPD-START")
    public void setFormatter(HashMap<String, String> formatter) {
        this.formatter = formatter;
    }

    public String getValueOfFormatter(String formatterName) {
        return this.formatter.get(formatterName);
    }
    
    @SuppressWarnings("CPD-END")
    public HashMap<String, String> getFormatter() {
        return formatter;
    }
}
