package ar.fiuba.tdd.tp1.model.modelcontainer;

import java.util.List;

public class SpreadSheetModel {
    private String name;
    private String version;
    private List<CellModel> cells;

    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getVersion() {
        return version;
    }
    

    public void setVersion(String version) {
        this.version = version;
    }
    
    public List<CellModel> getCells() {
        return cells;
    }
    
    public void setCells(List<CellModel> cells) {
        this.cells = cells;
    }
    
}
