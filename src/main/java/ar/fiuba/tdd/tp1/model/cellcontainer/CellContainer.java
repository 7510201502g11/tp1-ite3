package ar.fiuba.tdd.tp1.model.cellcontainer;

import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;
import ar.fiuba.tdd.tp1.model.cell.Cell;

import java.util.Map;

public interface CellContainer<CellIdType, DataType> {

    /**
     * Inserta una celda valida en el contenedor
     * 
     * @param cell
     *            : una celda en estado valido.
     */
    public Cell<CellIdType, DataType> putCell(Cell<CellIdType, DataType> cell);

    /**
     * Obtiene la celda solicitada con un identificador del contenedor.
     * 
     * @param cellIdVal
     *            : un identificador de celda.
     * @return : devuelve la celda solicitada al contenedor.
     * @throws InvalidCellIdentifierException
     *             : devuelve esta excepcion en caso de que el identificador de la celda sea
     *             invalido.
     */
    public Cell<CellIdType, DataType> getCell(CellIdType cellIdVal)
            throws InvalidCellIdentifierException;

    /**
     * Quita una celda del contenedor.
     * 
     * @param cellId
     *            : un identificador de la celda.
     * @return : devuelve la celda removida del contenedor.
     */
    public Cell<CellIdType, DataType> removeCell(CellIdType cellId);

    /**
     * Devuelve un mapa con las celdas que guarda el contenedor.
     * 
     * @return un mapa.
     */
    public Map<CellIdType, Cell<CellIdType, DataType>> getCells();

}
