package ar.fiuba.tdd.tp1.model.cellcontainer;

import ar.fiuba.tdd.tp1.model.cell.Cell;

import java.util.HashMap;
import java.util.Map;

/**
 * Implementacion de un contenedor de celda con la interfaz CellContainer.
 *
 * @param <CellIdentityTypeT>
 *            : Un tipo de identificador de celda.
 */
public class CellContainerImpl<CellIdentityTypeT, DataTypeT> implements
        CellContainer<CellIdentityTypeT, DataTypeT> {
    private Map<CellIdentityTypeT, Cell<CellIdentityTypeT, DataTypeT>> cellMap = 
            new HashMap<CellIdentityTypeT, Cell<CellIdentityTypeT, DataTypeT>>();

    @Override
    public Cell<CellIdentityTypeT, DataTypeT> putCell(Cell<CellIdentityTypeT, DataTypeT> cell) {
        cellMap.put(cell.getId(), cell);
        return cell;
    }

    @Override
    public Cell<CellIdentityTypeT, DataTypeT> getCell(CellIdentityTypeT cellIdVal) {
        if (cellMap.containsKey(cellIdVal)) {
            return cellMap.get(cellIdVal);
        }
        return null;
    }

    @Override
    public Cell<CellIdentityTypeT, DataTypeT> removeCell(CellIdentityTypeT cellId) {
        return cellMap.remove(cellId);
    }

    @SuppressWarnings("unchecked")
    public Map<CellIdentityTypeT, Cell<CellIdentityTypeT, DataTypeT>> getCells() {
        Map<CellIdentityTypeT, Cell<CellIdentityTypeT, DataTypeT>> newMap = 
                new HashMap<CellIdentityTypeT, Cell<CellIdentityTypeT, DataTypeT>>();
        newMap.putAll(cellMap);
        return newMap;
    }

}
