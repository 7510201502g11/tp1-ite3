package ar.fiuba.tdd.tp1.model.cellcontainer;

import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;
import ar.fiuba.tdd.tp1.model.cell.Cell;
import ar.fiuba.tdd.tp1.model.cell.CellImpl;
import ar.fiuba.tdd.tp1.model.cell.CrPair;

import java.util.Map;


public class CrpRawValueCellContainerImpl implements CrpRawValueCellContainer {

    private CrpCellContainer<String> cellContainer = new CrpCellContainer<String>();

    @Override
    public Cell<CrPair, String> putCell(Cell<CrPair, String> cell) {
        return cellContainer.putCell(cell);
    }

    @Override
    public Cell<CrPair, String> putCell(String rawValue, CrPair cellIdVal)
            throws InvalidCellIdentifierException {
        return cellContainer.putCell(new CellImpl<CrPair, String>(cellIdVal, rawValue));
    }

    @Override
    public Cell<CrPair, String> getCell(CrPair cellIdVal) throws InvalidCellIdentifierException {
        Cell<CrPair, String> cell = cellContainer.getCell(cellIdVal);
        if (cell == null) {
            return new CellImpl<CrPair, String>(cellIdVal, "");
        }
        return cell;
    }

    @Override
    public Cell<CrPair, String> removeCell(CrPair cellId) {
        return cellContainer.removeCell(cellId);
    }

    @Override
    public Cell<CrPair, String> removeCell(Cell<CrPair, String> cell) {
        return cellContainer.removeCell(cell.getId());
    }

    @Override
    public Map<CrPair, Cell<CrPair, String>> getCells() {
        return this.cellContainer.getCells();
    }

}
