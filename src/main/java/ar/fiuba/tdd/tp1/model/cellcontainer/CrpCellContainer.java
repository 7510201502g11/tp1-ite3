package ar.fiuba.tdd.tp1.model.cellcontainer;

import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;
import ar.fiuba.tdd.tp1.model.cell.Cell;
import ar.fiuba.tdd.tp1.model.cell.CellImpl;
import ar.fiuba.tdd.tp1.model.cell.CrPair;

import java.util.Map;
import java.util.Optional;

public class CrpCellContainer<DataTypeT> implements CellContainer<CrPair, DataTypeT> {

    private CellContainer<CrPair, DataTypeT> cellContainer = new CellContainerImpl<CrPair, DataTypeT>();

    @Override
    public Cell<CrPair, DataTypeT> putCell(Cell<CrPair, DataTypeT> cell) {
        if (checkId(cell.getId())) {
            cellContainer.putCell(cell);
            return cell;
        } else {
            throw new InvalidCellIdentifierException("Identificador de celda "
                    + cell.getId().toString() + " invalido!");
        }
    }

    @Override
    public Cell<CrPair, DataTypeT> getCell(CrPair cellIdVal) throws InvalidCellIdentifierException {
        if (checkId(cellIdVal)) {
            Cell<CrPair, DataTypeT> cell = cellContainer.getCell(cellIdVal);
            return Optional.ofNullable(cell).orElse(
                    new CellImpl<CrPair, DataTypeT>(cellIdVal, null));
        } else {
            throw new InvalidCellIdentifierException("Identificador de celda "
                    + cellIdVal.toString() + " invalido!");
        }
    }

    @Override
    public Cell<CrPair, DataTypeT> removeCell(CrPair cellId) {
        return cellContainer.removeCell(cellId);
    }

    /**
     * Chequea la validezdel identificador de celdas para esta implementacion.
     * 
     * @param cellIdVal
     *            : un identificador de celda.
     * @return : verdadero si es valido, falso si no lo es.
     */
    private boolean checkId(CrPair cellIdVal) {
        if (cellIdVal.getRow() > 0 && cellIdVal.getColumn().matches("[a-zA-Z]+")) {
            return true;
        }
        return false;
    }
    
    public Map<CrPair, Cell<CrPair, DataTypeT>> getCells() {
        return this.cellContainer.getCells();
    }
}
