package ar.fiuba.tdd.tp1.model.book;

import ar.fiuba.tdd.tp1.exceptions.model.ExistingSheetFoundException;
import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;
import ar.fiuba.tdd.tp1.exceptions.model.InvalidSheetNameException;
import ar.fiuba.tdd.tp1.exceptions.model.SheetNotFoundException;
import ar.fiuba.tdd.tp1.model.cell.Cell;
import ar.fiuba.tdd.tp1.model.cell.CrPair;
import ar.fiuba.tdd.tp1.model.cellcontainer.CrpCellContainer;
import ar.fiuba.tdd.tp1.model.sheet.CrpSheet;
import ar.fiuba.tdd.tp1.model.sheet.Sheet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class CrpBook<DataTypeT> implements Book<CrPair, DataTypeT> {
    List<Sheet<CrPair, DataTypeT>> sheets;

    public CrpBook() {
        sheets = new ArrayList<Sheet<CrPair, DataTypeT>>();
    }

    @Override
    public Book<CrPair, DataTypeT> createSheet(String name) {
        Sheet<CrPair, DataTypeT> newSheet = new CrpSheet<DataTypeT>(name,
                new CrpCellContainer<DataTypeT>());
        return putSheet(newSheet);
    }

    /**
     * Devuelve un iterador en una hoja determinada.
     * 
     * @param name
     *            : nombre de la hoja.
     * @return iterador.
     */
    private Iterator<Sheet<CrPair, DataTypeT>> getIteratorInSheet(String name) {
        Iterator<Sheet<CrPair, DataTypeT>> iter = sheets.listIterator();
        Iterator<Sheet<CrPair, DataTypeT>> oldIter = sheets.listIterator();
        while (iter.hasNext()) {
            if (iter.next().getSheetName().equals(name)) {
                return oldIter;
            }
            oldIter.next();
        }
        return null;
    }

    @Override
    public Book<CrPair, DataTypeT> removeSheet(String name) {
        Iterator<Sheet<CrPair, DataTypeT>> iter = this.getIteratorInSheet(name);
        if (iter == null) {
            throw new SheetNotFoundException("No existe hoja con ese nombre a eliminar");
        }
        iter.next();
        iter.remove();
        return this;
    }

    @Override
    public Sheet<CrPair, DataTypeT> getSheet(String name) {
        Iterator<Sheet<CrPair, DataTypeT>> iter = this.getIteratorInSheet(name);
        if (iter == null) {
            return null;
        }
        return iter.next();
    }

    @Override
    public Book<CrPair, DataTypeT> putCell(String sheet, String col, int row, DataTypeT rawValue)
            throws InvalidCellIdentifierException {
        Sheet<CrPair, DataTypeT> foundSheet = this.getSheet(sheet);
        if (foundSheet == null) {
            throw new SheetNotFoundException("Hoja no encontrada!");
        } else {
            foundSheet.putCell(col, row, rawValue);
        }
        return this;
    }

    @Override
    public Book<CrPair, DataTypeT> clearCell(String sheet, String col, int row)
            throws InvalidCellIdentifierException {
        Sheet<CrPair, DataTypeT> foundSheet = this.getSheet(sheet);
        if (foundSheet != null) {
            foundSheet.clearCell(col, row);
        } else {
            throw new SheetNotFoundException("No se encontro la hoja con el nombre solicitado.");
        }
        return this;
    }

    @Override
    public DataTypeT getCellRawValue(String sheet, String col, int row)
            throws InvalidCellIdentifierException {
        Sheet<CrPair, DataTypeT> foundSheet = this.getSheet(sheet);
        if (foundSheet != null) {
            return this.getSheet(sheet).getCellRawValue(col, row);
        } else {
            throw new SheetNotFoundException("No se encontro la hoja con el nombre solicitado.");
        }
    }

    @Override
    public DataTypeT getCellRawValue(String cellId) throws InvalidCellIdentifierException {
        String[] parsedString = cellId.split("!");
        if (parsedString.length != 2) {
            throw new InvalidCellIdentifierException("Id inv�lido!");
        }

        String sheetName = parsedString[0];
        String[] columnRow = parsedString[1].split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");
        String col = columnRow[0];
        int row = Integer.parseInt(columnRow[1]);

        return this.getCellRawValue(sheetName, col, row);
    }

    @Override
    public Book<CrPair, DataTypeT> renameSheet(String oldName, String newName) {
        Sheet<CrPair, DataTypeT> foundSheet = this.getSheet(oldName);
        if (foundSheet != null) {
            if (this.getSheet(newName) != null) {
                throw new ExistingSheetFoundException(
                        "No se puede renombrar la hoja pues el nuevo nombre ya existe en el libro.");
            } else {
                if (newName.trim().isEmpty()) {
                    throw new InvalidSheetNameException("Nombre de hoja inv�lido.");
                }
                foundSheet.renameSheet(newName);
            }
        } else {
            throw new SheetNotFoundException("La hoja con el nombre buscado no existe.");
        }
        return this;
    }

    @Override
    public boolean existsSheet(String name) {
        return (this.getSheet(name) != null);
    }

    @Override
    public List<String> getNamesSheet() {
        List<String> list = new ArrayList<String>();
        for (Sheet<CrPair, DataTypeT> sheet : sheets) {
            list.add(sheet.getSheetName());
        }
        return list;
    }

    @Override
    public Book<CrPair, DataTypeT> putSheet(Sheet<CrPair, DataTypeT> sheet) {
        Optional.of(sheet);
        if (!this.existsSheet(sheet.getSheetName())) {
            if (sheet.getSheetName().trim().isEmpty()) {
                throw new InvalidSheetNameException("Nombre de hoja inv�lido.");
            }
            this.sheets.add(sheet);
            return this;
        }
        throw new ExistingSheetFoundException("Ya existe una hoja con el mismo nombre.");
    }

    @Override
    public HashMap<String, Map<CrPair, Cell<CrPair, DataTypeT>>> getCellsInBook() {
        HashMap<String, Map<CrPair, Cell<CrPair, DataTypeT>>> result = new HashMap<String, Map<CrPair, Cell<CrPair, DataTypeT>>>();

        for (Sheet<CrPair, DataTypeT> sheet : sheets) {
            result.put(sheet.getSheetName(), sheet.getCellsInSheet());
        }

        return result;
    }

}
