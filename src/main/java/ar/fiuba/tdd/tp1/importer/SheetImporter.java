package ar.fiuba.tdd.tp1.importer;

import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetDriverImpl;

public interface SheetImporter {

    public void importFromFile(SpreadSheetDriverImpl driver, String filePath, String fileName, String bookName, String sheetName);
    
}
