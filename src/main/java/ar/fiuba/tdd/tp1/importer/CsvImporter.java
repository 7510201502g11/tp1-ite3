package ar.fiuba.tdd.tp1.importer;

import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetDriverImpl;
import ar.fiuba.tdd.tp1.exceptions.fileio.FileNotExist;
import ar.fiuba.tdd.tp1.model.cell.CrPair;
import ar.fiuba.tdd.tp1.util.ColumnIndexManager;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;

public class CsvImporter implements SheetImporter {

    private String currentMapColumn = "A";
    private int currentMapFile = 1;

    @Override
    public void importFromFile(SpreadSheetDriverImpl driver, String filePath, String filename,
            String bookName, String sheetName) {

        FileInputStream inputFile = null;
        
        createWorkSheet(driver, bookName);
        
        createSheetName(driver, bookName, sheetName);

        inputFile = openFileToRead(filePath + filename, inputFile);

        InputStreamReader reader = new InputStreamReader(inputFile, Charset.forName("UTF-8"));
        BufferedReader br = new BufferedReader(reader);

        String fileLine = null;
        readFileLines(driver, bookName, sheetName, fileLine, br);

        try {
            inputFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void createSheetName(SpreadSheetDriverImpl driver, String bookName, String sheetName) {
        if (!driver.workSheetNamesFor(bookName).contains(sheetName)) {
            driver.createNewWorkSheetNamed(bookName, sheetName);
        }
    }

    private void createWorkSheet(SpreadSheetDriverImpl driver, String bookName) {
        if (!driver.workBooksNames().contains(bookName)) {
            driver.createNewWorkBookNamed(bookName);
        }
    }

    private void readFileLines(SpreadSheetDriverImpl driver, String bookName, String sheetName,
            String fileLine, BufferedReader br) {
        List<String> cellValues;
        try {
            fileLine = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        while (fileLine != null) {
            cellValues = this.getCellsFromLine(fileLine);
            cellValues.forEach((cellValue) -> {
                    CrPair cellId = new CrPair(currentMapFile, currentMapColumn);
                    driver.setCellValue(bookName, sheetName, cellId.toString(), cellValue);
                    currentMapColumn = this.getExcelColumnName((this
                            .getExcelColumnNumber(currentMapColumn) + 1));
                }
            );
            currentMapColumn = "A";
            currentMapFile += 1;
            try {
                fileLine = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private FileInputStream openFileToRead(String filePath, FileInputStream inputFile) {
        try {
            inputFile = new FileInputStream(filePath);

        } catch (FileNotFoundException e) {
            throw new FileNotExist("el Archivo no fue encontrado");
        }
        return inputFile;
    }

    private int getExcelColumnNumber(String column) {
//      int result = 0;
//      for (int i = 0; i < column.length(); i++) {
//          result *= 26;
//          result += column.charAt(i) - 'A' + 1;
//      }
//      return result;
        return new ColumnIndexManager().getExcelColumnNumber(column);
    }

    private String getExcelColumnName(int number) {
//      final StringBuilder sb = new StringBuilder();
//
//      int num = number - 1;
//      while (num >= 0) {
//          int numChar = (num % 26) + 65;
//          sb.append((char) numChar);
//          num = (num / 26) - 1;
//      }
//      return sb.reverse().toString();
        return new ColumnIndexManager().getExcelColumnName(number);
    }
    
    private List<String> getCellsFromLine(String readLine) {
        String[] separatedValues = readLine.split(";");
        List<String> cells = Arrays.asList(separatedValues);
        return cells;
    }
    
}
