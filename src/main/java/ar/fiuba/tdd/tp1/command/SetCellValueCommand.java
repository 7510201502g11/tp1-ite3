package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;
import ar.fiuba.tdd.tp1.model.book.Book;
import ar.fiuba.tdd.tp1.spreadsheet.SpreadSheet;
import ar.fiuba.tdd.tp1.spreadsheet.SpreadSheetImpl;

import java.util.Optional;

/**
 * Esta clase se encarga de modificar una celda y de deshacer el cambio que provoco.
 * 
 * @author Galli
 *
 */
public class SetCellValueCommand<IdT,DataTypeT> implements Command {

    private Book<IdT, DataTypeT> book;
    private String sheet;
    private String col;
    private int row;
    private DataTypeT rawValueToSet;
    private DataTypeT previousRawValue;

    @SuppressWarnings("CPD-START")
    public SetCellValueCommand(Book<IdT, DataTypeT> book2, String sheet, String col,
            int row, DataTypeT rawValueToSet) {
        Optional.of(book2).of(sheet).of(col).of(row).of(rawValueToSet);
        this.book = book2;
        this.col = col;
        this.row = row;
        this.sheet = sheet;
        this.rawValueToSet = rawValueToSet;
        previousRawValue = null;
    }

    @SuppressWarnings("CPD-END")
    
    @Override
    public void execute() throws InvalidCellIdentifierException {
        this.previousRawValue = book.getCellRawValue(sheet, col, row);
        book.putCell(sheet, col, row, rawValueToSet);
    }

    @Override
    public void undo() {
        book.putCell(sheet, col, row, previousRawValue);
    }

}
