package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;

import java.util.ArrayList;
import java.util.List;

/**
 * Es una composicion de commandos, si se ejecuta este comando se ejecutaran los comandos que guarda
 * en su composicion en el orden que fueron agregados, lo mismo pasa si se deshace este comando.
 * 
 * @author Galli
 *
 */
public class CommandComposition implements Command {

    private List<Command> commands;

    public CommandComposition() {
        commands = new ArrayList<Command>();
    }

    /**
     * Agrega el comando que se pasa por parametro a la composicion de comandos.
     * 
     * @param command
     *            : comando que se quiere agregar a la composicion.
     * @return a si mismo.
     */
    public CommandComposition add(Command command) {
        commands.add(command);
        return this;
    }

    @Override
    public void execute() throws InvalidCellIdentifierException {
        commands.iterator().forEachRemaining(command -> command.execute());
    }

    @Override
    public void undo() {
        commands.iterator().forEachRemaining(command -> command.undo());
    }

}
