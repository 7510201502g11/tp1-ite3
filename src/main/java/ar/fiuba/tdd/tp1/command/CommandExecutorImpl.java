package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;

/**
 * Se encarga de ejecutar comandos y de deshacer cambios que estos provocaron.
 *
 * @author Galli
 *
 */
public class CommandExecutorImpl implements CommandExecutor {

    private CommandHistory commandHistory;

    public CommandExecutorImpl() {
        this.commandHistory = new CommandHistory();
    }

    @Override
    public void runCommand(Command command) throws InvalidCellIdentifierException {
        this.commandHistory.clearRedoStack();
        command.execute();
        this.commandHistory.pushCommand(command);
    }
 
    @Override
    public void undo() {
        this.commandHistory.undo();
    }

    @Override
    public void redo() {
        this.commandHistory.redo();
    }

}
