package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;

/**
 * Es la interfaz que debe implementar un ejecutor de comandos.
 * 
 * @author Galli
 *
 */
public interface CommandExecutor {

    /**
     * Corre el comando que se pasa por parametro.
     * 
     * @param command
     *            : comando a ejecutar.
     * @throws InvalidCellIdentifierException
     *             : la celda que se quiso modificar es invalida.
     */
    public abstract void runCommand(Command command) throws InvalidCellIdentifierException;

    /**
     * Deshace el ultimo cambio que se hizo y no se deshizo.
     */
    public abstract void undo();

    /**
     * Rehace el ultimo cambio que se deshizo.
     */
    public abstract void redo();

}