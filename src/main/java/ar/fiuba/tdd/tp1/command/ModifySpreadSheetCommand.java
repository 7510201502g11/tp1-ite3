package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;
import ar.fiuba.tdd.tp1.spreadsheet.SpreadSheetUndoRedo;

import java.util.Optional;

/**
 * Esta clase se encarga de pedirle al spreadSheet que hacer o deshace el ultimo cambio. No permite
 * hacer varios deshacer o rehacer ya que estaria modificandose mas cambios de los que puede hacer
 * este comando.
 * 
 * @author Galli
 *
 */
public class ModifySpreadSheetCommand<SpreadSheetTypeT> implements Command {

    private SpreadSheetUndoRedo<SpreadSheetTypeT> spreadSheet;
    private boolean canUndo;

    public ModifySpreadSheetCommand(SpreadSheetUndoRedo<SpreadSheetTypeT> spreadSheet) {
        Optional.of(spreadSheet);
        this.spreadSheet = spreadSheet;
        canUndo = true;
    }

    @Override
    public void execute() throws InvalidCellIdentifierException {
        if (!canUndo) {
            this.spreadSheet.redo();
            canUndo = true;
        }
    }

    @Override
    public void undo() {
        if (canUndo) {
            this.spreadSheet.undo();
            canUndo = false;
        }
    }

}
