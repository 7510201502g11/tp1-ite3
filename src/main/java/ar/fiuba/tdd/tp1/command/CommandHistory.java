package ar.fiuba.tdd.tp1.command;

import java.util.Optional;
import java.util.Stack;

/**
 * Esta clase se encarga de guardar un historial de los comandos que se ejecutaron. Sirve para poder
 * deshacer cambios y rehacerlos.
 *
 *@author Galli
 *
 */
public class CommandHistory {

    private Stack<Command> undoStack;
    private Stack<Command> redoStack;

    public CommandHistory() {
        undoStack = new Stack<Command>();
        redoStack = new Stack<Command>();
    }

    /**
     * Guarda los comandos que ya se corrieron.
     * 
     * @param command
     *            : comando que se ejecuto.
     */
    public void pushCommand(Command command) {
        undoStack.push(command);
    }

    /**
     * Borra todos los elementos de la pila, en caso de estar vacia no hace nada.
     * 
     * @param stackCommand
     *            : pila que se quiere vaciar.
     */
    private void clearStack(Stack<Command> stackCommand) {
        Optional.ofNullable(stackCommand).filter(stack -> !stack.isEmpty())
                .ifPresent(stack -> stack.clear());
    }

    /**
     * Borra los comandos que se pueden rehacer.
     */
    public void clearRedoStack() {
        this.clearStack(this.redoStack);
    }

    /**
     * Borra los comandos que se pueden deshacer.
     */
    public void clearUndoStack() {
        this.clearStack(this.undoStack);
    }

    /**
     * Deshace el ultimo comando que se corrio y no se deshizo.
     */
    public void undo() {
        Optional.ofNullable(undoStack).filter(stack -> !stack.empty()).ifPresent(stack -> {
                Command command = stack.pop();
                command.undo();
                redoStack.push(command);
            });
    }

    /**
     * Rehace el ultimo comando que se deshizo.
     */
    public void redo() {
        Optional.ofNullable(redoStack).filter(stack -> !stack.empty()).ifPresent(stack -> {
                Command command = stack.pop();
                command.execute();
                undoStack.push(command);
            });
    }
}
