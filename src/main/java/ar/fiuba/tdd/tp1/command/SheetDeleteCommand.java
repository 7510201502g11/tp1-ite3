package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;
import ar.fiuba.tdd.tp1.model.book.Book;
import ar.fiuba.tdd.tp1.model.sheet.Sheet;

import java.util.Optional;

/**
 * Esta clase se encarga de eliminar una hoja de un libro y en caso que se desee deshace el cambio
 * recupera la hoja como estaba.
 * 
 * @author Galli
 *
 */
public class SheetDeleteCommand<CellIdentityTypeT, DataTypeT> implements Command {

    private String sheetName;
    private Book<CellIdentityTypeT, DataTypeT> book;
    private Sheet<CellIdentityTypeT, DataTypeT> sheet;

    public SheetDeleteCommand(String name, Book<CellIdentityTypeT, DataTypeT> book) {
        Optional.of(name).of(book);
        sheetName = name;
        this.book = book;
        sheet = null;
    }

    @Override
    public void execute() throws InvalidCellIdentifierException {
        if (book.existsSheet(sheetName)) {
            sheet = book.getSheet(sheetName);
        }
        book.removeSheet(sheetName);
    }

    @Override
    public void undo() {
        Optional.ofNullable(sheet).ifPresent(newSheet -> book.putSheet(newSheet));
    }

}
