package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;
import ar.fiuba.tdd.tp1.model.book.Book;

import java.util.Optional;

/**
 * Esta clase se encarga de renombrar las hojas y en caso de deshacer los cambios le pone el nombre
 * original a la hoja.
 * 
 * @author Galli
 *
 */
public class SheetRenameCommand<CellIdentityTypeT, DataTypeT> implements Command {

    private String newSheetName;
    private String oldSheetName;
    private Book<CellIdentityTypeT, DataTypeT> book;

    public SheetRenameCommand(String oldName, String newName, Book<CellIdentityTypeT, DataTypeT> book) {
        Optional.of(oldName).of(newName).of(book);
        newSheetName = newName;
        oldSheetName = oldName;
        this.book = book;
    }

    @Override
    public void execute() throws InvalidCellIdentifierException {
        book.renameSheet(oldSheetName, newSheetName);
    }

    @Override
    public void undo() {
        if (book.existsSheet(newSheetName)) {
            book.renameSheet(newSheetName, oldSheetName);
        }
    }
}
