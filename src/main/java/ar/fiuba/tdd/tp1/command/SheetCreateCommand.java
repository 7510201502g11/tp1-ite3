package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;
import ar.fiuba.tdd.tp1.model.book.Book;

import java.util.Optional;

/**
 * Esta clase se encarga de crear una hoja en un libro y en caso que se desee deshacer la elimina.
 * 
 * @author Galli
 *
 */
public class SheetCreateCommand<CellIdentityTypeT, DataTypeT> implements Command {

    private String sheetName;
    private Book<CellIdentityTypeT, DataTypeT> book;

    public SheetCreateCommand(String name, Book<CellIdentityTypeT, DataTypeT> book) {
        Optional.of(name).of(book);
        sheetName = name;
        this.book = book;
    }

    @Override
    public void execute() throws InvalidCellIdentifierException {
        book.createSheet(sheetName);
    }

    @Override
    public void undo() {
        book.removeSheet(sheetName);
    }

}
