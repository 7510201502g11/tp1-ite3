package ar.fiuba.tdd.tp1.formatconverter;

public interface FormatConverterFactory {
    
    public FormatConverter create(String type);
    
}