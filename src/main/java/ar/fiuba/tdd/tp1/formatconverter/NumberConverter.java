package ar.fiuba.tdd.tp1.formatconverter;

import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetDriverImpl;
import ar.fiuba.tdd.tp1.model.modelcontainer.FormatterModel;

public class NumberConverter implements FormatConverter {

    @Override
    public void convert(FormatterModel formModel, String cellId, String sheetName,
            SpreadSheetDriverImpl driver, String bookName) {
        
        driver.setCellFormatNumber(bookName, sheetName, cellId, Integer.parseInt(formModel.getValueOfFormatter("Number.Decimal")));
    }

}