package ar.fiuba.tdd.tp1.formatconverter;

import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetDriverImpl;
//import ar.fiuba.tdd.tp1.format.factory.FormatterDateFactory.DateOrder;
import ar.fiuba.tdd.tp1.model.modelcontainer.FormatterModel;

import java.util.HashMap;

public class DateConverter implements FormatConverter {
    
    @Override
    public void convert(FormatterModel formModel, String cellId, String sheetName,
            SpreadSheetDriverImpl driver, String bookName) {
        
        driver.setCellFormatDate(bookName, sheetName, cellId, formModel.getValueOfFormatter("Date.Format"));
    }

}
