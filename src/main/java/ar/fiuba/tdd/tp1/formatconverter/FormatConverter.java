package ar.fiuba.tdd.tp1.formatconverter;

import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetDriverImpl;
import ar.fiuba.tdd.tp1.model.modelcontainer.FormatterModel;

public interface FormatConverter {
    
    public void convert(FormatterModel formModel, String cellId, String sheetName, SpreadSheetDriverImpl driver, String bookName);
    
}
