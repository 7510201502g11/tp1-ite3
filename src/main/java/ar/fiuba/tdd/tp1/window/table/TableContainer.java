package ar.fiuba.tdd.tp1.window.table;

import javax.swing.table.DefaultTableModel;

/**
 * Esta interfaz debe implementarla aquella clase que tenga la informacion para imprimir en la
 * tabla.
 * 
 * @author Galli
 *
 */
public interface TableContainer {
    
    /**
     * Agrega al dtm los datos que deben encontrarse en la tabla.
     * 
     * @param dtm
     *            : modelo de tabla.
     * @return dtm modificado.
     */
    public DefaultTableModel addDataToTableModel(DefaultTableModel dtm);

}
