package ar.fiuba.tdd.tp1.window.table;

import javax.swing.table.DefaultTableModel;

/**
 * Esta clase en realidad es de prueba para ver como se imprimen una tabla.
 * 
 * @author Galli
 *
 */
public class ExampleTableContainerImpl implements TableContainer {

    @Override
    public DefaultTableModel addDataToTableModel(DefaultTableModel dtm) {
        dtm = this.addColumnsToTableModel(dtm);
        Object[] data = new Object[5];
        for (int row = 1; row < 10; row++) {
            data[0] = row;
            for (char column = 'A'; column < 'E'; column++) {
                int col = column - 'A' + 1;
                data[col] = "Celda " + column + ',' + row;
            }
            dtm.addRow(data);
        }
        return dtm;
    }

    /**
     * Agrega los valores de las columnas a la TableModel.
     * 
     * @param dtm
     *            : TableModel que se debe modificar.
     * @return TableModel modificada.
     */
    private DefaultTableModel addColumnsToTableModel(DefaultTableModel dtm) {
        dtm.addColumn("Book.NombreHoja");
        for (char column = 'A'; column < 'E'; column++) {
            dtm.addColumn("" + column);
        }
        return dtm;
    }

}
