package ar.fiuba.tdd.tp1.window.table;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 * Esta clase crea una ventana con la tabla y permite visualizarla.
 * 
 * @author Galli
 *
 */
public class WindowTable extends JFrame { //implements TableModelListener {

    private JTable table;
//    private TableContainer tableContainer;
//    private ControllerSpreadSheet controller;

    public WindowTable(TableContainer tableContainer) { //, ControllerSpreadSheet controller) {
 //       this.controller = controller;
        DefaultTableModel tableModel = new DefaultTableModel();
        table = new JTable(tableModel);
        setModel(tableModel, tableContainer);
        table.setPreferredScrollableViewportSize(new Dimension(500, 70));
        JScrollPane scrollPane = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        getContentPane().add(scrollPane, BorderLayout.CENTER);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent windowEvent) {
                System.exit(0);
            }
        });
    }

    /**
     * Setea un modelo a la tablaModel a partir de los datos del tableContainer.
     * 
     * @param tableModel
     *            : tabla para presentar por pantalla.
     * @param tableContainer
     *            : tabla con los datos que se quiere presentar por pantalla.
     */
    private void setModel(DefaultTableModel tableModel, TableContainer tableContainer) {
  //      this.tableContainer = tableContainer;
     //   tableContainer.addColumnsToTableModel(tableModel);
        tableContainer.addDataToTableModel(tableModel);
        table.setModel(tableModel);
      //  TableModel model = table.getModel();
        //model.addTableModelListener(this);
    }

    /**
     * Modifica la tabla con los datos del tableContainer.
     * 
     * @param tableContainer
     *            : tabla que contiene los datos.
     */
    public void changeModel(TableContainer tableContainer) {
        this.setModel(new DefaultTableModel(), tableContainer);
    }

    /**
     * Abre la ventana con la tabla.
     */
    public void printWindow() {
        pack();
        setVisible(true);
    }

//    @Override
//    public void tableChanged(TableModelEvent event) {
//        DefaultTableModel model = (DefaultTableModel) table.getModel();
//        int row = event.getFirstRow();
//        int column = event.getColumn();
//        String columnName = model.getColumnName(column);
//        Object data = model.getValueAt(row, column);
//        row++; // Porque la primera fila arranca en 0
//        System.out.println(columnName + row + " : " + data.toString());
//        controller.setCellValueInTheCurrentSheetAndBook("" + columnName + row, data.toString());
//    }

}
