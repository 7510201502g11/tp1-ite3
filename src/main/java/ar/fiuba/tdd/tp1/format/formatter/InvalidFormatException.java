package ar.fiuba.tdd.tp1.format.formatter;

public class InvalidFormatException extends RuntimeException {
    
    public InvalidFormatException(String msg) {
        super(msg);
    }

}
