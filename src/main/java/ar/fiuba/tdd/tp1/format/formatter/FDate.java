package ar.fiuba.tdd.tp1.format.formatter;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Es un contenedor de fecha (a�o, mes y dia), se puede instanciar un objeto a partir de un numero o
 * con los datos de la fecha.
 * 
 * @author Galli
 *
 */
public class FDate {

    private LocalDate date;

    public FDate() {
        date = null;
    }

    public FDate(long time) {
        if (time < 0) {
            throw new DateTimeException("El numero a transformar a fecha es negativo.");
        }
        date = LocalDate.ofEpochDay(time);
    }

    public FDate(int year, int month, int day) {
        createDate(year, month, day);
    }

    /**
     * Si el string es una fecha con el formato DD-MM-YYYY entonces se crea un Fdate, en caso
     * contrario devuelve InvalidFormatException.
     * 
     * @param stringDate
     *            : el string de una fecha con formato DD-MM-YYYY.
     */
    public FDate(String stringDate) {
        date = LocalDate.parse(stringDate, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
    }

    /**
     * Crea una fecha a partir del anio, mes y dia.
     * 
     * @param year
     *            : anio de la fecha.
     * @param month
     *            : mes de la fecha.
     * @param day
     *            : dia de la fecha.
     */
    private void createDate(int year, int month, int day) {
        date = LocalDate.of(year, month, day);
    }

    /**
     * Devuelve la fecha pasado a numero.
     * 
     * @return un numero.
     */
    public long date() {
        return date.toEpochDay();
    }

    /**
     * Devuelve el a�o de la fecha.
     * 
     * @return un a�o.
     */
    public int year() {
        return date.getYear();
    }

    /**
     * Devuelve el mes de la fecha.
     * 
     * @return un mes.
     */
    public int month() {
        return date.getMonthValue();
    }

    /**
     * Devuelve el dia de la fecha.
     * 
     * @return un dia.
     */
    public int day() {
        return date.getDayOfMonth();
    }

    /**
     * Devuelve la fecha con un determinado formato.
     * 
     * @param format
     *            : formato de la fecha.
     * @return un string con la fecha.
     */
    public String getFormattedDate(String format) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        return date.format(formatter);
    }

    /**
     * Devuelve si un formato es valido o no.
     * 
     * @param format
     *            : formato a evaluar.
     * @return true si es valido y en caso contrario false.
     */
    public boolean isAValidFormat(String format) {
        try {
            DateTimeFormatter.ofPattern(format);
            return true;
        } catch (IllegalArgumentException exc) {
            return false;
        }
    }
}
