package ar.fiuba.tdd.tp1.format.model.cell;

import ar.fiuba.tdd.tp1.format.formatter.Formatter;

/**
 * Es la interfaz que debe implementar una celda que guarda un formato.
 * 
 * @author Galli
 * 
 */
public interface FormattedCell<IdTypeT> {

    /**
     * Inserta un formato a una celda.
     * 
     * @param formatter
     *            : formato se desea insertar.
     * @return una celda con el formato deseado.
     */
    public FormattedCell<IdTypeT> setFormatter(Formatter formatter);

    /**
     * Devuelve el formato que guarda la celda.
     * 
     * @return Formatter de la celda.
     */
    public Formatter getFormatter();

    /**
     * Devuelve el identificador de la celda.
     * 
     * @return IdTypeT de la celda.
     */
    public IdTypeT getId();

}
