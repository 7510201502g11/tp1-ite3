package ar.fiuba.tdd.tp1.format.formatter;

/**
 * Se encarga de dar el formato mes-dia-a�o a una fecha.
 * 
 * @author Galli
 *
 */
public class DateMmDdYyyy implements TypeFormatterDate {
    
    @Override
    public String format() {
        return "MM-dd-yyyy";
    }

    @Override
    public String getInformation() {
        return "MM-DD-YYYY";
    }

}
