package ar.fiuba.tdd.tp1.format.formatter;

import java.util.Map;

/**
 * Este es el formato String para las celdas del SpreadSheet.
 * 
 * @author Galli
 *
 */
public class FString implements Formatter {

    @Override
    public String format(String value) {
        return value;
    }

    @Override
    public Map<String, String> getInformation() {
        CreateInformation information = new CreateInformation();
        information.addInformation("Type", "String");
        return information.getInformation();
    }

}
