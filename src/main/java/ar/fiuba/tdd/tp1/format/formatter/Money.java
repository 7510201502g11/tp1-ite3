package ar.fiuba.tdd.tp1.format.formatter;

import java.util.Map;
import java.util.Optional;

/**
 * Este es el formato de moneda que permite al usuario elegir el simbolo que quiere para representar
 * la moneda.
 * 
 * @author Galli
 *
 */
public class Money implements Formatter {

    private Formatter otherFormatter;
    private String symbol;
    
    public Money(String symbol, Formatter otherFormatter) {
        Optional.of(symbol).of(otherFormatter);
        if (symbol.equals("")) {
            throw new InvalidFormatException("El simbolo es invalido.");
        }
        this.symbol = symbol;
        this.otherFormatter = otherFormatter;
    }

    @Override
    public String format(String value) {
        String newValue = otherFormatter.format(value);
        if (newValue.equals("")) {
            return "";
        }
        try {
            Double.valueOf(newValue);
            return symbol + " " + newValue;
        } catch (NumberFormatException exc) {
            return "Error:BAD_CURRENCY";
//            throw new InvalidFormatException("El valor: " + value
//                    + " no se le puede poner formato moneda");
        }
    }

    @Override
    public Map<String, String> getInformation() {
        CreateInformation information = new CreateInformation(otherFormatter.getInformation());
        information.addInformation("Type", "Money");
        information.addInformation("Symbol", symbol);
        return information.getInformation();
    }

}
