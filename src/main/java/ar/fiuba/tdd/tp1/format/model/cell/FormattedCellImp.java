package ar.fiuba.tdd.tp1.format.model.cell;

import ar.fiuba.tdd.tp1.format.formatter.Formatter;

import java.util.Optional;

/**
 * Esta clase guarda un formato en una celda.
 * 
 * @author Galli
 *
 */
public class FormattedCellImp<IdTypeT> implements FormattedCell<IdTypeT> {

    private IdTypeT id;
    private Formatter formatter;

    public FormattedCellImp(IdTypeT id) {
        super();
        Optional.of(id);
        this.id = id;
    }

    public FormattedCellImp(Formatter formatter, IdTypeT id) {
        super();
        Optional.of(formatter).of(id);
        this.id = id;
        this.formatter = formatter;
    }

    @Override
    public FormattedCell<IdTypeT> setFormatter(Formatter formatter) {
        Optional.of(formatter);
        this.formatter = formatter;
        return this;
    }

    @Override
    public Formatter getFormatter() {
        return formatter;
    }

    @Override
    public IdTypeT getId() {
        return id;
    }

}
