package ar.fiuba.tdd.tp1.format.formatter;

/**
 * Se encarga de dar el formato dia-mes-a�o a una fecha.
 * 
 * @author Galli
 *
 */
public class DateDdMmYyyy implements TypeFormatterDate {
    
    @Override
    public String format() {
        return "dd-MM-yyyy";
    }

    @Override
    public String getInformation() {
        return "DD-MM-YYYY";
    }

}
