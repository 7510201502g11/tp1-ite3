package ar.fiuba.tdd.tp1.format.formatter;

/**
 * Se encarga de dar el formato a�o-mes-dia a una fecha.
 * 
 * @author Galli
 *
 */
public class DateYyyyMmDd implements TypeFormatterDate {

    @Override
    public String getInformation() {
        return "YYYY-MM-DD";
    }

    @Override
    public String format() {
        return "yyyy-MM-dd";
    }

}
