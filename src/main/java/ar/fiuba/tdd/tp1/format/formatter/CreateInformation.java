package ar.fiuba.tdd.tp1.format.formatter;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Arma un mapa con los atributos y los datos que se le envie.
 * 
 * @author Galli
 *
 */
public class CreateInformation {

    private Map<String, String> information;

    public CreateInformation() {
        information = new HashMap<String, String>();
    }

    public CreateInformation(Map<String, String> information) {
        this.information = information;
    }

    /**
     * Agrega el titulo del atributo y el dato a la informacion.
     * 
     * @param title
     *            : titulo del atributo.
     * @param date
     *            : dato del atributo.
     */
    public void addInformation(String title, String date) {
        information.put(title, date);
    }

    /**
     * Devuelve el dato de un atributo de la informacion seteada.
     * 
     * @param title
     *            : titulo del atributo.
     * @return un string con el dato del atributo.
     */
    public String getInformation(String title) {
        return Optional.ofNullable(information.get(title)).orElse("");
    }

    /**
     * Devuelve un mapa con la informacion de los atributos y sus datos seteados.
     * 
     * @return un mapa.
     */
    public Map<String, String> getInformation() {
        return information;
    }
}
