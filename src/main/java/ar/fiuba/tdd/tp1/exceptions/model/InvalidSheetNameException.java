package ar.fiuba.tdd.tp1.exceptions.model;

public class InvalidSheetNameException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public InvalidSheetNameException(String msg) {
        super(msg);
    }

}
