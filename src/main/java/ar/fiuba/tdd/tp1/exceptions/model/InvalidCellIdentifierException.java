package ar.fiuba.tdd.tp1.exceptions.model;

public class InvalidCellIdentifierException extends RuntimeException {
    public InvalidCellIdentifierException(String string) {
        super(string);
    }

    public InvalidCellIdentifierException() {
        super();
    }

    private static final long serialVersionUID = 1L;

}
