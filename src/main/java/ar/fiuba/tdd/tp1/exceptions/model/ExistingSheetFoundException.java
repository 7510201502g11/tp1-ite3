package ar.fiuba.tdd.tp1.exceptions.model;

public class ExistingSheetFoundException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public ExistingSheetFoundException(String msg) {
        super(msg);
    }
}
