package ar.fiuba.tdd.tp1.exceptions.fileio;

public class ErrorParsingJsonFileException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public ErrorParsingJsonFileException(String msg) {
        super(msg);
    }
}
