package ar.fiuba.tdd.tp1.exceptions.fileio;

public class InvalidDirectoryNameException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public InvalidDirectoryNameException(String msg) {
        super(msg);
    }
}
