package ar.fiuba.tdd.tp1.exceptions.fileio;

public class FileCreationException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public FileCreationException(String msg) {
        super(msg);
    }
}
