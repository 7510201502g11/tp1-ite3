package ar.fiuba.tdd.tp1.spreadsheet;

/**
 * Es la interfaz que debe implementar una SpreadSheet que pueda deshacer o rehacer cambios.
 * 
 * @author Galli
 *
 */
public interface SpreadSheetUndoRedo<SpreadSheetTypeT> {

    /**
     * Deshace el ultimo cambio que se hizo y no se deshizo.
     */
    public SpreadSheetTypeT undo();

    /**
     * Rehace el ultimo cambio que se deshizo.
     */
    public SpreadSheetTypeT redo();

}
