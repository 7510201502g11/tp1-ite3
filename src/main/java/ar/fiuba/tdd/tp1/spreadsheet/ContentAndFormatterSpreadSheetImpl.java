package ar.fiuba.tdd.tp1.spreadsheet;

import ar.fiuba.tdd.tp1.command.CommandComposition;
import ar.fiuba.tdd.tp1.command.CommandExecutor;
import ar.fiuba.tdd.tp1.command.CommandExecutorImpl;
import ar.fiuba.tdd.tp1.command.ModifySpreadSheetCommand;
import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;
import ar.fiuba.tdd.tp1.format.formatter.FDate;
import ar.fiuba.tdd.tp1.format.formatter.FNumber;
import ar.fiuba.tdd.tp1.format.formatter.FString;
import ar.fiuba.tdd.tp1.format.formatter.Formatter;
import ar.fiuba.tdd.tp1.format.formatter.FormatterDate;
import ar.fiuba.tdd.tp1.format.formatter.InvalidFormatException;
import ar.fiuba.tdd.tp1.format.formatter.Money;
import ar.fiuba.tdd.tp1.model.cell.Cell;
import ar.fiuba.tdd.tp1.model.cell.CellImpl;
import ar.fiuba.tdd.tp1.model.cell.CrPair;

import java.time.DateTimeException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Es la interfaz de un SpreadSheet que guarda los valores y los formatos de un celda. Los valores
 * deben ser del tipo String y los formatos deben ser Formatters.
 * 
 * @author Galli
 *
 */
public class ContentAndFormatterSpreadSheetImpl implements ContentAndFormatterSpreadSheet {

    private ContentSpreadSheet contentSpreadSheet;
    private FormatterSpreadSheet formatterSpreadSheet;
    private CommandExecutor commandExecutor;

    public ContentAndFormatterSpreadSheetImpl() {
        contentSpreadSheet = new ContentSpreadSheetImpl();
        formatterSpreadSheet = new FormatterSpreadSheet();
        commandExecutor = new CommandExecutorImpl();
    }

    /**
     * Intenta convertir el valor pasado por parametro a fecha.
     * 
     * @param rawValue
     *            : es el valor que se quiere convertir a fecha.
     * @return si es una fecha devuelve el numero que representa esa fecha y en caso contrario
     *         devuelve el valor pasado por parametro.
     */
    private String convertValue(String rawValue) {
        try {
            FDate fdate = new FDate(rawValue);
            return "" + fdate.date();
        } catch (Exception exc) {
            return rawValue;
        }
    }

    @Override
    public ContentAndFormatterSpreadSheet putCellRawValue(String sheet, String col, Integer row,
            String rawValue) throws InvalidCellIdentifierException {
        String newValue = convertValue(rawValue);
        contentSpreadSheet.putCell(sheet, col, row, newValue);
        if (!rawValue.equals(newValue)) {
            formatterSpreadSheet.putCell(sheet, col, row, new FormatterDate("dd-MM-yyyy"));
            createCommandCompositionWithContentAndFormatterSpreadSheetAndExecute();
        } else {
            commandExecutor.runCommand(new ModifySpreadSheetCommand<>(contentSpreadSheet));
        }
        return this;
    }

    /**
     * Pone un formato en un celda de una hoja.
     * 
     * @param sheet
     *            : nombre de la hoja.
     * @param col
     *            : nombre de la columna.
     * @param row
     *            : numero de fila.
     * @param formatter
     *            : formato a insertar.
     * @return ContentAndFormatterSpreadSheet.
     */
    private ContentAndFormatterSpreadSheet putCellFormat(String sheet, String col, Integer row,
            Formatter formatter) {
        formatterSpreadSheet.putCell(sheet, col, row, formatter);
        commandExecutor.runCommand(new ModifySpreadSheetCommand<>(formatterSpreadSheet));
        return this;
    }

    /**
     * Devuelve el formato de una celda de una hoja.
     * 
     * @param sheet
     *            : nombre de la hoja.
     * @param col
     *            : nombre de la columna.
     * @param row
     *            : numero de fila.
     * @return el formato buscado.
     */
    private Formatter getCellFormat(String sheet, String col, Integer row) {
        return formatterSpreadSheet.getCellRawValue(sheet, col, row);
    }

    @Override
    public ContentAndFormatterSpreadSheet putCellFormatString(String sheet, String col, Integer row)
            throws InvalidCellIdentifierException {
        return putCellFormat(sheet, col, row, new FString());
    }

    @Override
    public ContentAndFormatterSpreadSheet putCellFormatNumber(String sheet, String col,
            Integer row, int decimal) throws InvalidCellIdentifierException {
        Formatter otherFormatter = this.getCellFormat(sheet, col, row);
        return putCellFormat(sheet, col, row, new FNumber(decimal, otherFormatter));
    }

    @Override
    public ContentAndFormatterSpreadSheet putCellFormatMoney(String sheet, String col, Integer row,
            String symbol) throws InvalidCellIdentifierException {
        Formatter otherFormatter = this.getCellFormat(sheet, col, row);
        return putCellFormat(sheet, col, row, new Money(symbol, otherFormatter));
    }

    @Override
    public ContentAndFormatterSpreadSheet putCellFormatDate(String sheet, String col, Integer row,
            String dateOrder) throws InvalidCellIdentifierException {
        return putCellFormat(sheet, col, row, new FormatterDate(dateOrder));
    }

    @Override
    public String getCellRawValue(String sheet, String col, Integer row)
            throws InvalidCellIdentifierException {
        return contentSpreadSheet.getCellRawValue(sheet, col, row);
    }

    @Override
    public String getCellEvaluatedValue(String sheet, String col, Integer row) {
        String cell = contentSpreadSheet.getCellEvaluatedValue(sheet, col, row);
        if (cell.equals("Formula Invalida")) {
            return "Error:BAD_FORMULA";
        }
        return cell;
    }

    @Override
    public String getCellFormattedValue(String sheet, String col, Integer row) {
        Formatter formatter = formatterSpreadSheet.getCellRawValue(sheet, col, row);
        String cell = getCellEvaluatedValue(sheet, col, row);
        if (cell.equals("Error:BAD_FORMULA")) {
            return cell;
        }
//        try {
        return (formatter.format(cell));
//        } catch (DateTimeException excep) {
//            return "Error:INVALID_FORMAT";
//        } catch (InvalidFormatException exception) {
//            return "Error:INVALID_FORMAT";
//        }
    }

    @Override
    public ContentAndFormatterSpreadSheet createSheet(String name) {
        contentSpreadSheet.createSheet(name);
        formatterSpreadSheet.createSheet(name);
        createCommandCompositionWithContentAndFormatterSpreadSheetAndExecute();
        return this;
    }

    @Override
    public ContentAndFormatterSpreadSheet removeSheet(String name) {
        contentSpreadSheet.removeSheet(name);
        formatterSpreadSheet.removeSheet(name);
        createCommandCompositionWithContentAndFormatterSpreadSheetAndExecute();
        return this;
    }

    @Override
    public ContentAndFormatterSpreadSheet renameSheet(String oldName, String newName) {
        contentSpreadSheet.renameSheet(oldName, newName);
        formatterSpreadSheet.renameSheet(oldName, newName);
        createCommandCompositionWithContentAndFormatterSpreadSheetAndExecute();
        return this;
    }

    @Override
    public boolean existsSheet(String name) {
        return contentSpreadSheet.existsSheet(name);
    }

    @Override
    public ContentAndFormatterSpreadSheet undo() {
        commandExecutor.undo();
        return this;
    }

    @Override
    public ContentAndFormatterSpreadSheet redo() {
        commandExecutor.redo();
        return this;
    }

    @Override
    public List<String> getNamesSheet() {
        return contentSpreadSheet.getNamesSheet();
    }

    /**
     * Crea un comando que modifica el SpreadSheet del contenedor de Strings y del contenedor de
     * formatos.
     */
    private void createCommandCompositionWithContentAndFormatterSpreadSheetAndExecute() {
        CommandComposition command = new CommandComposition();
        command.add(new ModifySpreadSheetCommand<>(contentSpreadSheet)).add(
                new ModifySpreadSheetCommand<>(formatterSpreadSheet));
        commandExecutor.runCommand(command);
    }

    @Override
    public HashMap<String, Map<CrPair, Cell<CrPair, String>>> getCellsInBook() {
        return contentSpreadSheet.getCellsInBook();
    }

    /**
     * Devuelve un mapa nuevo.
     * 
     * @return mapa.
     */
    private HashMap<String, Map<CrPair, Map<String, String>>> createMapStringCrPairStringString() {
        return new HashMap<String, Map<CrPair, Map<String, String>>>();
    }

    @Override
    public HashMap<String, Map<CrPair, Cell<CrPair, String>>> getCellsFormattedInBook() {
        HashMap<String, Map<CrPair, Cell<CrPair, String>>> map = getCellsInBook();
        HashMap<String, Map<CrPair, Cell<CrPair, String>>> newMap = new HashMap<String, Map<CrPair, Cell<CrPair, String>>>();

        for (final Iterator<Entry<String, Map<CrPair, Cell<CrPair, String>>>> iter = map.entrySet()
                .iterator(); iter.hasNext();) {
            Entry<String, Map<CrPair, Cell<CrPair, String>>> entry = iter.next();
            final String sheet = entry.getKey();
            final Map<CrPair, Cell<CrPair, String>> contenedor = entry.getValue();
            Map<CrPair, Cell<CrPair, String>> newContenedor = new HashMap<CrPair, Cell<CrPair, String>>();
            for (final Iterator<Entry<CrPair, Cell<CrPair, String>>> iter2 = contenedor.entrySet()
                    .iterator(); iter2.hasNext();) {
                Entry<CrPair, Cell<CrPair, String>> entry2 = iter2.next();
                final CrPair id = entry2.getKey();
                newContenedor.put(
                        id,
                        new CellImpl<CrPair, String>(id, this.getCellFormattedValue(sheet,
                                id.getColumn(), id.getRow())));
            }
            newMap.put(sheet, newContenedor);
        }
        return newMap;
    }

    @SuppressWarnings("CPD-START")
    @Override
    public HashMap<String, Map<CrPair, Map<String, String>>> getFormatterInBook() {
        HashMap<String, Map<CrPair, Cell<CrPair, String>>> map = getCellsInBook();
        HashMap<String, Map<CrPair, Map<String, String>>> mapFormatter = createMapStringCrPairStringString();
        map.forEach((sheet, contenedor) -> {
                Map<CrPair, Map<String, String>> cellformatter = new HashMap<CrPair, Map<String, String>>();
                contenedor.forEach((id, cell) -> {
                        cellformatter.put(
                                id,
                                formatterSpreadSheet.getFormatInformation(sheet, id.getColumn(),
                                        id.getRow()));
                    });
                mapFormatter.put(sheet, cellformatter);
            });
        return mapFormatter;
    }
    
    @SuppressWarnings("CPD-END")

    @Override
    public void putCellFormatter(String sheet, String col, int row, String formatter, String format) {
        if (formatter.equals("decimal")) {
            this.putCellFormatNumber(sheet, col, row, Integer.parseInt(format));
        } else {
            if (formatter.equals("format")) {
                this.putCellFormatDate(sheet, col, row, format);
            } else {
                if (formatter.equals("symbol")) {
                    this.putCellFormatMoney(sheet, col, row, format);
                } else {
                    throw new InvalidFormatException("El formato " + formatter
                            + " no es un formato valido.");
                }
            }
        }
    }

}
