package ar.fiuba.tdd.tp1.spreadsheet;

import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;
import ar.fiuba.tdd.tp1.model.cell.Cell;
import ar.fiuba.tdd.tp1.model.cell.CrPair;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Es la interfaz de un SpreadSheet que guarda los valores y los formatos de un celda.
 * 
 * @author Galli
 *
 */
public interface ContentAndFormatterSpreadSheet extends
        SpreadSheetUndoRedo<ContentAndFormatterSpreadSheet> {

    /**
     * Cambia el valor de una celda.
     * 
     * @param sheet
     *            : pagina que se quiere modificar.
     * @param col
     *            : columna que se quiere modificar.
     * @param row
     *            : fila que se quiere modificar.
     * @param rawValue
     *            : nuevo valor de la celda.
     * @throws InvalidCellIdentifierException
     *             : la celda que se quiso modificar es invalida.
     */
    public ContentAndFormatterSpreadSheet putCellRawValue(String sheet, String col, Integer row,
            String rawValue) throws InvalidCellIdentifierException;

    /**
     * Cambia el formato de una celda a String.
     * 
     * @param sheet
     *            : pagina que se quiere modificar.
     * @param col
     *            : columna que se quiere modificar.
     * @param row
     *            : fila que se quiere modificar.
     * @param rawValue
     *            : nuevo valor de la celda.
     * @throws InvalidCellIdentifierException
     *             : la celda que se quiso modificar es invalida.
     */
    public ContentAndFormatterSpreadSheet putCellFormatString(String sheet, String col, Integer row)
            throws InvalidCellIdentifierException;

    /**
     * Cambia el formato de una celda a Numero.
     * 
     * @param sheet
     *            : pagina que se quiere modificar.
     * @param col
     *            : columna que se quiere modificar.
     * @param row
     *            : fila que se quiere modificar.
     * @param rawValue
     *            : nuevo valor de la celda.
     * @throws InvalidCellIdentifierException
     *             : la celda que se quiso modificar es invalida.
     */
    public ContentAndFormatterSpreadSheet putCellFormatNumber(String sheet, String col,
            Integer row, int decimal) throws InvalidCellIdentifierException;

    /**
     * Cambia el formato de una celda a Moneda.
     * 
     * @param sheet
     *            : pagina que se quiere modificar.
     * @param col
     *            : columna que se quiere modificar.
     * @param row
     *            : fila que se quiere modificar.
     * @param rawValue
     *            : nuevo valor de la celda.
     * @throws InvalidCellIdentifierException
     *             : la celda que se quiso modificar es invalida.
     */
    public ContentAndFormatterSpreadSheet putCellFormatMoney(String sheet, String col, Integer row,
            String symbol) throws InvalidCellIdentifierException;

    /**
     * Cambia el formato de una celda a Fecha.
     * 
     * @param sheet
     *            : pagina que se quiere modificar.
     * @param col
     *            : columna que se quiere modificar.
     * @param row
     *            : fila que se quiere modificar.
     * @param rawValue
     *            : nuevo valor de la celda.
     * @throws InvalidCellIdentifierException
     *             : la celda que se quiso modificar es invalida.
     */
    public ContentAndFormatterSpreadSheet putCellFormatDate(String sheet, String col, Integer row,
            String dateOrder) throws InvalidCellIdentifierException;

    /**
     * Obtiene el valor crudo de la celda que se busca.
     * 
     * @param sheet
     *            : pagina que se busca.
     * @param col
     *            : columna que se busca.
     * @param row
     *            : fila que se busca.
     * @return el valor crudo, sin procesar, que guarda la celda.
     * @throws InvalidCellIdentifierException
     *             : los parametros de las celdas son invalidos.
     */
    public String getCellRawValue(String sheet, String col, Integer row)
            throws InvalidCellIdentifierException;

    /**
     * Obtiene el valor de la celda que se busca.
     * 
     * @param sheet
     *            : pagina que se busca.
     * @param col
     *            : columna que se busca.
     * @param row
     *            : fila que se busca.
     * @return el valor que guarda la celda.
     * @throws InvalidCellIdentifierException
     *             : los parametros de las celdas son invalidos.
     */
    public String getCellEvaluatedValue(String sheet, String col, Integer row)
            throws InvalidCellIdentifierException;

    /**
     * Obtiene el valor de la celda formateado.
     * 
     * @param sheet
     *            : pagina que se busca.
     * @param col
     *            : columna que se busca.
     * @param row
     *            : fila que se busca.
     * @return el valor que guarda la celda con el formato que se le habia asignado.
     * @throws InvalidCellIdentifierException
     *             : los parametros de las celdas son invalidos.
     */
    public String getCellFormattedValue(String sheet, String col, Integer row)
            throws InvalidCellIdentifierException;

    /**
     * Crea una hoja con el nombre que se pasa por parametro.
     * 
     * @param name
     *            : nombre de la hoja.
     */
    public ContentAndFormatterSpreadSheet createSheet(String name);

    /**
     * Borra la hoja del libro que tiene el nombre que se pasa por parametro.
     * 
     * @param name
     *            : nombre de la hoja a eliminar.
     */
    public ContentAndFormatterSpreadSheet removeSheet(String name);

    /**
     * Cambia el nombre de la hoja oldName por newName.
     * 
     * @param oldName
     *            : nombre de la hoja la cual se quiere modificar.
     * @param newName
     *            : nuevo nombre para la hoja.
     */
    public ContentAndFormatterSpreadSheet renameSheet(String oldName, String newName);

    /**
     * Revisa si existe la hoja con el nombre pasado por parametro.
     * 
     * @param name
     *            : nombre de la hoja que se busca.
     * @return true si existe y false en caso contrario.
     */
    public boolean existsSheet(String name);

    /**
     * Devuelve los nombres de las hojas.
     * 
     * @return lista con los nombres de las hojas.
     */
    public List<String> getNamesSheet();

    /**
     * Devuelve un mapa con los valores crudos de un libro.
     * 
     * @param bookName
     *            : nombre del libro que se busca.
     * @return un mapa.
     */
    public HashMap<String, Map<CrPair, Cell<CrPair, String>>> getCellsInBook();

    /**
     * Devuelve un mapa con las celdas formateadas de un libro.
     * 
     * @param bookName
     *            : nombre del libro que se busca.
     * @return un mapa.
     */
    public HashMap<String, Map<CrPair, Cell<CrPair, String>>> getCellsFormattedInBook();

    /**
     * Devuelve un mapa con la informacion de los formatos de cada celda de un libro.
     * 
     * @param bookName
     *            : nombre del libro que se busca.
     * @return un mapa.
     */
    public HashMap<String, Map<CrPair, Map<String, String>>> getFormatterInBook();

    /**
     * Pone un formato especifico a una celda de un hoja de un libro determinado.
     * 
     * @param workBookName
     *            : nombre del libro.
     * @param workSheetName
     *            : nombre de la hoja.
     * @param cellId
     *            : posicion de la celda (columna+fila).
     * @param formatter
     *            : el tipo de formato.
     * @param format
     *            : caracteristicas del formato.
     */
    public void putCellFormatter(String sheet, String col, int row, String formatter, String format);

}
