package ar.fiuba.tdd.tp1.spreadsheet;

import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;

/**
 * Esta interfaz va a servir para que el parser pueda pedir los valores de las celdas que necesite.
 * 
 * @author Galli
 *
 */
public interface CellVarIdAccessor<DataTypeT> {

    /**
     * Devuelve el valor qu e guarda la celda con el identificador pasado por parametro.
     * 
     * @param cellId
     *            : es el identificador de la celda.
     * @return devuelve el contenido de la celda.
     * @throws InvalidCellIdentifierException
     *             : la celda que se quiere obtener es invallida.
     */
    public DataTypeT getCellRawValue(String cellId) throws InvalidCellIdentifierException;

}
