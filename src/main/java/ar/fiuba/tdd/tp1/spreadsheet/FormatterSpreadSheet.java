package ar.fiuba.tdd.tp1.spreadsheet;

import ar.fiuba.tdd.tp1.format.formatter.FString;
import ar.fiuba.tdd.tp1.format.formatter.Formatter;

import java.util.Map;
import java.util.Optional;

/**
 * Es el SpreadSheet que guarda formatos.
 * 
 * @author Galli
 *
 */
public class FormatterSpreadSheet extends SpreadSheetImpl<Formatter> {

    @Override
    protected Formatter returnRawValue(Formatter rawValue) {
        return Optional.ofNullable(rawValue).orElse(new FString());
    }

    /**
     * Devuelve un mapa con los atributos y datos relevantes de un formato de una celda.
     * 
     * @param sheet
     *            : nombre de la hoja.
     * @param col
     *            : nombre de la columna de la celda.
     * @param row
     *            : numero de la fila de la celda.
     * @return un mapa con la informacion del formato.
     */
    public Map<String, String> getFormatInformation(String sheet, String col, Integer row) {
        return this.getCellRawValue(sheet, col, row).getInformation();
    }

}
