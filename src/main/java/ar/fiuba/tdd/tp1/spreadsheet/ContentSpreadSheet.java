package ar.fiuba.tdd.tp1.spreadsheet;

/**
 * Es la interfaz grafica que debe implementar un SpreadSheet que guarda en sus celdas Strings.
 * 
 * @author Galli
 *
 */
public interface ContentSpreadSheet extends SpreadSheet<String> {

    /**
     * Obtiene el valor de la celda que se busca.
     * 
     * @param sheet
     *            : pagina que se busca.
     * @param col
     *            : columna que se busca.
     * @param row
     *            : fila que se busca.
     * @return el valor que guarda la celda.
     * @throws InvalidCellIdentifierException
     *             : los parametros de las celdas son invalidos.
     */
    public String getCellEvaluatedValue(String sheet, String col, Integer row);

}
