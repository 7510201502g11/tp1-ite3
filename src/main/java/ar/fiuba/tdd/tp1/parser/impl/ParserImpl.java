package ar.fiuba.tdd.tp1.parser.impl;

import ar.fiuba.tdd.tp1.parser.Parser;
import ar.fiuba.tdd.tp1.parser.ParserException;
import ar.fiuba.tdd.tp1.parser.TokenInterpreter;
import ar.fiuba.tdd.tp1.parser.expression.Expression;
import ar.fiuba.tdd.tp1.parser.expression.impl.ConstantExpression;
import ar.fiuba.tdd.tp1.parser.expression.impl.ExponentiationExpression;
import ar.fiuba.tdd.tp1.parser.expression.impl.SequenceOperationExpression;
import ar.fiuba.tdd.tp1.parser.expression.impl.VariableExpression;
import ar.fiuba.tdd.tp1.parser.token.Token;
import ar.fiuba.tdd.tp1.parser.token.TokenConsumer;
import ar.fiuba.tdd.tp1.parser.token.TokenType;
import ar.fiuba.tdd.tp1.parser.token.Tokenizer;
import ar.fiuba.tdd.tp1.util.CompareUtils;
import ar.fiuba.tdd.tp1.util.DefaultValueMap;
import ar.fiuba.tdd.tp1.util.VarRef;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/**
 * Implementacion por defecto del parser. Entiende expresiones aritmeticas y declaracion de
 * variables.
 * 
 * @author martin
 *
 */
public class ParserImpl implements Parser {
    protected static final TokenType TOKEN_TYPE = TokenType.get();
    private Tokenizer tokenizer;
    private Set<String> variables = new HashSet<String>();
    private Expression endExpression;
    protected String defaultPointer = "";

    protected TokenConsumer tokenConsumer;

    protected DefaultValueMap<Integer, TokenInterpreter> argumentTokenInterpreters = new DefaultValueMap<>();
    protected DefaultValueMap<Integer, TokenInterpreter> valueTokenInterpreters = new DefaultValueMap<>();

    public String getDefaultPointer() {
        return defaultPointer;
    }

    protected TokenConsumer getTokenConsumer() {
        return tokenConsumer;
    }

    public Parser setDefaultPointer(String defaultPointer) {
        this.defaultPointer = defaultPointer;
        return this;
    }

    @Override
    public Expression getEndExpression() {
        return endExpression;
    }

    @Override
    public Set<String> getReferencedVariables() {
        return variables;
    }

    public ParserImpl(Tokenizer tokenizer) {
        super();
        this.tokenizer = tokenizer;
        this.buildArgumentTokenInterpreters();
        this.buildValueTokenInterpreters();
    }

    protected void buildTokenConsumer(String toParse) {
        Token epsilon = new Token(TOKEN_TYPE.epsilon(), "", -1);
        tokenConsumer = tokenizer.tokenize(toParse).getTokenConsumer();
        tokenConsumer.setDefault(epsilon);
    }

    protected void resetReferencedVariables() {
        this.variables = new HashSet<>();
    }

    protected void print(String str) {
        System.out.println("Parser::" + str);
    }

    @Override
    public Expression parse(String toParse) {
        resetReferencedVariables();

        buildTokenConsumer(toParse);

        print("begin: " + tokenConsumer.getCurrent());

        //         verifico si el primer token es invalido, en caso afirmativo el resto del parseo no tiene sentido.
        checkForInvalidTokens();

        if (getCurrentTokenType().equals(TOKEN_TYPE.equals())) {
            nextToken();
        }

        this.endExpression = expression();

        return endExpression;
    }

    protected void checkForInvalidTokens() {
        //        Integer currentTokenType = getCurrentTokenType();
        //        if (CompareUtils.equalsOr(currentTokenType, TOKEN_TYPE.misc(), TOKEN_TYPE.epsilon())) {
        //            throw new ParserException("Token inicial es invalido!");
        //        }

        if (tokenConsumer.containsType(TOKEN_TYPE.equals())
                && tokenConsumer.containsType(TOKEN_TYPE.misc())
                && !tokenConsumer.containsType(TOKEN_TYPE.pairFunction())) {
            throw new ParserException(
                    "Tokens parseados contienen EQUALS y MISC pero no contienen FUNCION PAR!");
        }
    }

    protected void nextToken() {
        print(tokenConsumer.getCurrentTokenSequence() + " CONSUMED!");
        tokenConsumer.next();
        print("nextToken: " + tokenConsumer.getCurrent());
    }

    protected Expression expression() {
        print("expression -> signed_term sum_op");

        // expression -> signed_term sum_op
        Expression signedTerm = signed(this::term);
        return sumOp(signedTerm);
    }

    protected Expression sumOp(Expression expr) {
        // si el siguiente token es de tipo TERMINAL , entonces se debe
        // consumir el token y continuar con la secuencia

        if (tokenConsumer.getCurrentTokenType() == TOKEN_TYPE.plusminus()) {
            print("sum_op -> PLUSMINUS term sum_op");
            // sum_op -> PLUSMINUS term sum_op

            SequenceOperationExpression sum = new SequenceOperationExpression(expr);

            //            String strOperation = tokenConsumer.getCurrentTokenSequence();
            //            nextToken();
            String strOperation = this.getSequenceAndDiscardToken();
            Expression term = term();
            sum.addOperand(term, strOperation);

            return sumOp(sum);
        }

        print("sum_op -> EPSILON");
        // sum_op -> EPSILON
        return expr;
    }

    protected Expression term() {
        print("term -> factor term_op");
        // term -> factor term_op
        Expression factor = factor();
        return termOp(factor);
    }

    protected String getSequenceAndDiscardToken() {
        String strOperation = tokenConsumer.getCurrentTokenSequence();
        nextToken();
        return strOperation;
    }

    protected Expression termOp(Expression expression) {
        if (tokenConsumer.getCurrentTokenType() == TOKEN_TYPE.multdiv()) {
            print("term_op -> MULTDIV signed_factor term_op");

            SequenceOperationExpression prod = new SequenceOperationExpression(expression);

            //            String strOperation = tokenConsumer.getCurrentTokenSequence();
            //            nextToken();
            String strOperation = this.getSequenceAndDiscardToken();
            Expression signeFactor = signed(this::factor);
            prod.addOperand(signeFactor, strOperation);

            return termOp(prod);

        }

        print("term_op -> EPSILON");
        // term_op -> EPSILON
        return expression;
    }

    protected Expression factor() {
        print("factor -> argument factor_op");
        // factor -> argument factor_op
        Expression arg = argument();
        return factorOp(arg);
    }

    protected Expression factorOp(Expression expression) {
        if (tokenConsumer.getCurrentTokenType() == TOKEN_TYPE.raised()) {
            print("factor_op -> RAISED expression");
            // factor_op -> RAISED expression
            nextToken();
            Expression exponent = signed(this::factor);
            return new ExponentiationExpression(expression, exponent);
        }
        print("factor_op -> EPSILON");
        // factor_op -> EPSILON
        return expression;
    }

    protected void buildArgumentTokenInterpreters() {
        this.argumentTokenInterpreters = new ArgumentTokenInterpreterBuilder(this).build();
    }

    protected void buildValueTokenInterpreters() {
        this.valueTokenInterpreters = new ValueTokenInterpreterBuilder(this).build();
    }

    protected Expression argument() {
        Integer currentTokenType = tokenConsumer.getCurrentTokenType();
        TokenInterpreter tokenInterpreter = this.argumentTokenInterpreters.get(currentTokenType);
        return tokenInterpreter.create(tokenConsumer);
    }

    protected Expression value() {
        TokenInterpreter tokenInterpreter = this.valueTokenInterpreters.get(getCurrentTokenType());
//        tokenInterpreter = Optional.ofNullable(tokenInterpreter).orElseThrow(
//                () -> new ParserException("Simbolo encontrado inesperado "
//                        + tokenConsumer.getCurrent() + " found"));
        return tokenInterpreter.create(tokenConsumer);
    }

    protected void addReferencedVariable(String variableReference) {
        this.variables.add(variableReference);
    }

    protected void addReferencedVariables(Collection<String> variables) {
        this.variables.addAll(variables);
    }

    @FunctionalInterface
    public interface ParserTerm {
        public Expression get();
    }

    protected Expression signed(ParserTerm parserTerm) {

        if (tokenConsumer.getCurrentTokenType() == TOKEN_TYPE.plusminus()) {
            print("signedTerm -> PLUSMINUS term");
            // signedTerm -> PLUSMINUS term
            boolean positive = tokenConsumer.getCurrentTokenSequence().equals("+");
            nextToken();
            Expression expr = parserTerm.get();
            return new SequenceOperationExpression(expr, positive);
        }

        print("signedTerm -> term");
        // signedTerm -> term
        return parserTerm.get();
    }

    protected Integer getCurrentTokenType() {
        return tokenConsumer.getCurrentTokenType();
    }
}
