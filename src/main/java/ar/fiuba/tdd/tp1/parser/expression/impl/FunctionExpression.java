package ar.fiuba.tdd.tp1.parser.expression.impl;

import ar.fiuba.tdd.tp1.parser.expression.ArithmeticOperation;
import ar.fiuba.tdd.tp1.parser.expression.Context;
import ar.fiuba.tdd.tp1.parser.expression.EvaluationException;
import ar.fiuba.tdd.tp1.parser.expression.Expression;

import java.util.HashMap;
import java.util.Map;

/**
 * Expresion de tipo funcion aritmetica (sin cos tan, etc.).
 * 
 * @author martin.zaragoza
 *
 */
public class FunctionExpression implements Expression {
    private Expression argument;
    private String operationId;
    private Map<String, ArithmeticOperation> operations = new HashMap<String, ArithmeticOperation>();

    /**
     * Agrega una operacion que la expresion deba poder interpretar.
     * 
     * @param opId
     *            - id de la operacion (ej: sin).
     * @param operation
     *            - Aritmetica matematica que se debe invocar al solicitar la operacion 'opId' (ej:
     *            Math::sin).
     * @return this.
     */
    public FunctionExpression addOperation(String opId, ArithmeticOperation operation) {
        this.operations.put(opId.toLowerCase(), operation);
        return this;
    }

    /**
     * Establece argumento de la funcion.
     * 
     * @param argument
     *            - Expresion argumento de la funcion a ejecutar.
     * @return this.
     */
    public FunctionExpression setArgument(Expression argument) {
        this.argument = argument;
        return this;
    }

    /**
     * Establece la operacion aritmetica a invocar.
     * 
     * @param operationId
     *            - id de operacion aritmetica a invocar.
     * @return this.
     */
    public FunctionExpression setOperationId(String operationId) {
        this.operationId = operationId;
        return this;
    }

    /**
     * Construye una instancia estableciendo de inmediato funcion aritmetica y argumento a aplicar.
     * 
     * @param operationId
     *            - id de operacion aritmetica a invocar.
     * @param argument
     *            - Expresion argumento de la funcion a ejecutar.
     */
    public FunctionExpression(String operationId, Expression argument) {
        super();
        this.operationId = operationId;
        this.argument = argument;
    }

    public Double getValueAsDouble(Context context) {
        try {
            return this.operations.get(operationId).calculate(argument.getValueAsDouble(context));
        } catch (Exception e) {
            throw new EvaluationException("Invalid function id " + operationId + "!");
        }
    }

    public FunctionExpression() {
        super();
    }

    /**
     * Agrega un conjunto de operaciones aritmeticas que la instancia debe entender.
     * 
     * @param ops
     *            - operaciones aritmeticas que la instancia debe entender.
     * @return this.
     */
    public FunctionExpression addOperations(Map<String, ArithmeticOperation> ops) {
        this.operations.putAll(ops);
        return this;
    }

}
