package ar.fiuba.tdd.tp1.parser.expression;

import ar.fiuba.tdd.tp1.parser.expression.impl.FunctionExpression;

/**
 * Representa un constructor de expresiones de tipo funcion aritmetica.
 * 
 * @author martin.zaragoza
 *
 */
public interface FunctionExpressionBuilder {
    public FunctionExpression build();
}
