package ar.fiuba.tdd.tp1.parser.expression.impl.pair;

import ar.fiuba.tdd.tp1.parser.expression.Context;
import ar.fiuba.tdd.tp1.parser.expression.Expression;

import java.util.ArrayList;
import java.util.List;

public class RightExpression extends SubstringExpression {

    @Override
    public List<Integer> getIndexes(Context context, int range, String stringToCut) {
        List<Integer> indexes = new ArrayList<>();
        indexes.add(Math.max(stringToCut.length() - range, 0));
        indexes.add(stringToCut.length());

        return indexes;
    }

    @Override
    public StringFunctionExpression getExpression() {
        return new RightExpression();
    }
}
