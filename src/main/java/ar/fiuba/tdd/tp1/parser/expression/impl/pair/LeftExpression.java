package ar.fiuba.tdd.tp1.parser.expression.impl.pair;

import ar.fiuba.tdd.tp1.parser.expression.Context;

import java.util.ArrayList;
import java.util.List;

public class LeftExpression extends SubstringExpression {

    @Override
    public StringFunctionExpression getExpression() {
        return new LeftExpression();
    }

    @Override
    public List<Integer> getIndexes(Context context, int range, String stringToCut) {
        List<Integer> indexes = new ArrayList<>();
        indexes.add(0);
        indexes.add(Math.min(range, stringToCut.length()));

        return indexes;
    }
}
