package ar.fiuba.tdd.tp1.parser.expression;

/**
 * Representa una operacion aritmetica que transforma un valor en otro.
 * 
 * @author martin
 *
 */
@FunctionalInterface
public interface ArithmeticOperation {
    double calculate(double val);
}
