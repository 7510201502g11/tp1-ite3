package ar.fiuba.tdd.tp1.parser.expression.impl.pair;

import ar.fiuba.tdd.tp1.parser.expression.Context;
import ar.fiuba.tdd.tp1.parser.expression.Expression;

import java.util.List;

public abstract class SubstringExpression extends StringFunctionExpression {

    @Override
    public String getValueAsString(Context context) {
        List<Expression> expressions = this.getExpressions();
        String result = expressions.get(0).getValueAsString(context);
        List<Integer> indexes = this.getIndexes(context, (int)Math.round(expressions.get(1).getValueAsDouble(context)), result);

        return result.substring(indexes.get(0), indexes.get(1));
    }

    public abstract List<Integer> getIndexes(Context context, int range, String stringToCut);
}
