package ar.fiuba.tdd.tp1.parser.expression.impl.pair;

import ar.fiuba.tdd.tp1.parser.expression.Context;
import ar.fiuba.tdd.tp1.parser.expression.Expression;

import java.util.List;

public class PrintFunctionExpression extends StringFunctionExpression {

    @Override
    public StringFunctionExpression getExpression() {
        return new PrintFunctionExpression();
    }

    @Override
    public String getValueAsString(Context context) {

        List<Expression> expressions = this.getExpressions();
        String result = expressions.get(0).getValueAsString(context);
        expressions.remove(0);

        for (Expression expression : expressions) {
            result = result.replaceFirst("\\$\\d+", expression.getValueAsString(context));
        }

        return result;
    }
}
