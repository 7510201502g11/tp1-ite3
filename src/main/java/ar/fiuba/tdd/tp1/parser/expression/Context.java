package ar.fiuba.tdd.tp1.parser.expression;

import java.util.Collection;
import java.util.Set;

/**
 * Representa informacion que permite a una expresion resolver incertidumbres.
 * 
 * @author martin.zaragoza
 *
 */
public interface Context {
    /**
     * Guarda un valor.
     * 
     * @param key
     *            - Clave para recuperar el valor mas tarde.
     * @param value
     *            - Valor a guardar.
     * @return this.
     */
    public Context put(String key, Object value);

    /**
     * Recupera un valor guardado. Null si la clave no corresponde a ningun valor.
     * 
     * @param key
     *            - clave de valor a recuperar.
     * @return valor guardado. Null si la clave no corresponde a ningun valor.
     */
    public Object get(String key);

    /**
     * Retorna todas las claves guardadas.
     * 
     * @return - Claves asignadas.
     */
    Set<String> keys();

    /**
     * Retorna todos los valores guardados.
     * 
     * @return todos los valores guardados.
     */
    Collection<Object> values();
}
