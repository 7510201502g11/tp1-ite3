package ar.fiuba.tdd.tp1.parser.expression.impl;

import ar.fiuba.tdd.tp1.parser.expression.ArithmeticOperation;
import ar.fiuba.tdd.tp1.parser.expression.FunctionExpressionBuilder;

import java.util.HashMap;
import java.util.Map;

/**
 * Constructor de funciones aritmeticas estandar. Construye expresiones de funciones aritmeticas que
 * entenderan: sin cos tan asin acos atan sqrt exp ln log2.
 * 
 * @author martin.zaragoza
 *
 */
public class StandardArithmeticFunctionExpressionBuilder implements FunctionExpressionBuilder {
    private static Map<String, ArithmeticOperation> operations;

    static {
        operations = new HashMap<String, ArithmeticOperation>() { {
                put("sin", Math::sin);
                put("cos", Math::cos);
                put("tan", Math::tan);
                put("asin", Math::asin);
                put("acos", Math::acos);
                put("atan", Math::atan);
                put("sqrt", Math::sqrt);
                put("exp", Math::exp);
                put("ln", Math::log);
                put("log", val -> {
                        return Math.log(val) * 0.43429448190325182765;
                    });
                put("log2", val -> {
                        return Math.log(val) * 1.442695040888963407360;
                    });
            } };
    }

    @Override
    public FunctionExpression build() {
        FunctionExpression functionExpressionNode = new FunctionExpression();
        functionExpressionNode.addOperations(operations);
        return functionExpressionNode;
    }

    public static StandardArithmeticFunctionExpressionBuilder newInstance() {
        return new StandardArithmeticFunctionExpressionBuilder();
    }
}
