package ar.fiuba.tdd.tp1.parser.expression.impl;

import ar.fiuba.tdd.tp1.parser.expression.Context;
import ar.fiuba.tdd.tp1.parser.expression.ContextUtils;
import ar.fiuba.tdd.tp1.parser.expression.Expression;
import ar.fiuba.tdd.tp1.parser.var.RangeSequence;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class RangeOperationExpression implements Expression {
    private RangeExpression rangeExpression;
    private String strOperation = "";
    protected static final Map<String, RangeOperation> RANGE_OPERATIONS = new HashMap<>();

    static {
        RANGE_OPERATIONS.put("MAX", (rangeSequence, ctx) -> {
                List<Double> ctxValues = rangeSequenceAsDoubleList(rangeSequence, ctx);
                return ctxValues.stream().mapToDouble(val -> val).max().getAsDouble();
            });

        RANGE_OPERATIONS.put("AVERAGE", (rangeSequence, ctx) -> {
                List<Double> ctxValues = rangeSequenceAsDoubleList(rangeSequence, ctx);
                return ctxValues.stream().mapToDouble(val -> val).average().getAsDouble();
            });

        RANGE_OPERATIONS.put("MIN", (rangeSequence, ctx) -> {
                List<Double> ctxValues = rangeSequenceAsDoubleList(rangeSequence, ctx);
                return ctxValues.stream().mapToDouble(val -> val).min().getAsDouble();
            });
    }

    private static List<Double> rangeSequenceAsDoubleList(RangeSequence rangeSequence, Context ctx) {
        List<Double> res = new ArrayList<>();
        rangeSequence.asList().forEach(
                rangeElem -> res.add(ContextUtils.getValueAsDouble(ctx, rangeElem)));
        return res;
    }

    public RangeOperationExpression(RangeExpression expr, String strRangeFunction) {
        this.rangeExpression = Optional.of(expr).get();
        this.strOperation = Optional.of(strRangeFunction).get().trim().toUpperCase();
    }

    @Override
    public Double getValueAsDouble(Context context) {
        RangeOperation rangeOperation = RANGE_OPERATIONS.get(this.strOperation);
        RangeSequence rangeSequence = rangeExpression.buildRangeSequence();
        return rangeOperation.calculate(rangeSequence, context);
    }

    @FunctionalInterface
    private static interface RangeOperation {
        public double calculate(RangeSequence rangeSequence, Context ctx);
    }

    public String toString() {
        return strOperation + " " + rangeExpression.toString();
    }
}
