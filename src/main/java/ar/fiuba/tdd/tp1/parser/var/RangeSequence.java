package ar.fiuba.tdd.tp1.parser.var;

import ar.fiuba.tdd.tp1.util.StringUtils;
import ar.fiuba.tdd.tp1.util.VarRef;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Representa una secuencia de elementos (por ejemplo celdas) definidas a partir
 * de un rango.
 * 
 * @author martin
 *
 */
public class RangeSequence {
    private PointerColumnRow topLeft;
    private PointerColumnRow bottomRight;
    private PointerColumnRow current;
    private ColumnSequenceFactory sequenceFactory;
    private String defaultPointer = "";
    private String range;

    @SuppressWarnings("serial")
    public static class NullRangeException extends RuntimeException {
        public NullRangeException() {
            super("Imposible asignar rango nulo o vacio a RangeSequence");
        }
    }

    /**
     * Crea una instancia de rango de celdas a partir de dos Strings que
     * representan la esquina superior izquierda y la inferior derecha
     * respectivamente.
     * 
     * @param topLeft
     *            - Celda superior izquierda.
     * @param bottomRight
     *            - Celda inferior derecha.
     */
    public RangeSequence(String topLeft, String bottomRight) {
        this(VarRef.get().rangeBuilder().build(topLeft, bottomRight), new NestedColumnSequenceFactory());
    }

    /**
     * Crea una secuencia de elementos a partir de una definicion de rango como
     * String.
     * 
     * @param range
     *            - Rango a partir del cual se debe obtener la secuencia.
     * @param columnSequenceFactory
     *            - Generadora de secuencias de columnas [OPCIONAL].
     */
    public RangeSequence(String range, ColumnSequenceFactory columnSequenceFactory) {
        super();
        this.sequenceFactory = Optional.ofNullable(columnSequenceFactory).orElse(new NestedColumnSequenceFactory());
        range = Optional.ofNullable(range).filter(StringUtils::notEmpty).orElseThrow(NullRangeException::new);
        RangePair rangePair = RangePair.create(range);
        this.topLeft = PointerColumnRow.createFromString(rangePair.left.toString(), columnSequenceFactory);
        this.bottomRight = PointerColumnRow.createFromString(rangePair.right.toString());

        this.current = topLeft.copy();

        this.range = range;
    }

    /**
     * Crea un generador de secuencias de elementos a partir de un rango usando
     * un secuenciador Anidado {@link NestedColumnSequenceFactory}.
     * 
     * @param range
     *            - Rango a partir del cual definir la secuencia con formato
     *            {@link VarRef#relativeRangeRegex()} o
     *            {@link VarRef#absoluteRangeRegex()}
     */
    public RangeSequence(String range) {
        this(range, new NestedColumnSequenceFactory());
    }

    /**
     * Retorna true si la secuencia ha llegado a su fin.
     * 
     * @return true si la secuencia ha llegado a su fin.
     */
    public boolean ended() {
        return this.current.equals(bottomRight);
    }

    public RangeSequence setDefaultPointer(String defaultPointer) {
        this.defaultPointer = Optional.of(defaultPointer).get();
        return this;
    }

    /**
     * Retorna el elemento actual como string.
     * 
     * @return elemento actual como string.
     */
    public String getCurrentAsString() {
        return getCurrentCopy().toString();
    }

    /**
     * Retorna una copia del elemento corriente.
     * 
     * @return copia del elemento corriente.
     */
    public PointerColumnRow getCurrentCopy() {
        PointerColumnRow copy = current.copy();
        Optional.of(copy).filter(curr -> curr.getPointer().isEmpty()).ifPresent(curr -> curr.setPointer(defaultPointer));
        return copy;
    }

    /**
     * Mueve el cursor al siguiente elemento de la secuencia.
     * 
     * @return this.
     */
    public RangeSequence next() {
        if (this.ended()) {
            return this;
        }

        String maxCol = bottomRight.getColumn();
        String initCol = topLeft.getColumn();
        String currPointer = current.getPointer();

        if (current.getColumn().equals(maxCol)) {
            Integer newRow = current.augmentRow().getRow();
            this.current = new PointerColumnRow(currPointer, initCol, newRow, sequenceFactory);
        } else {
            this.current.augmentColumn();
        }

        return this;
    }

    /**
     * Crea una copia de la secuencia. La copia se encontrara reiniciada, es
     * decir que tiene los mismos puntos de inicio y fin que la original pero el
     * elemento corriente es el inicial.
     * 
     * @return copia de la secuencia. La copia se encontrara reiniciada, es
     *         decir que tiene los mismos puntos de inicio y fin que la original
     *         pero el elemento corriente es el inicial.
     */
    public RangeSequence copy() {
        return new RangeSequence(range, sequenceFactory).setDefaultPointer(defaultPointer);
    }

    /**
     * Retorna una lista con todas los nombres de variables referenciadas por el
     * rango.
     * 
     * @return lista con todas los nombres de variables referenciadas por el
     *         rango.
     */
    public List<String> asList() {
        List<String> res = new ArrayList<>();
        RangeSequence copy = this.copy();

        while (copy.ended() == false) {
            res.add(copy.getCurrentAsString());
            copy.next();
        }
        res.add(copy.getCurrentAsString());

        return res;
    }

    public String toString() {
        return this.asList().toString();
    }
}
