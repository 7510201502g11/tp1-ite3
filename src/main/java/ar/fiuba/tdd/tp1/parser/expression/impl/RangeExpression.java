package ar.fiuba.tdd.tp1.parser.expression.impl;

import ar.fiuba.tdd.tp1.parser.expression.Context;
import ar.fiuba.tdd.tp1.parser.expression.Expression;
import ar.fiuba.tdd.tp1.parser.var.RangeSequence;
import ar.fiuba.tdd.tp1.util.StringUtils;
import ar.fiuba.tdd.tp1.util.VarRef;
import ar.fiuba.tdd.tp1.util.VarRef.VarMatcher;

import java.util.Optional;

public class RangeExpression implements Expression {
    protected String range;
    protected String defaultPointer = "";

    @SuppressWarnings("serial")
    public static class NullDefaultPointerException extends RuntimeException {
        public NullDefaultPointerException() {
            super("Imposible asignar defaultPointer nulo a RangeExpression");
        }
    }

    @SuppressWarnings("serial")
    public static class NullRangeException extends RuntimeException {
        public NullRangeException() {
            super("Imposible asignar rango nulo o vacio a RangeExpression");
        }
    }

    public RangeExpression(String strRange, String defaultPointer) {
        this.range = Optional.ofNullable(strRange).filter(StringUtils::notEmpty)
                .orElseThrow(NullRangeException::new);
        this.defaultPointer = Optional.ofNullable(defaultPointer).orElseThrow(
                NullDefaultPointerException::new);

        VarRef variableReference = VarRef.get();
        VarMatcher absoluteRangeMatcher = variableReference.absoluteRangeMatcher();
        VarMatcher relativeRangeMatcher = variableReference.relativeRangeMatcher();

        absoluteRangeMatcher.matches(range);
        relativeRangeMatcher.matches(range);

        Optional.of(strRange)
                .filter(ran -> absoluteRangeMatcher.matches(ran)
                        || relativeRangeMatcher.matches(ran))
                .orElseThrow(
                        () -> new InvalidRangeException("Formato de rango " + strRange
                                + " invalido!"));

    }

    public RangeExpression(String strRange) {
        this(strRange, "");
    }

    @Override
    public Double getValueAsDouble(Context context) {
        throw new UnsupportedOperationException("No es posible obtener el valor numerico de un rango!");
    }
    
    @Override
    public String getValueAsString(Context context) {
        return range;
    }

    public RangeSequence buildRangeSequence() {
        return new RangeSequence(range).setDefaultPointer(defaultPointer);
    }

    public String toString() {
        return this.range;
    }
}
