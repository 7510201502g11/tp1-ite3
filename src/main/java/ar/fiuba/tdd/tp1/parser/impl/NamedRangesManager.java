package ar.fiuba.tdd.tp1.parser.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class NamedRangesManager {

    private List<NamedRange> mappedRanges;

    private static NamedRangesManager instance = null;

    protected NamedRangesManager() {

        mappedRanges = new ArrayList<NamedRange>();
    }

    public static NamedRangesManager getInstance() {

        if (instance == null) {
            instance = new NamedRangesManager();
        }

        return instance;
    }

    public void addNamedRange(String rangeName, String range) {

        mappedRanges.add(new NamedRange(rangeName, range));
    }

    public String replaceNamedRanges(String cellRawValue) {
        cellRawValue = Optional.ofNullable(cellRawValue).orElse("");
        for (NamedRange namedRange : mappedRanges) {
            cellRawValue = cellRawValue.replaceAll("\\b" + namedRange.getRangeName() + "\\b", namedRange.getRange());
        }

        return cellRawValue;
    }

}
