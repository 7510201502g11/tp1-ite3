package ar.fiuba.tdd.tp1.parser.token.impl;

import ar.fiuba.tdd.tp1.parser.token.Token;
import ar.fiuba.tdd.tp1.parser.token.TokenConsumer;
import ar.fiuba.tdd.tp1.parser.token.TokenInfo;
import ar.fiuba.tdd.tp1.parser.token.Tokenizer;
import ar.fiuba.tdd.tp1.parser.token.TokenizerException;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;

/**
 * Lee un string y lo separa en componentes fundamentales o tokens a partir de expresiones regulares
 * cargadas a la instancia.
 */
public class TokenizerImpl implements Tokenizer {
    private LinkedList<TokenInfo> tokenInfos;
    private LinkedList<Token> tokens;

    public TokenizerImpl() {
        super();
        tokenInfos = new LinkedList<TokenInfo>();
        tokens = new LinkedList<Token>();
    }

    @Override
    public void add(String stringRegex, int tokenType) {
        tokenInfos.add(new TokenInfo(stringRegex, tokenType));
    }

    @Override
    public void stack(String stringRegex, int tokenType) {
        tokenInfos.addFirst(new TokenInfo(stringRegex, tokenType));
    }

    /**
     * Filtra el string inicial aplicando algun filtro.
     * 
     * @param str
     *            - String a filtrar.
     * @return String con filtro aplicado.
     */
    protected String filterString(String str) {
        return str;
    }

    @Override
    public Tokenizer tokenize(String str) {
        str = filterString(str);
        doTokenize(str);
        return this;
    }

    //FIXME : matcher.group() se come los espacios...
    /**
     * Accion de tokenizacion de un String.
     * 
     * @param str
     *            - Linea a tokenizar.
     */
    protected void doTokenize(String str) {
        String tempString = str.trim();
        int totalLength = tempString.length();
        tokens.clear();
        while (!tempString.equals("")) {
            int remaining = tempString.length();
            boolean match = false;

            for (TokenInfo info : tokenInfos) {
                Matcher matcher = info.regex.matcher(tempString);
                if (matcher.find()) {
                    match = true;
//                    String tok = matcher.group().trim();
                    String tok = matcher.group();

                    tempString = matcher.replaceFirst("").trim();
                    tokens.add(new Token(info.token, tok, totalLength - remaining));
                    break;
                }
            }
            if (!match) {
                throw new TokenizerException("No es posible TOKENIZAR: " + str
                        + " Caracteres inesperados encontrados especificamente en segmento: "
                        + tempString);
            }
        }
    }

    @Override
    public List<Token> getTokens() {
        return tokens;
    }

    @Override
    public TokenConsumer getTokenConsumer() {
        return new TokenListConsumer(tokens);
    }
}
