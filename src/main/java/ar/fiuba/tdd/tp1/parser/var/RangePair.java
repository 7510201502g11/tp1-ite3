package ar.fiuba.tdd.tp1.parser.var;

import ar.fiuba.tdd.tp1.util.VarRef;

/**
 * Par de variables que representan un rango (ej default!A1:default!C3 , A1:C3).
 * 
 * @author martin
 *
 */
public class RangePair {
    public final PointerVariablePair left;
    public final PointerVariablePair right;

    public RangePair(PointerVariablePair left, PointerVariablePair right) {
        super();
        this.left = left;
        this.right = right;
    }

    public static RangePair create(String range) {
        String[] split = range.split(VarRef.RANGE_SEP);
        return new RangePair(PointerVariablePair.create(split[0]), PointerVariablePair.create(split[1]));
    }

    public String toString() {
        return left.toString() + VarRef.RANGE_SEP + right.toString();
    }
}
