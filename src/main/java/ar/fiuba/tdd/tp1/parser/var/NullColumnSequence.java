package ar.fiuba.tdd.tp1.parser.var;

public class NullColumnSequence implements ColumnSequence {
    @Override
    public boolean didReset(boolean clearReset) {
        return false;
    }

    @Override
    public String getCurrent() {
        return "";
    }
}
