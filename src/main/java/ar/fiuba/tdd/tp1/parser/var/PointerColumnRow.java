package ar.fiuba.tdd.tp1.parser.var;

import ar.fiuba.tdd.tp1.util.VarRef;

import java.util.Objects;
import java.util.Optional;

/**
 * Representa una tripla PUNTERO COLUMNA FILA incrementable.
 * 
 * @author martin
 *
 */
public class PointerColumnRow {
    private String pointer = "";
    private Integer row = 0;
    private ColumnSequenceFactory sequenceFactory = new NestedColumnSequenceFactory();
    private ColumnSequence columnSequence;

    public String getColumn() {
        return columnSequence.getCurrent();
    }

    public Integer getRow() {
        return row;
    }

    public String getPointer() {
        return pointer;
    }

    public PointerColumnRow setPointer(String pointer) {
        this.pointer = pointer;
        return this;
    }

    public PointerColumnRow(String pointer, String column, Integer row) {
        this(pointer, column, row, null);
    }

    public PointerColumnRow(String pointer, String column, Integer row, ColumnSequenceFactory sequenceFactory) {
        super();
        this.pointer = Optional.ofNullable(pointer).orElse("");
        this.row = row = Optional.of(row).get();
        column = Optional.of(column).get();
        this.sequenceFactory = Optional.ofNullable(sequenceFactory).orElse(this.sequenceFactory);
        this.columnSequence = this.sequenceFactory.create(column);
    }

    public PointerColumnRow(PointerColumnRow copy) {
        this(copy.pointer, copy.getColumn(), copy.row, copy.sequenceFactory);
    }

    public PointerColumnRow copy() {
        return new PointerColumnRow(this);
    }

    public PointerColumnRow augmentRow() {
        this.row++;
        return this;
    }

    public PointerColumnRow augmentColumn() {
        this.columnSequence.next();
        return this;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof PointerColumnRow) {
            PointerColumnRow that = (PointerColumnRow) obj;
            return Objects.equals(this.row, that.row) && Objects.equals(this.getColumn(), that.getColumn())
                    && Objects.equals(this.getPointer(), that.getPointer());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(row, getColumn());
    }

    /**
     * Crea una instancia a partir de una expresion de celda. [ej: C23 ,
     * hoja1!A2]
     * 
     * @param str
     *            - String a partir del cual crear la instancia.
     * @param sequenceFactory
     *            - Secuencia de columnas a seguir.
     * @return Nueva instancia.
     */
    public static PointerColumnRow createFromString(String str, ColumnSequenceFactory sequenceFactory) {
        PointerVariablePair pointerVar = PointerVariablePair.create(str);
        String[] split = pointerVar.relativeVariable.split(VarRef.get().colRowSplitRegex());
        return new PointerColumnRow(pointerVar.pointer, split[0], Integer.valueOf(split[1]), sequenceFactory);
    }

    /**
     * Crea una instancia a partir de una expresion de celda. [ej: C23 ,
     * hoja1!A2]. Utiliza un secuenciador de columnas por defecto.
     * 
     * @param str
     *            - String a partir del cual crear la instancia.
     * @return Nueva instancia.
     */
    public static PointerColumnRow createFromString(String str) {
        return createFromString(str, null);
    }

    public String toString() {
        return new PointerVariablePair(pointer, getColumn() + getRow()).toString();
    }

    public ColumnSequenceFactory getSequenceFactory() {
        return sequenceFactory;
    }
}
