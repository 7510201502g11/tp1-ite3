package ar.fiuba.tdd.tp1.parser.impl;

import ar.fiuba.tdd.tp1.parser.token.Tokenizer;

public class RangePairParser extends ParserImpl {
    public RangePairParser(Tokenizer tokenizer) {
        super(tokenizer);
    }

    @Override
    protected void buildArgumentTokenInterpreters() {
        this.argumentTokenInterpreters = new RangePairArgumentTokenInterpreterBuilder(this).build();
    }

}
