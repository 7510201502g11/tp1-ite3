package ar.fiuba.tdd.tp1.parser.expression;

/**
 * Representa una expresion matematica a la cual se le puede solicitar
 * resolverse como valor numerico a partir de un contexto.
 * 
 * @author martin.zaragoza
 *
 */
public interface Expression {
    /**
     * Resolver expresion como valor numerico.
     * 
     * @param context
     *            - Contexto que la expresion usara para resolver incertidumbres
     *            (por ejemplo variables).
     * @return Valor numerico resuelto.
     */
    Double getValueAsDouble(Context context);

    /**
     * Resolver expresion como String.
     * 
     * @param context
     *            - Contexto que la expresion usara para resolver incertidumbres
     *            (por ejemplo variables).
     * @return Valor resuelto como String.
     */
    default String getValueAsString(Context context) {
        return getValueAsDouble(context).toString();
    }
}
