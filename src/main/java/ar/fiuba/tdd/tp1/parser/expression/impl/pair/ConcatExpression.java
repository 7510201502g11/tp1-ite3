package ar.fiuba.tdd.tp1.parser.expression.impl.pair;

import ar.fiuba.tdd.tp1.parser.expression.Context;
import ar.fiuba.tdd.tp1.parser.expression.Expression;
import ar.fiuba.tdd.tp1.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class ConcatExpression extends StringFunctionExpression {

    @Override
    public StringFunctionExpression getExpression() {
        return new ConcatExpression();
    }

    @Override
    public String getValueAsString(Context context) {
        List<String> stringValues = new ArrayList<>();
//        this.getExpressions().forEach(expr -> stringValues.add(expr.getValueAsString(context)));
        List<Expression> expressions = this.getExpressions();
        for (Expression expression : expressions) {
            stringValues.add( expression.getValueAsString(context) );
        }
        return StringUtils.joinAllAsString(stringValues);
    }

}
