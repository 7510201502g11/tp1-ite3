package ar.fiuba.tdd.tp1.parser.impl;

import ar.fiuba.tdd.tp1.parser.Parser;
import ar.fiuba.tdd.tp1.parser.TokenInterpreter;
import ar.fiuba.tdd.tp1.parser.expression.Expression;
import ar.fiuba.tdd.tp1.parser.token.TokenConsumer;
import ar.fiuba.tdd.tp1.parser.token.TokenType;
import ar.fiuba.tdd.tp1.util.DefaultValueMap;

import java.util.List;

public abstract class TokenInterpreterBuilder {
    protected static final TokenType tokType = TokenType.get();
    protected Parser parser;
    protected DefaultValueMap<Integer, TokenInterpreter> argumentTokenInterpreters = new DefaultValueMap<>();

    public TokenInterpreterBuilder(Parser parser) {
        this.parser = parser;
    }

    protected ParserImpl parserImpl() {
        return (ParserImpl) parser;
    }

    protected Expression value() {
        return parserImpl().value();
    }

    protected void print(String string) {
        parserImpl().print(string);
    }

    protected Expression expression() {
        return parserImpl().expression();
    }

    protected void nextToken() {
        parserImpl().nextToken();
    }

    protected Expression argument() {
        return parserImpl().argument();
    }

    protected String getSequenceAndDiscardToken() {
        return parserImpl().getSequenceAndDiscardToken();
    }

    protected Object getCurrentTokenType() {
        return parserImpl().getCurrentTokenType();
    }
    
    protected Object getCurrentTokenSequence() {
        return getTokenConsumer().getCurrentTokenSequence();
    }

    protected void addReferencedVariables(List<String> asList) {
        parserImpl().addReferencedVariables(asList);
    }

    protected TokenConsumer getTokenConsumer() {
        return parserImpl().getTokenConsumer();
    }

    protected String getDefaultPointer() {
        return parserImpl().getDefaultPointer();
    }

    protected void addReferencedVariable(String strVar) {
        parserImpl().addReferencedVariable(strVar);
    }

    public abstract DefaultValueMap<Integer, TokenInterpreter> build();
}
