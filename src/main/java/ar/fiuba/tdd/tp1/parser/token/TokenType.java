package ar.fiuba.tdd.tp1.parser.token;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Encapsula distintos tipos de tokens. Puede extenderse para soportar nuevos tipos.
 * 
 * @author martin.zaragoza
 *
 */
public class TokenType {
    public static final Integer NULL_TYPE = -1;

    private static TokenType tokenType = new TokenType();
    private Map<String, Integer> tokenTypesMap = new HashMap<>();

    public static TokenType get() {
        return tokenType;
    }

    void addTokenType(String tokenTypeName) {
        Integer tokenTypeCode = this.tokenTypesMap.size();
        this.tokenTypesMap.put(tokenTypeName, tokenTypeCode);
    }

    Integer getTokenType(String tokenTypeName) {
        return this.tokenTypesMap.get(tokenTypeName);
    }

    TokenType() {
        String[] strTypes = { "EPSILON", "PLUSMINUS", "MULTDIV", "RAISED", "FUNCTION",
                "OPEN_BRACKET", "CLOSE_BRACKET", "NUMBER", "VARIABLE" };
        Arrays.asList(strTypes).forEach(this::addTokenType);

        addTokenType("RANGE_TERM");
        addTokenType("RANGE_FUNCTION");
        addTokenType("COMMA");
        addTokenType("PAIR_FUNCTION");
        addTokenType("MISC");
        addTokenType("EQUALS");

    }

    public Integer equals() {
        return getTokenType("EQUALS");
    }

    public Integer epsilon() {
        return getTokenType("EPSILON");
    }

    public int plusminus() {
        return getTokenType("PLUSMINUS");
    }

    public int multdiv() {
        return getTokenType("MULTDIV");
    }

    public int raised() {
        return getTokenType("RAISED");
    }

    public int function() {
        return getTokenType("FUNCTION");
    }

    public int openBracket() {
        return getTokenType("OPEN_BRACKET");
    }

    public int closeBracket() {
        return getTokenType("CLOSE_BRACKET");
    }

    public int number() {
        return getTokenType("NUMBER");
    }

    public int variable() {
        return getTokenType("VARIABLE");
    }

    public Integer misc() {
        return getTokenType("MISC");
    }

    public Integer rangeTerm() {
        return getTokenType("RANGE_TERM");
    }

    public Integer rangeFunction() {
        return getTokenType("RANGE_FUNCTION");
    }

    public Integer comma() {
        return getTokenType("COMMA");
    }

    public Integer pairFunction() {
        return getTokenType("PAIR_FUNCTION");
    }

}
