package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.parser.expression.Expression;

import java.util.Set;

public interface Parser {

    /**
     * Retorna la Expresion creada a partir del parseo del string.
     * 
     * @return Expresion creada a partir del parseo del string.
     */
    public abstract Expression getEndExpression();

    /**
     * Obtiene las variables que aparecen en la expresion parseada.
     * 
     * @return - Lista de variables referenciadas por la expresion.
     * */
    public abstract Set<String> getReferencedVariables();

    /**
     * Parsea una expresion.
     * 
     * @param toParse
     *            - Expresion a parsear.
     * @return Expression traducida.
     */
    public abstract Expression parse(String toParse);

    /**
     * Establece el puntero por defecto al que referencian las variables.
     * 
     * @param defaultPointer
     *            - puntero por defecto al que referencian las variables.
     */
    public Parser setDefaultPointer(String defaultPointer);

}