package ar.fiuba.tdd.tp1.parser.var;

@FunctionalInterface
public interface ColumnSequenceFactory {
    public ColumnSequence create(String beginColumn);
}
