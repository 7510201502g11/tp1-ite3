package ar.fiuba.tdd.tp1.parser.token.impl;

import ar.fiuba.tdd.tp1.parser.token.Token;
import ar.fiuba.tdd.tp1.parser.token.TokenConsumer;

import java.util.LinkedList;
import java.util.Optional;

/**
 * Representa un consumidor de tokens que trabaja a partir de una {@link LinkedList}.
 * 
 * @author martin
 *
 */
public class TokenListConsumer implements TokenConsumer {
    private final LinkedList<Token> tokenList;
    private Token defToken;

    /**
     * Construye un consumidor de tokens a partir de una lista enlazada la cual es clonada.
     * 
     * @param tokenList
     *            - Lista enlazada a partir de la cual se construira el consumidor de tokens.
     */
    @SuppressWarnings("unchecked")
    public TokenListConsumer(LinkedList<Token> tokenList) {
        super();
        this.tokenList = (LinkedList<Token>) tokenList.clone();
    }

    @Override
    public Token getCurrent() {
        return this.empty() ? getDefault() : tokenList.getFirst();
    }

    @Override
    public TokenConsumer next() {
        Optional.of(tokenList).filter(tokLst -> tokLst.isEmpty() == false)
                .ifPresent(tokLst -> tokLst.pop());
        return this;
    }

    @Override
    public boolean empty() {
        return tokenList.isEmpty();
    }

    @Override
    public void add(Token token) {
        tokenList.add(token);
    }

    @Override
    public TokenConsumer setDefault(Token token) {
        this.defToken = token;
        return this;
    }

    @Override
    public Token getDefault() {
        return this.defToken;
    }

    public String toString() {
        return tokenList.toString();
    }

    @Override
    public boolean containsType(Integer tokenType) {
        for (Token token : tokenList) {
            if (token.typeEquals(tokenType)) {
                return true;
            }
        }

        return false;
    }
}
