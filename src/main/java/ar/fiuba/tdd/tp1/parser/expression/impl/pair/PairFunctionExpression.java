package ar.fiuba.tdd.tp1.parser.expression.impl.pair;

import ar.fiuba.tdd.tp1.parser.expression.Expression;

import java.util.ArrayList;
import java.util.List;

public abstract class PairFunctionExpression implements Expression {
    private List<Expression> expressions = new ArrayList<>();

    public PairFunctionExpression add(Expression expr) {
        expressions.add(expr);
        return this;
    }

    public List<Expression> getExpressions() {
        return expressions;
    }

    public abstract PairFunctionExpression copy();
}
