package ar.fiuba.tdd.tp1.parser.expression.impl.pair;

import ar.fiuba.tdd.tp1.parser.expression.Context;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by fedevm on 07/11/15.
 */
public class TodayExpression extends StringFunctionExpression {

    @Override
    public StringFunctionExpression getExpression() {
        return new TodayExpression();
    }

    @Override
    public String getValueAsString(Context context) {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();
        return dateFormat.format(date);
    }
}
