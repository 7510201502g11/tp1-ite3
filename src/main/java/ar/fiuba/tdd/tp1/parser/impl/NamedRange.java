package ar.fiuba.tdd.tp1.parser.impl;

public class NamedRange {

    private String rangeName;
    private String range;

    public NamedRange(String rangeName, String range) {
        this.rangeName = rangeName;
        this.range = range;
    }

    public String getRange() {
        return this.range;
    }

    public String getRangeName() {
        return this.rangeName;
    }
}
