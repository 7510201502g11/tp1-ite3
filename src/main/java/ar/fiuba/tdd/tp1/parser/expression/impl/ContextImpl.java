package ar.fiuba.tdd.tp1.parser.expression.impl;

import ar.fiuba.tdd.tp1.parser.expression.Context;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Implementacion por defecto de contexto.
 * 
 * @author martin.zaragoza
 *
 */
public class ContextImpl implements Context {
    private Map<String, Object> values = new HashMap<String, Object>();

    @Override
    public Context put(String key, Object value) {
        values.put(key, value);
        return this;
    }

    @Override
    public Object get(String key) {
        return values.get(key);
    }

    @Override
    public Set<String> keys() {
        return values.keySet();
    }

    @Override
    public Collection<Object> values() {
        return values.values();
    }

    public String toString() {
        return values.toString();
    }
}
