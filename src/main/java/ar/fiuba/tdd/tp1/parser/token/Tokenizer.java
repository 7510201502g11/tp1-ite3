package ar.fiuba.tdd.tp1.parser.token;

import java.util.List;

/**
 * Representa un tokenizador de strings.
 * 
 * @author martin.zaragoza
 *
 */
public interface Tokenizer {

    /**
     * Agrega una nueva expresion regular y se le asigna un tipo. Una expresion regular AGREGADA
     * (insertada al tokenizer mediante "add"), es considerada con menor prioridad que las ya
     * existentes en el Tokenizer.
     * 
     * @param strRegex
     *            String con expresion regular a detectar.
     * @param tokenType
     *            Tipo de token que representa.
     */
    public abstract void add(String strRegex, int tokenType);

    /**
     * Apila una expresion regular. Una expresion regular APLIADA es considerada con mayor prioridad
     * que las expresiones regulares ya presentes.
     * 
     * @param stringRegex
     *            String con expresion regular a detectar.
     * @param tokenType
     *            Tipo de token que representa.
     */
    public void stack(String stringRegex, int tokenType);

    /**
     * Tokeniza un string. Acceder al resultado mediante getTokens.
     * 
     * @param str
     *            String a tokenizar
     */
    public abstract Tokenizer tokenize(String str);

    /**
     * Retorna la lista de tokens.
     * 
     * @return lista de tokens.
     */
    public abstract List<Token> getTokens();

    /**
     * Obtiene un nuevo consumidor de tokens a partir de la tokenizacion.
     * 
     * @return nuevo consumidor de tokens.
     */
    public abstract TokenConsumer getTokenConsumer();

}