package ar.fiuba.tdd.tp1.parser.var;

import ar.fiuba.tdd.tp1.util.VarRef;

import java.util.Optional;

/**
 * Representa un puntero y variable (por ejemplo: puntero = default, variable= C23).
 * 
 * @author martin
 *
 */
public class PointerVariablePair {
    public final String pointer;
    public final String relativeVariable;
    private static final String PAGE_SEP = VarRef.PAGE_SEP;

    public PointerVariablePair(String pointer, String relativeVariable) {
        super();
        this.pointer = Optional.ofNullable(pointer).orElse("");
        this.relativeVariable = relativeVariable;
    }

    public boolean hasPointer() {
        return pointer != null && !pointer.isEmpty();
    }

    public static PointerVariablePair create(String absoluteVariable) {
        if (absoluteVariable.contains(PAGE_SEP)) {
            String[] split = absoluteVariable.split(PAGE_SEP);
            return new PointerVariablePair(split[0], split[1]);
        } else {
            return new PointerVariablePair("", absoluteVariable);
        }
    }

    public String toString() {
        String prefix = pointer.isEmpty() ? "" : pointer + PAGE_SEP;
        return prefix + relativeVariable;
    }
}
