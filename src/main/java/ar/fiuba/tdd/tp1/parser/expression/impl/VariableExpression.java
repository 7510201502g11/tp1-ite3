package ar.fiuba.tdd.tp1.parser.expression.impl;

import ar.fiuba.tdd.tp1.parser.expression.Context;
import ar.fiuba.tdd.tp1.parser.expression.ContextUtils;
import ar.fiuba.tdd.tp1.parser.expression.ContextUtils.ValueGetter;
import ar.fiuba.tdd.tp1.parser.expression.EvaluationException;
import ar.fiuba.tdd.tp1.parser.expression.Expression;
import ar.fiuba.tdd.tp1.util.ValueTransformer;

import java.util.Optional;

/**
 * Representa expresion de valores variables.
 * 
 * @author martin
 *
 */
public class VariableExpression implements Expression {
    private String name;
    //    private Double value = null;
    private Object value = null;

    public VariableExpression(String name) {
        this.name = name;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public Double getValueAsDouble(Context context) {
        // si dentro del contexto se encuentra la variable, la misma se establece usando dicho
        // valor. De lo contrario la misma no se modifica.

        Object objVal = getValue(context,  ContextUtils::getValueAsDouble );
        return ValueTransformer.get().asDouble( objVal );
    }

    @Override
    public String getValueAsString(Context context) {
        return this.getValue(context, ContextUtils::getValueAsString).toString();
    }

    public Object getValue(Context context, ValueGetter valueGetter) {
        Optional.ofNullable(context).ifPresent(ctx -> {
                Optional.ofNullable(ctx.get(name)).ifPresent(val -> {
                        this.setValue(valueGetter.get(ctx, name));
                    });
            });

        Optional.ofNullable(value).orElseThrow(
                () -> new EvaluationException("Variable " + name + " no seteada!"));

        return value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isValueSet() {
        return value != null;
    }
}
