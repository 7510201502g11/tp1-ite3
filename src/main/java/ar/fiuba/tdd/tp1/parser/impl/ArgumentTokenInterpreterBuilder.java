package ar.fiuba.tdd.tp1.parser.impl;

import ar.fiuba.tdd.tp1.parser.Parser;
import ar.fiuba.tdd.tp1.parser.ParserException;
import ar.fiuba.tdd.tp1.parser.TokenInterpreter;
import ar.fiuba.tdd.tp1.parser.expression.Expression;
import ar.fiuba.tdd.tp1.parser.expression.impl.StandardArithmeticFunctionExpressionBuilder;
import ar.fiuba.tdd.tp1.parser.token.TokenConsumer;
import ar.fiuba.tdd.tp1.parser.token.TokenType;
import ar.fiuba.tdd.tp1.util.DefaultValueMap;

public class ArgumentTokenInterpreterBuilder extends TokenInterpreterBuilder {
    public ArgumentTokenInterpreterBuilder(Parser parser) {
        super(parser);
    }

    public DefaultValueMap<Integer, TokenInterpreter> build() {
        argumentTokenInterpreters = new DefaultValueMap<>();
        
        this.putArgumentDefaultValue();
        this.putArgumentFunction();
        this.putArgumentOpenbracket();
        
        return argumentTokenInterpreters;
    }

    protected void putArgumentDefaultValue() {
        this.argumentTokenInterpreters.setDefaultValue((params) -> {
                print("value -> PLUSMINUS value");
                print("argument -> value");
                return value();
            });
    }

    protected void putArgumentOpenbracket() {
        this.argumentTokenInterpreters.put(tokType.openBracket(), (params) -> {
                print("argument -> OPEN_BRACKET expression CLOSE_BRACKET");
                nextToken();
                Expression expr = expression();
    
                TokenConsumer cons = (TokenConsumer) params[0];
    
                String strOperation = cons.getCurrentTokenSequence();
                if (cons.getCurrentTokenType() != tokType.closeBracket()) {
                    throw new ParserException("Close brackets expected, " + strOperation
                            + " found instead!");
                }
    
                nextToken();
                return expr;
    
            });
    }


    protected void putArgumentFunction() {
        this.argumentTokenInterpreters.put(tokType.function(), (params) -> {
                print("argument -> FUNCTION argument");
                TokenConsumer cons = (TokenConsumer) params[0];
                String strOperation = cons.getCurrentTokenSequence();
                nextToken();
                Expression expr = argument();
                return StandardArithmeticFunctionExpressionBuilder.newInstance().build()
                        .setOperationId(strOperation).setArgument(expr);
            });
    }
}
