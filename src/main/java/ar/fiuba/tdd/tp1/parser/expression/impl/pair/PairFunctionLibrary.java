package ar.fiuba.tdd.tp1.parser.expression.impl.pair;

import java.util.HashMap;
import java.util.Map;

public class PairFunctionLibrary {
    private Map<String, PairFunctionExpression> pairExpressions = new HashMap<>();
    private static PairFunctionLibrary instance = new PairFunctionLibrary();

    public static PairFunctionLibrary getInstance() {
        return instance;
    }

    protected PairFunctionLibrary() {
        pairExpressions.put("CONCAT", new ConcatExpression());
        pairExpressions.put("LEFT", new LeftExpression());
        pairExpressions.put("RIGHT", new RightExpression());
        pairExpressions.put("PRINTF", new PrintFunctionExpression());
        pairExpressions.put("TODAY", new TodayExpression());
    }

    public PairFunctionExpression get(String key) {
        return pairExpressions.get(key.toUpperCase()).copy();
    }
}
