package ar.fiuba.tdd.tp1.parser.token;

/**
 * Constructor de Tokenizers.
 * 
 * @author martin.zaragoza
 *
 */
public interface TokenizerBuilder {
    /**
     * Crea una nueva instancia de Tokenizer lista para usarse.
     * 
     * @return nueva instancia de Tokenizer lista para usarse.
     */
    Tokenizer build();
}
