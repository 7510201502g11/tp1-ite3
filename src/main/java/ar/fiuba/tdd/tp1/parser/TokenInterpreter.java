package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.parser.expression.Expression;

@FunctionalInterface
public interface TokenInterpreter {
    public Expression create(Object... params);
}
