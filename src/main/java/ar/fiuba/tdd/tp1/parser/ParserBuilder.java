package ar.fiuba.tdd.tp1.parser;

/**
 * Armador de parsers.
 * 
 * @author martin.zaragoza
 *
 */
public interface ParserBuilder {
    /**
     * Crea una nueva instancia de parser lista para usarse.
     * 
     * @return Nuevo parser.
     */
    Parser build();
}
