package ar.fiuba.tdd.tp1.parser.expression.impl.pair;

import ar.fiuba.tdd.tp1.parser.expression.Context;

public abstract class StringFunctionExpression extends PairFunctionExpression {

    @Override
    public PairFunctionExpression copy() {
        StringFunctionExpression copy = getExpression();
        this.getExpressions().forEach(expr -> copy.add(expr));
        return copy;
    }

    @Override
    public Double getValueAsDouble(Context context) {
        throw new UnsupportedOperationException(
                "Imposible obtener valor numerico de concatenacion...");
    }

    public abstract StringFunctionExpression getExpression();
}
