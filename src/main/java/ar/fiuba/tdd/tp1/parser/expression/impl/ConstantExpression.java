package ar.fiuba.tdd.tp1.parser.expression.impl;

import ar.fiuba.tdd.tp1.parser.expression.Context;
import ar.fiuba.tdd.tp1.parser.expression.Expression;
import ar.fiuba.tdd.tp1.util.ValueTransformer;

/**
 * Representa expresion de valor constante.
 * 
 * @author martin
 *
 */
public class ConstantExpression implements Expression {
    private Object value;

    @Override
    public Double getValueAsDouble(Context context) {
        //        return Double.valueOf( value.toString() );
        return ValueTransformer.get().asDouble(value);
    }

    @Override
    public String getValueAsString(Context context) {
//        return ValueTransformer.get().asDoubleString(value);
        return value.toString();
    }

    public ConstantExpression(Object value) {
        super();
        this.value = value;
    }

    public static ConstantExpression create(Object value) {
        return new ConstantExpression(value);
    }
}
