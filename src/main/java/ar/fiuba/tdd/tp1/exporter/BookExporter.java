package ar.fiuba.tdd.tp1.exporter;

import ar.fiuba.tdd.tp1.model.cell.Cell;
import ar.fiuba.tdd.tp1.model.cell.CrPair;

import java.util.HashMap;
import java.util.Map;

public interface BookExporter {

    public void export(HashMap<String, Map<CrPair, Cell<CrPair, String>>> cellValueMap,
            HashMap<String, Map<CrPair, Map<String, String>>> cellFormatsMap, String filePath, String fileName);

}
