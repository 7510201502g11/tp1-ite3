package ar.fiuba.tdd.tp1.exporter;

import ar.fiuba.tdd.tp1.exceptions.fileio.FileCreationException;
import ar.fiuba.tdd.tp1.exceptions.fileio.InvalidDirectoryNameException;
import ar.fiuba.tdd.tp1.model.cell.Cell;
import ar.fiuba.tdd.tp1.model.cell.CrPair;
import ar.fiuba.tdd.tp1.util.ColumnIndexManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CsvExporter implements SheetExporter {

    @Override
    public void export(Map<CrPair, Cell<CrPair, String>> cellValueMap, String filePath,
            String fileName) {

        Set<CrPair> cellIds = cellValueMap.keySet();
        List<String> ids = new ArrayList<String>();
        Set<Integer> rows = this.getCellsRows(cellIds);
        Set<String> columns = this.getCellColumns(cellIds);

        HashMap<Integer, String> fileLines = null;

        fileLines = this.createFileLines(cellValueMap, rows, columns);

        addCellIds(cellIds, ids);

        java.util.Collections.sort(ids);

        StringBuffer buf = new StringBuffer();
        makeLineToWrite(cellValueMap, buf, ids);

        String fileExtension = fileName;

        if (!(new File(filePath)).exists()) {
            if (!(new File(filePath)).getParentFile().mkdirs()) {
                throw new InvalidDirectoryNameException(
                        "La ruta de directorio especificada no es valida.");
            }
        }

        File file = new File(filePath + fileExtension);

        createFile(file);

        exportToFile(filePath, fileLines, fileExtension);
    }

    private HashMap<Integer, String> createFileLines(
            Map<CrPair, Cell<CrPair, String>> cellValueMap, Set<Integer> rows, Set<String> columns) {

        StringBuffer buf = new StringBuffer();
        HashMap<Integer, String> fileLines = new HashMap<Integer, String>();

        List<Integer> rowList = new ArrayList<Integer>();
        List<String> colList = new ArrayList<String>();

        convertSetsToList(rows, columns, rowList, colList);

        // ciclo entrelas rows que hay
        for (int i = rowList.get(0); i <= rowList.get(rowList.size() - 1); i++) {
            // ciclo las columnas
            int firstCol = this.getExcelColumnNumber(colList.get(0));
            int lastCol = this.getExcelColumnNumber(colList.get(colList.size() - 1));
            for (int j = firstCol; j <= lastCol; j++) {

                Cell<CrPair, String> cellToWrite = cellValueMap.get(new CrPair(i, this
                        .getExcelColumnName(j)));
                if (cellToWrite != null) {
                    buf.append(cellToWrite.getRawValue() + ";");
                } else {
                    buf.append(";");
                }
            }
            buf.append("\n");
            fileLines.put(i, buf.toString());
            buf.delete(0, buf.length());
        }

        return fileLines;
    }

    private void convertSetsToList(Set<Integer> rows, Set<String> columns, List<Integer> rowList,
            List<String> colList) {
        rowList.addAll(rows);
        colList.addAll(columns);
        java.util.Collections.sort(rowList);
        java.util.Collections.sort(colList);
    }

    private Set<Integer> getCellsRows(Set<CrPair> cellIds) {
        List<Integer> rows = new ArrayList<Integer>();

        cellIds.forEach((id) -> {
                rows.add(id.getRow());
            }
        );

        Set<Integer> result = new HashSet<Integer>();
        result.addAll(rows);
        return result;
    }

    private Set<String> getCellColumns(Set<CrPair> cellIds) {
        List<String> columns = new ArrayList<String>();

        cellIds.forEach((id) -> {
                columns.add(id.getColumn());
            }
        );

        Set<String> result = new HashSet<String>();
        result.addAll(columns);
        return result;
    }

    private void exportToFile(String filePath, HashMap<Integer, String> fileLines,
            String fileExtension) {
        try {
            OutputStream output = new FileOutputStream(filePath + fileExtension);

            fileLines.forEach((lineNumber, line) -> {
                    byte[] contentInBytes = line.getBytes();
                    try {
                        output.write(contentInBytes);
                        output.flush();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            );
            output.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createFile(File file) {
        try {
            file.createNewFile();
        } catch (IOException e) {
            throw new FileCreationException("No se pudo crear el archivo.");
        }
    }

    private void makeLineToWrite(Map<CrPair, Cell<CrPair, String>> cellValueMap, StringBuffer buf,
            List<String> ids) {
        for (String cellId : ids) {
            buf.append(cellValueMap.get(CrPair.create(cellId)).getRawValue() + ";");
        }
    }

    private int getExcelColumnNumber(String column) {
//        int result = 0;
//        for (int i = 0; i < column.length(); i++) {
//            result *= 26;
//            result += column.charAt(i) - 'A' + 1;
//        }
//        return result;
        return new ColumnIndexManager().getExcelColumnNumber(column);
    }

    private String getExcelColumnName(int number) {
//        final StringBuilder sb = new StringBuilder();
//
//        int num = number - 1;
//        while (num >= 0) {
//            int numChar = (num % 26) + 65;
//            sb.append((char) numChar);
//            num = (num / 26) - 1;
//        }
//        return sb.reverse().toString();
        return new ColumnIndexManager().getExcelColumnName(number);
    }

    private void addCellIds(Set<CrPair> cellIds, List<String> ids) {
        cellIds.forEach((id) -> {
                ids.add(id.toString());
            }
        );
    }

}
