package ar.fiuba.tdd.tp1.exporter;

import ar.fiuba.tdd.tp1.model.cell.Cell;
import ar.fiuba.tdd.tp1.model.cell.CrPair;

import java.util.Map;

public interface SheetExporter {

    public void export(Map<CrPair, Cell<CrPair, String>> cellValueMap, String filePath, String fileName);
    
}
